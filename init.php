<?php


	require_once("core/Common.php");
	require_once("core/PhpInfo.php");
	require_once("core/CheckPhpVersion.php");
	
	require_once("config.php");

	//if we need to check php version, 
	if ( defined("FW_CHECK_PHP_VERSION") && FW_CHECK_PHP_VERSION && !fw_check_php_version() ) {

		//if old php, show notice, return to previous theme and dont continue loading framework
		add_action("after_switch_theme", "fw_old_php_notice");

	} else {

		//load framework
		require_once("core/Fw.php");
		
		\Fw\Fw::init();
		
		
	}