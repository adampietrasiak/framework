<?php 

	/**
	 * @name Framework example page
	 * @group Framework
	 */

?>

<div class="fw-center-box">
	<h2><strong>Framework</strong> example admin page</h2>
	<p>Framework example admin page content here</p>
	<p>You can check and edit this page file in <br/><strong><?php echo __FILE__; ?></strong></p>
	<hr/>
	<p>
		<a href="<?php echo home_url(); ?>" class="button button-primary">Return to homepage</a>
	</p>
</div>

<?php //return false; //return false to avoid displaying this page