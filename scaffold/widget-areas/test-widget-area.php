<?php 
	
	/**
	 * @name Framework test widget area
	 * @description Framework test widget area description
	 */

	//you can display this widget area with function widget_area("test-widget-area");

	//if you display widget area using function above - special variable with all widgets html compiled together $widgets is avaliable in this file and you can use it in this file
	//if you dont write any html in this file - only widgets html will be displayed

 ?>

 <div class="test-widget-area-container"><?php echo $widgets; ?></div>