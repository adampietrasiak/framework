<?php 

	/**
	 * @name Framework test post template
	 */

	//this file create templates (same as page templates) for custom post types
	//templates will be added to select box in post edition page
	//you can use it same way as page templates. All wordpress function like the_content() can be used

	//you can create many folders named as post type you want to add template to
	//so you can create folder like 'post' , 'page' , 'custom-post-type-name' etc

?>

<p>This is custom post template</p>

<h2><?php the_title(); ?></h2>

<p><?php the_content(); ?></p>