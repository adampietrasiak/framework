<?php 

	//this is partial file
	//partial files helps you to organize parts of template that are commonly used
	//you can pass variables to partial
	//partial can be used with 'partial' function that will render it or 'get_partial' that will return html
	
	//you can use this partial with partial("test/test-partial", array("someVar" => "value"));
	//echo $someVar;

?>