// files in js folder will be auto enqueued to website

// files in main js folder will be used only on front-end (no on admin panel)
// files in sub folder 'admin' will be only used on back-end (admin panel)
// files in sub folder 'global' will be used both on front and back-end

// files in sub folder 'assets' will be registered to be enqueued later
// slug of registered file is last part of its name - so file named jquery.some-script.script-name.js will have slug 'script-name'
// such file can be enqueued with wordpress functions or with framework function needs('script-name')

// files with multi-part name will be loaded in proper order. so:
// if there are files file-a.js and file-a.file-b.js - file-a will be loaded first

// if you want some file to be always loaded last - name it like last.my-script.js


/* js code here */