<?php 

	/**
	 * @name Framework test widget
	 * @description This is test widget
	 * @param name : propA | type : text
	 * @param name : propB | type : textarea
	 * @param name : propC | type : html
	 * @param name : propD | type : color
	 * @param name : propE | type : checkbox
	 * @param name : propF | type : image
	 * @param name : propG | type : number | default : 8
	 * @param name : propH | type : select | options : optionA, optionB, optionC | default : optionB
	 */

	//this will create new widget that can be used in admin panel widgets section
	//all params will be added to widgets editor box

	//you can use params as normal variables here. So if param name is 'someParam' you can use it in this file with $someParam

	//type html will create rich text editor
	//type image param will have value of url of image
	//type checkbox will have bool true/false value

?>

This is widget output with propA value : <?php echo $propA; ?>