<?php 
	if ( current_user_can("edit_theme_options") ) {
		\Fw\Options::saveAll($options);
	}