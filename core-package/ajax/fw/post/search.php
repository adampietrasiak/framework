<?php 
	$query = array(
		"s" => $keyword
	);

	if ( $type ) $query["post_type"] = $type;
	if ( $settings ) foreach ( (array) $settings as $key => $val ) {
		$query[$key] = $val;
	}

	$results = query_posts( $query );

	$results = empty($results) ? false : fw_post($results);

	echo json_encode($results);
	
?>