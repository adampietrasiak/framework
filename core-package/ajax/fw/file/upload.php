<?php 	
	$upload_overrides = array( 'test_form' => false );
	$file = wp_handle_upload($_FILES["file"], $upload_overrides);


	if ( $attachment ) {
		$pi = pathinfo($file["file"]);
		$mediaId = wp_insert_attachment( array(
			"post_title" => $pi["filename"],
			"post_content" => "",
			"post_status" => "publish",
			"post_mime_type" => $file["type"]
		), $file["file"] );

		$file["id"] = $mediaId;
	}

	echo json_encode($file);		
?>