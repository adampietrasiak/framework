<?php

	if ( is_file($partial) ) die;	

	$output = "";


	foreach ($items as $item) {
		$output .= \Fw\Partial::get($partial, (array) $item);
	}

	echo $output;