<?php


	if ( is_file($partial) ) die; //render only registered partials. It would be quite danger to render any file to frontend
	\Fw\Partial::render($partial, $vars);