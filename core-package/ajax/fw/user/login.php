<?php
	$creds = array();
	$creds['user_login'] = $login;
	$creds['user_password'] = $pass;
	$creds['remember'] = $remember;
	$user = wp_signon( $creds, false );
	$result = array();
	if ( is_wp_error($user) ) {
		\Fw\WpUtil::errorHeader();
		$result["result"] = "error";
		$result["message"] = $user->get_error_message();
		echo json_encode($result);
	} else {
		$result["result"] = "ok";
		echo json_encode($result);
	}
	die;