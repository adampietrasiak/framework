<?php 

	if ( !is_admin() ) {
		add_filter("post_class", function($classes) {
			$classParts = explode( "-" , $classes[0]);
			$id = $classParts[1];
			if ( is_numeric($id) && has_post_thumbnail( (int) $id ) ) {
				$classes[] = "post-with-thumbnail";
			} else {
				$classes[] = "post-without-thumbnail";
			}
			return $classes;
		});
	}



?>