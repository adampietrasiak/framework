<?php 

	add_action("fw_body_start", function(){
		$bodyStart = \Fw\WpUtil::templateHasFile( "body-start" , array("php") );
		if ( $bodyStart ) \Fw\Partial::render($bodyStart);
	});

	add_action("fw_body_end", function(){
		$bodyEnd = \Fw\WpUtil::templateHasFile( "body-end" , array("php") );
		if ( $bodyEnd ) \Fw\Partial::render($bodyEnd);
	});	

?>