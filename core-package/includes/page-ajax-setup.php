<?php 
	if ( FW_ENABLE_PAGE_AJAX ) {
		add_filter("body_class", function($classes){
			$classes[] = "fw-ajax-enabled";
			return $classes;
		});
	}
 ?>