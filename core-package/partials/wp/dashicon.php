<?php 
	/**
	 * @default class : fw-dashicon | icon : admin-page
	 */
?>

<span class="dashicons dashicons-<?php echo $icon ?> <?php echo $class ?>"></span>