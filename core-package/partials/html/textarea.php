<?php 
	$attrStr = isset($attr) ? \Fw\Common::arrayToHtmlAttr($attr) : "";
	$value = $value ?: $_POST[$name] ?: $_GET[$name] ?: null;
	$placeholder = $placeholder ?: $label ?: "";
	$labelAfter = !empty($labelAfter) && $labelAfter;
?>

<?php if ( !empty($if) ) : ?>
<div show-if="<?php echo $if ?>">
<?php endif; ?>
	<?php if ( !empty($align) ) : ?>
		<div class="align">
	<?php endif; ?>

	<?php if ( !$labelAfter && !empty($label) ) : ?>
		<label class="<?php echo $labelClass ?>" for="<?php echo $id ?>"><?php echo $label ?></label>
	<?php endif; ?>

	<?php if ( !empty($icon) ) : ?>
		<span class="icon-bigger <?php echo $icon ?>"></span>
	<?php endif; ?>
	<textarea placeholder="<?php echo $placeholder ?>" name="<?php echo $name; ?>" id="<?php echo $id ?>" class="<?php echo $class ?>" <?php echo $attrStr ?> rows="<?php echo $rows ?>" cols="<?php echo $cols ?>"></textarea>

	<?php if ( $labelAfter && !empty($label) ) : ?>
		<label class="<?php echo $labelClass ?>" for="<?php echo $id ?>"><?php echo $label ?></label>
	<?php endif; ?>

	<?php if ( !empty($align) ) : ?>
		</div>
	<?php endif; ?>

<?php if ( !empty($if) ) : ?>
</div>
<?php endif; ?>