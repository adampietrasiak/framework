<div class="fw-input">

<?php 
	

	$value = $value ?: $_POST[$name] ?: $_GET[$name] ?: null;
	$type = $type ?: "text";
	$placeholder = $placeholder ?: $label ?: "";
	$labelAfter = !empty($labelAfter) && $labelAfter;
	if ( !empty($suggestions) ) {
		$class .= " fw-input-suggestions ";
		$attr["data-suggestions"] = htmlspecialchars( \json_encode( (array) $suggestions) , ENT_QUOTES );
	}
	$attrStr = isset($attr) ? \Fw\Common::arrayToHtmlAttr($attr) : "";

?>

<?php if ( !empty($if) ) : ?>
<div show-if="<?php echo $if ?>">
<?php endif; ?>
	<?php if ( !empty($align) ) : ?>
		<div class="align">
	<?php endif; ?>

	<?php if ( !$labelAfter && !empty($label) ) : ?>
		<label class="<?php echo $labelClass ?>" for="<?php echo $id ?>"><?php echo $label ?></label>
	<?php endif; ?>

	<?php if ( !empty($icon) ) : ?>
		<span class="icon-bigger <?php echo $icon ?>"></span>
	<?php endif; ?>
	<input type="<?php echo $type ?>" placeholder="<?php echo $placeholder ?>" name="<?php echo $name; ?>" id="<?php echo $id ?>" class="<?php echo $class ?>" value="<?php echo $value ?>" autocomplete="<?php echo $autocomplete ?>" <?php echo ( !empty($disabled) ) ? 'disabled' : ''?> <?php echo $attrStr ?>/>

	<?php if ( $labelAfter && !empty($label) ) : ?>
		<label class="<?php echo $labelClass ?>" for="<?php echo $id ?>"><?php echo $label ?></label>
	<?php endif; ?>

	<?php if ( !empty($align) ) : ?>
		</div>
	<?php endif; ?>

<?php if ( !empty($if) ) : ?>
</div>
<?php endif; ?>

</div>