<div class="fw-input">
	<?php 
		
		$options = \Fw\Common::prepareSelectOptions($options);

	?>

	<?php if ( !is_array($options) && is_string($options) ) $options = explode(",", $options); ?>

	<?php 

		$assoc = !empty($assoc) && $assoc || !empty($protected) && $protected || \Fw\Common::isArrayAssoc($options);
		$attrStr = isset($attr) ? \Fw\Common::arrayToHtmlAttr($attr) : "";


		if ( empty($selected) && !empty($_POST[$name]) ) $selected = $_POST[$name];
		if ( empty($selected) && !empty($_GET[$name]) ) $selected = $_GET[$name];
	?>


	<?php if ( !empty($if) ) : ?>
	<div show-if="<?php echo $if ?>">
	<?php endif; ?>
		<?php if ( !empty($align) ) : ?>
			<div class="align">
		<?php endif; ?>
			<?php if ( !empty($label) ) : ?>
			<label for="<?php echo $id ?>"><?php echo $label ?></label>
			<?php endif; ?>
			<select name="<?php echo $name; ?>" id="<?php echo $id ?>" class="<?php echo $class ?>" <?php echo $attrStr ?> <?php echo ( !empty($disabled) ) ? 'disabled' : ''?>>
				<?php if ( !empty($placeholder) ) : ?>
					<option><?php echo $placeholder ?></option>
				<?php endif; ?>
				<?php foreach ($options as $key => $option): ?>
					<?php if ( !is_scalar($option) ) continue; ?>
					<?php $value = $assoc ? $key : $option; ?>
					<option value="<?php echo $value ?>" <?php selected( $selected, $value ); ?>><?php echo $option ?></option>
				<?php endforeach ?>
			</select>
		<?php if ( !empty($align) ) : ?>
			</div>
		<?php endif; ?>

	<?php if ( !empty($if) ) : ?>
	</div>
	<?php endif; ?>
</div>