<?php
	$config = \Fw\Common::readFileConfig($file);
	$options = $options ?: $config["option"];
?>

<?php if ( $options ) foreach ($options as $option): ?>
	<?php
		$option["namePrefix"] = $namePrefix;
		$option["idPrefix"] = $idPrefix;
		$option["values"] = $values;
	?>
	<?php \Fw\Partial::render("fw/common/options/set-single", $option) ?>
<?php endforeach ?>