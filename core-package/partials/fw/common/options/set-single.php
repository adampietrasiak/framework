<?php
	$type = $type ?: "text";
	$namePrefix = $namePrefix ?: "";
	$idPrefix = $idPrefix ?: "";

	$label = $label ?: \Fw\Common::humanize($name);

	$slug = \Fw\Common::slugify($name);
	$id = $id ?: $slug;

	$value = $value ?: $values[$slug] ?: $default;

	$name = $namePrefix . $slug;
	$id = $idPrefix . $id;

	$attrString = \Fw\Common::arrayToHtmlAttr($attr);

	$placeholder = $placeholder ?: $label;

?>

<div class="fw-option-container fw-option-container-<?php echo $type ?> s-mg-b <?php echo $class ?>">

	<div class="fw-option-label">
		<h3 for="<?php echo $fullSlug ?>" class="capital secondary d-b no-pad ws-nw light xs-mg-b fw-option-name"><?php echo $label ?></h3>

		<div class="fw-option-description">
			<label for="<?php echo $fullSlug ?>" class="capital tertiary d-b xs-mg-b"><?php echo $description ?></label>
		</div>
	</div>
	<div class="fw-option-input">
		<?php if ($type == "text"): ?>
			<input placeholder="<?php echo $placeholder ?>" type="text" value="<?php echo $value ?>" class="fw-options-input fullwidth d-b" name="<?php echo $name ?>" id="<?php echo $id ?>" <?php echo $attrString ?>>
		<?php endif ?>

		<?php if ($type == "textarea"): ?>
			<textarea placeholder="<?php echo $placeholder ?>" class="fw-options-input fullwidth d-b" name="<?php echo $name ?>" id="<?php echo $id ?>" <?php echo $attrString ?>><?php echo $value ?></textarea>
		<?php endif ?>

		<?php if ($type == "number"): ?>
			<input placeholder="<?php echo $placeholder ?>" type="number" value="<?php echo $value ?>" class="fw-options-input fullwidth d-b" name="<?php echo $name ?>" id="<?php echo $id ?>" <?php echo $attrString ?>>
		<?php endif ?>

		<?php if ($type == "color"): ?>
			<input type="color" value="<?php echo $value ?>" class="fw-options-input" name="<?php echo $name ?>" id="<?php echo $id ?>" <?php echo $attrString ?>>
		<?php endif ?>

		<?php if ($type == "image" || $type == "file"): ?>
			<button class="button button-default fw-option-choose-image" fw-media-library> Upload File <i name="upload"></i> </button>
			<input type="hidden" value="<?php echo $value ?>" class="fw-options-input" name="<?php echo $name ?>" id="<?php echo $id ?>" <?php echo $attrString ?>>
			<img alt="" title="Click to remove" class="fw-option-file-preview" <?php if ( $value ): ?> src="<?php echo $value ?>" <?php endif ?>>
		<?php endif ?>	

		<?php if ($type == "checkbox"): ?>
			<input type="hidden" value="off" name="<?php echo $name ?>">	
			<input type="checkbox" <?php checked( $value == "on" ); ?> class="fw-options-input no-mg-t no-mg-b" name="<?php echo $name ?>" id="<?php echo $id ?>" <?php echo $attrString ?>>
			<label for="<?php echo $fullSlug ?>" class="capital tertiary va-t"><?php echo $name ?></label>
			<?php if ( isset($suboptions) && count($suboptions) ): ?>
				<div class="clearfix"></div>				
				<?php foreach ($suboptions as $suboption): ?>
					<?php \Fw\Partial::render("fw/common/options/set-single", $suboption); ?>
				<?php endforeach ?>
			<?php endif ?>
		<?php endif ?>

		<?php if ($type == "select" && isset($options) ): ?>
			<?php $options = explode("," , $options); ?>		
			<?php \Fw\Partial::render("html/select", array(
				"options" => $options,
				"class" => "fw-options-input fullwidth d-b",
				"name" => $name,
				"id" => $id,
				"selected" => $value,
				"attr" => $attr
			)) ?>
		<?php endif ?>

	</div>

</div>
