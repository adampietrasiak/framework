<?php 
	$class = !empty($class) ? $class : "";
	if ( isset($large) && $large ) $class .= " large";
	if ( isset($smaller) && $smaller ) $class .= " smaller";

	$attrStr = isset($attr) ? \Fw\Common::arrayToHtmlAttr($attr) : "";

	if ( \Fw\Common::startsWith( $icon , "fw-icon" ) ) $icon = \Fw\Common::removePrefix( $icon , "fw-icon" );
?>

<i class="fw-icon-<?php echo $icon; ?> <?php echo $class ?>" <?php echo $attrStr ?>></i>