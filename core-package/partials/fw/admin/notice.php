<?php 
	if ( empty($message) ) return;
	$type = isset($type) ? $type : "updated"; 
?>
<div class="<?php echo $type ?>">
	<?php if ( !empty($title) ): ?>
		<h3><?php echo $title ?></h3>
	<?php endif ?>
    <p>
    	<?php echo $message ?>
    </p>
    <?php if ( !empty($cta) ): ?>
    	<p>
    		<a class="button button-primary no-underline" href="<?php echo $cta["href"] ?>"><?php echo $cta["text"] ?></a>
    	</p>
    <?php endif ?>
</div>