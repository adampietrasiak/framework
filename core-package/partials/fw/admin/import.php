<div class="fw-center-box">
	<h2>
		<?php echo sprintf( __( "<strong>%s</strong> data importer." , "fw" ) , wp_get_theme() ); ?>
	</h2>
	<p><?php _e("Click button below to import prepared data for this theme.", "fw") ?></p>

	<hr/>

	<p class="secondary">
		<?php if ( !empty($load) && $load ): ?>
			<button class="button button-primary button-hero fw-import-btn" data-confirm="<?php _e("Are you sure?", "fw") ?>" data-success="<?php _e("Successfully imported... Redirecting to homepage...", "fw") ?>" data-action="fw-load-import" data-progress="<?php _e("Importing data...", "fw") ?>" id="fw-import" data-redirect="<?php echo get_home_url() ?>"><span class="va-m dashicons dashicons-download"></span> <?php _e("Import Data", "fw") ?></button>
		<?php endif ?>

		<?php if ( !empty($save) && $save ): ?>
			<button class="button button-default button-hero fw-import-btn" data-confirm="<?php _e("Are you sure?", "fw") ?>" data-success="<?php _e("Content was successfully saved", "fw") ?>" data-action="fw-save-import" data-progress="<?php _e("Saving current data...", "fw") ?>" id="fw-export"><span class="va-m dashicons dashicons-upload"></span> <?php _e("Save current state to import", "fw") ?></button>
		<?php endif ?>
	</p>


	<?php if ( ( !isset($save) || !$save ) && ( !isset($load) || !$load ) ): ?>
		<p>
			<?php _e("There is nothing to import for this theme.", "fw") ?>
		</p>
	<?php endif ?>
	<p class="fw-import-status">
		<span class="fw-import-status-text"></span>
		<span class="spinner"></span>
	</p>
	<p class="tertiary">
		<strong><?php _e("Warning!", "fw") ?></strong> <?php _e("After you import content, some of your actual content might be lost.", "fw") ?>
	</p>
</div>



<script type="text/javascript">
	jQuery(document).ready(function($) {

		var block = false;
		$(".fw-import-btn").click(function(){
			var t = $(this);

			if ( block ) {
				return false;
			}

			block = true;

			var data = t.data();

			if ( data.confirm && !confirm( data.confirm ) ) {
				block = false;
				return false;
			}

			//t.addClass('tertiary');
			
			var status = $(".fw-import-status")
			var statusText = $(".fw-import-status-text", status).text( data.progress );
			var spinner = $(".spinner", status).addClass('visible')


			fw.ajax( data.action ).success(function(){
				
				if ( data.success ) {
					statusText.text(data.success);
				} else {
					status.empty();
				}

				if ( data.redirect ) window.location.href = data.redirect;
			}).always(function(){
				//t.removeClass('tertiary');
				spinner.removeClass('visible');
				block = false;
			})
		});

	});
</script>