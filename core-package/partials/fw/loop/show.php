<div class="loop loop-<?php echo $name; ?>">

	<?php 

		$loopDir = FW_TEMPLATE_DIR . DS . "loop" . DS . $name . DS;

		if ( have_posts() ) {
			\partial( $loopDir . "start" );
			while ( have_posts() ) {
				the_post();

				$type = \get_post_type();

				\partial( $loopDir . $type );
				\partial( $loopDir . "between" );

			} 
			\partial( $loopDir . "start" );
		} 

	?>
	
</div>

