<?php needs("fw-options") ?>
<div class="fw-options-page-info">
	<?php _e("Framework theme options version ", "fw"); echo FW_VERSION; ?>
</div>
<div class="fw-options-page" data-slug="<?php echo \Fw\Common::slugify($name); ?>">


	<?php \Fw\Partial::render("fw/options/navigation", array(
		"sections" => $sections,
		"name" => $name
	)); ?>

	<div class="fw-options-sections animating">
		<?php foreach ($sections as $sec) {
			\Fw\Partial::render("fw/options/section", $sec);
		} ?>
		<?php //partial("fw/options/tools") ?>
	</div>
</div>

