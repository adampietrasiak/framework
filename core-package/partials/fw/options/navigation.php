<div class="fw-options-nav">
	<!-- <h3 class="no-color light fw-options-nav-title"><?php echo $name ?></h3> -->
	<ul class="fw-options-nav-list">
		<?php foreach ($sections as $section): ?>
			<li class="fw-options-nav-single animating" data-target="#fw-section-<?php echo \Fw\Common::slugify($section["name"]) ?>">
				<span class="no-color no-mg"><?php fw_dashicon( !empty($section["icon"]) ? $section["icon"] : "wordpress" ) ?><?php echo $section["name"] ?></span>
			</li>
		<?php endforeach ?>
	</ul>
</div>