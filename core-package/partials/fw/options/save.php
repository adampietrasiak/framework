<div class="fw-options-save xl-mg-t">
	<button class="fw-button button button-primary va-m" fw-options-save="fw/options/save-all">
		<?php _e("Save changes", "fw") ?>
	</button>

	<button class="fw-button button button-primary va-m secondary" fw-click-ajax="fw/options/restore" fw-confirm="<?php _e("Are you sure?", "fw") ?>" fw-redirect>
		<?php _e("Restore defaults", "fw") ?>
	</button>
</div>
