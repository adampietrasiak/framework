<?php
	
	$vars["filter"] = function($option) {
		$option["loadValue"] = true;
		if ( empty($option["title"]) && !empty($option["name"]) ) {
			$option["title"] = $option["name"];
			unset($option["name"]);
		}

		if ( !empty($option["slug"]) ) $option["name"] = $option["slug"];

		return $option;
	};


	partial("fw/option/option", $vars);
?>

