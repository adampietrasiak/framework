<div class="fw-options-section hidden fade-in" id="fw-section-<?php echo \Fw\Common::slugify($name) ?>">
	<h1 class="fw-options-section-title light m-mg-b no-mg-t">
		<?php if ( !empty($icon) ): ?>
			<?php fw_dashicon($icon , "secondary"); ?>
		<?php endif ?>
		<?php echo $name ?>
	</h1>
	<p class="fw-options-section-description tertiary d-b no-mg-t xl-mg-b"><?php echo $description ?></p>

	

	<?php foreach ($option as $opt): ?>
		<?php \Fw\Partial::render("fw/options/option", $opt); ?>
	<?php endforeach ?>
	<div class="m-mg-t">
		<?php \Fw\Partial::render("fw/options/save"); ?>
	</div>
</div>

