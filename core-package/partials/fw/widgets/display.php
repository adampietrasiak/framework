<?php 
	$config = \Fw\Common::readFileConfig($file);
	$container = isset($config["wrap"]) ? $config["wrap"] !== "false" : true;

	if ( isset( $config["param"] ) ) foreach ( (array) $config["param"] as $param ) {
		$slug = isset($param["slug"]) ? $param["slug"] : \Fw\Common::slugify($param["name"]);



		if ( !isset($data[(string)$slug]) && isset($param["default"]) ) {
			$data[$slug] = $param["default"];
		}
	}
?>

<?php if ($container): ?><div class='widget fw-widget widget-<?php echo \Fw\Common::slugify($name) ?>'><?php endif ?>

	<?php \Fw\Partial::render( $file , $data); ?>

<?php if ($container): ?></div><?php endif ?>