<?php if ( is_active_sidebar($name) ) { 

		ob_start();
		dynamic_sidebar($name);
		$widgetsHtml = ob_get_contents();
		ob_end_clean();

		$areaFile = \Fw\WidgetAreas::$areas[$name];

		$output = get_partial( $areaFile , array("widgets" => $widgetsHtml) );

		echo strlen( trim( $output ) ) ? $output : $widgetsHtml;

} elseif ( isset($fallback) && is_callable($fallback) ) {
	$fallback($name);
} ?>