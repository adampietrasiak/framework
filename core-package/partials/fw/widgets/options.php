<div class="fw-widget-options">
	<?php 

		
		
		foreach ( $param as $option ) {

			$slug = isset($option["slug"]) ? $option["slug"] : $option["name"];
			$slug = \Fw\Common::slugify($slug);

			$option["object"] = $object; //pass instance to single option
			$option["slug"] = $slug;
			$option["value"] = isset($instance[$slug]) ? $instance[$slug] : (isset($option["default"]) ? $option["default"] : "");
			$option["type"] = isset($option["type"]) ? $option["type"] : "text";

			$fieldName = "widget-" . $object->control_options["id_base"] . "[" . $object->number . "][" . $slug . "]";
			$fieldId = "widget-" . $object->id . "-" . $slug;

			$option["title"] = \Fw\Common::humanize( $option["name"] );

			$option["name"] = $fieldName;
			$option["id"] = $fieldId;


			\Fw\Partial::render("fw/option/option", $option);
		}
	?>
</div>

