<div class="button button-default fw-button" fw-choose="<?php echo $partial ?>" fw-choose-title="<?php echo $title ?>" fw-choose-to="#<?php echo $id ?>"><?php echo empty($label) ? __("Choose", "fw") : $label ?></div>
<div class="button button-default fw-button secondary" fw-clear="#<?php echo $id ?>"><?php _e("Clear", "fw"); ?></div>
<p>
	<input type="text" readonly placeholder="<?php echo $placeholder ?>" value="<?php echo $value ?>" class="<?php echo $class ?> tertiary" name="<?php echo $name ?>" id="<?php echo $id ?>" <?php echo $attrStr ?>>
</p>