<?php wp_enqueue_media(); ?>
<?php
	if ( empty($value) ) $value = ""; 
?>
<div class="fw-option-image-preview <?php echo !$value ? "fw-option-image-preview-empty" : "" ?>" style="background-image: url(<?php echo $value ?>)" data-empty="<?php _e("No image selected", "fw"); ?>">
	<img class="fw-option-image-preview-image"/ src="<?php echo $value ?>">
</div>
<div class="<?php echo $buttonClass ?> <?php echo $class ?>" fw-media-library> <?php _e("Choose image", "fw") ?></div>
<div class="<?php echo $buttonClass ?> <?php echo $class ?> secondary fw-option-image-clear"> <?php _e("Clear image", "fw") ?></div>
<input type="hidden" value="<?php echo $value ?>" class="<?php echo $class ?>" name="<?php echo $name ?>" id="<?php echo $id ?>" <?php echo $attrStr ?>>