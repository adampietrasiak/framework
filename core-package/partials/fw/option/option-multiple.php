<?php \Fw\Partial::render("html/select", array(
	"options" => $options,
	"class" => $class,
	"name" => $name,
	"id" => $id,
	"selected" => $value,
	"attr" => !empty($attr) ? $attr : array(),
	"multiple" => true
)); ?>