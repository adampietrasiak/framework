<?php 

	/**
	 * @default type : text | multiple : 0 | labelClass : fw-option-label | titleClass : fw-option-title | containerClass : fw-option-container | optionClass : fw-option | class : fw-option-input | buttonClass : button button-default fw-button | suboptionsClass : fw-option-suboptions fade-in
	 */

	\needs( "fw-options" );

?>

<?php
	if ( !has_partial("fw/option/option-" . $type) ) return;
?>

<div class="<?php echo $containerClass ?> fw-option-container fw-option-container-<?php echo $type; ?>">

	<div class="fw-option-info">
		<?php if ( !empty($title) ): ?>
			<div class="fw-option-title <?php echo $titleClass ?>">
				<?php echo $title; ?>
			</div>
		<?php endif ?>

		<?php if ( !empty($description) ): ?>
			<label for="<?php echo $id ?>" class="fw-option-label <?php echo $labelClass ?>"><?php echo $description ?></label>
		<?php endif ?>
	</div>

	<div class="fw-option <?php echo $optionClass ?>">
		<?php 
			$vars["attrStr"] = !empty($attr) ? $attrStr = \Fw\Common::arrayToHtmlAttr($attr) : "";

			partial("fw/option/option-" . $type , $vars);
		?>
	</div>

	<?php if ( $type = "checkbox" && !empty($suboptions) && is_array($suboptions) ): ?>
		<div class="clearfix"></div>	
		<div class="fw-option-suboptions <?php echo $suboptionsClass ?>" show-if="%<?php echo $name ?>:on">
			<?php foreach ($suboptions as $suboption): ?>
				<?php 
					if ( $filter && empty($suboption["filter"]) ) {
						$suboption["filter"] = $filter;
					}
					
					partial("fw/option/option", $suboption ); 
				?>
			<?php endforeach ?>
		</div>
	<?php endif ?>

	<?php if ( $multiple ): ?>
		<div class="fw-option-add-new-container">
			<div class="button button-primary"><?php fw_dashicon("plus"); ?></div>
			<div class="button button-default"><?php fw_dashicon("minus"); ?></div>
		</div>
		
	<?php endif ?>
</div>