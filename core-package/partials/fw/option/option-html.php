<div class="fw-option-tinyMCE">
	<?php wp_editor($value, $name, array(

		"textarea_name" => $name,
		"editor_class" => $class,
		"teeny" => true,
		"textarea_rows" => 3

	)); ?>
</div>
