<?php
	
	/**
	 * @default max : 100 | min : 0 | step : 1
	 */

?>

<?php if ( empty($value) ) $value = (int) $min + ( (int) $max / 2 ); ?>

<input type="range" max="<?php echo $max ?>" min="<?php echo $min ?>" step="<?php echo $step ?>" value="<?php echo $value ?>" class="<?php echo $class ?>" name="<?php echo $name ?>" id="<?php echo $id ?>" <?php echo $attrStr ?>>
<span class="fw-option-range-value"><?php echo $value ?></span>