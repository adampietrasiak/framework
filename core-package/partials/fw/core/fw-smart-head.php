<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="<?php bloginfo( 'description' ); ?>">
<meta name="author" content="<?php bloginfo( 'admin_email' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
 
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS2 Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<title><?php echo \Fw\WpUtil::pageTitle(); ?></title>