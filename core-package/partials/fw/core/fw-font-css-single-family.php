<?php 

	$url = \Fw\Common::wpDirToUri($dir);
	$eot = is_file($dir . DS . $font . ".eot") ? $url . "/" . $font . ".eot" : false;
	$woff = is_file($dir . DS . $font . ".woff") ? $url . "/" . $font . ".woff" : false;
	$ttf = is_file($dir . DS . $font . ".ttf") ? $url . "/" . $font . ".ttf" : false;
	$svg = is_file($dir . DS . $font . ".svg") ? $url . "/" . $font . ".svg" : false;

?>

		/* @font-face : <?php echo $font ?> */

		@font-face {
		  font-family: "<?php echo $font ?>";
		  font-weight: normal;
		  font-style: normal;

		  src: url("<?php echo $eot ?>");
		  src: url("<?php echo $eot ?>?#iefix") format("embedded-opentype"), 
		  url("<?php echo $woff ?>") format("woff"), 
		  url("<?php echo $ttf ?>") format("truetype"), 
		  url("<?php echo $svg ?>#<?php echo $font ?>") format("svg");

		}
		@media screen and (-webkit-min-device-pixel-ratio:0) {
		    @font-face {
		        font-family: "<?php echo $font ?>";
		        src: url("<?php echo $svg ?>#<?php echo $font ?>") format("svg");
		    }
		}

		