
	<style id="fw-fonts">
		<?php foreach ($fonts as $font => $dir): ?>
			<?php \Fw\Partial::render("fw/core/fw-init-fonts-single", array(
				"font" => $font,
				"dir" => $dir
			)); ?>
		<?php endforeach ?>		
	</style>
