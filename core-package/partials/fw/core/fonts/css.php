<?php 

	foreach ( $fonts as $family => $weights ) {

		foreach ( $weights as $weight => $styles ) {

			foreach ( $styles as $style => $formats ) {

				usort( $formats , function($a, $b){
					$fA = $a["format"];
					$fB = $b["format"];

					$fA = str_replace( array("eot", "woff", "ttf", "svg") , array(4,3,2,1) , $fA);
					$fB = str_replace( array("eot", "woff", "ttf", "svg") , array(4,3,2,1) , $fB);

					if ( !is_numeric($fA) ) $fA = 0;
					if ( !is_numeric($fB) ) $fB = 0;

					return $fA < $fB;
				});

				//var_dump($formats);
				//
				if ( $weight == "lighter" ) $weight = 100;

				partial("fw/core/fonts/single-css", array(
					"family" => $family,
					"weight" => $weight,
					"style" => $style,
					"formats" => $formats
				));
				//foreach ( $formats as $fontData ) {}
				

			}
		}

	}

?>