@font-face {
	font-family: '<?php echo $family ?>';
	src:url('<?php echo $formats[0]['uri'] ?>');
	<?php $formats = array_map(function($item){
		$url = $item['uri'];
		if ( $item["format"] == "svg" ) $url .= "#" . $item["font-family"];
		return "\turl('" . $url . "') format('" . $item["format"] . "')";
	}, $formats); ?>
src: 
<?php echo implode(",\n", $formats) ?> ;
	font-weight: <?php echo $weight ?>;
	font-style: <?php echo $style ?>;
}

