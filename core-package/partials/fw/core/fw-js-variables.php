<?php if ($tag): ?>
<script>
<?php endif; ?>

	window.fw = window.fw || {};	
	window.fw.FW_URI = <?php echo json_encode(FW_URI); ?>;
	window.fw.FW_TEMPLATE_URI = <?php echo json_encode(FW_TEMPLATE_URI); ?>;
	window.fw.FW_DEBUG = <?php echo json_encode(FW_DEBUG); ?>;
	window.fw.FW_VERSION = <?php echo json_encode(FW_VERSION); ?>;
	window.fw.FW_AJAX = <?php echo json_encode(admin_url('admin-ajax.php')); ?>;
	window.fw.FW_HOME_URL = <?php echo json_encode(get_home_url()); ?>;
	window.fw.site = <?php echo json_encode( \Fw\WpUtil::bloginfo() ); ?>;

<?php if($tag) : ?>
</script>
<?php endif; ?>