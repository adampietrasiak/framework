<?php 
	$templates = array("default" => "Default") + (array) $templates;
	if ( isset($_GET["post"]) ) {
		$template = get_post_meta( (int) $_GET["post"], '_post_type_template' , true );
	}
?>

<p><strong><?php echo $niceType ?> display template</strong></p>
<?php \Fw\Partial::render("html/select", array(
	"options" => $templates,
	"id" => "post_template",
	"name" => "_post_type_template",
	"selected" => isset($template) ? $template : false
) ); ?>