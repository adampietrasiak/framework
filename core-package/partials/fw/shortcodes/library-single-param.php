<?php 
	$slug = \Fw\Common::slugify($name);
	$label = $description ?: $name;

?>
<div class="fw-shortcode-param-container">

	<?php if ($type == "text"): ?>
		<label for="" class="capital secondary"><?php echo $label ?>:</label>
		<input type="text" placeholder="<?php echo $description ?>" value="<?php echo $default ?>" class="fw-shortcode-param-single" data-param="<?php echo $slug ?>">
	<?php endif ?>

	<?php if ($type == "number"): ?>
		<label for="" class="capital secondary"><?php echo $label ?>:</label>
		<input type="number" value="<?php echo $default ?>" class="fw-shortcode-param-single" data-param="<?php echo $slug ?>">
	<?php endif ?>

	<?php if ($type == "color"): ?>
		<input type="color" value="<?php echo $default ?>" class="fw-shortcode-param-single" data-param="<?php echo $slug ?>">
		<label for="" class="capital secondary d-i"><?php echo $label ?></label>
	<?php endif ?>	

	<?php if ($type == "checkbox"): ?>
		<input type="checkbox" class="fw-shortcode-param-single" data-param="<?php echo $slug ?>">	
		<label for="" class="capital secondary d-i"><?php echo $label ?></label>
	<?php endif ?>

	<?php if ($type == "select" && isset($options) ): ?>
		<?php $options = explode("," , $options); ?>		
		<label for="" class="capital secondary d-i"><?php echo $label ?></label>
		<?php \Fw\Partial::render("html-select", array(
			"options" => $options,
			"class" => "fw-shortcode-param-single",
			"attr" => array(
				"data-param" => $slug
			)
		)) ?>
	<?php endif ?>	

</div>