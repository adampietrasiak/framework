<?php $shortcodes = \Fw\Shortcodes::$shortcodes; ?>
<div id="fw-sc-lib">
	<div class="fw-sc-lib">
		<div class="fw-sc-lib-nav">
			<div class="fw-sc-search">
				<input class="fw-sc-lib-nav-search" type="text" placeholder="Search...">
				<i name="search"></i>
			</div>
			<ul class="fw-sc-list zoom-in">
				<?php foreach ($shortcodes as $name => $scData): ?>
					<li class="fw-sc-nav-list-single" data-target="#fw-sc-<?php echo \Fw\Common::slugify($name) ?>"><i name="magic"></i> <?php echo $name ?></li>
				<?php endforeach ?>
			</ul>
		</div><div class="fw-sc-lib-details">
			<ul class="fw-sc-details-list">				
				<?php foreach ($shortcodes as $name => $scData): ?>
					<li class="sc-details-single hidden zoom-in" id="fw-sc-<?php echo \Fw\Common::slugify($name) ?>" data-tag="<?php echo $name ?>">	
						<em><?php echo $scData["description"] ?></em>
						<?php if( !empty( $scData["param"] ) ) foreach ($scData["param"] as $param) : ?>
							<?php \Fw\Partial::render("fw/option/option", $param); ?>
						<?php endforeach ?>
					</li>
				<?php endforeach ?>
			</ul>
			<pre id="fw-shortcode-preview" class="hidden"></pre>
		</div>
	</div>
</div>
