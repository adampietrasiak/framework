
<div id="fw-editor-sections-manager" class="bootstrap">
	<div class="fw-editor-sections-manager-section">
		<h2 class="fw-editor-section-title">Available Sections</h2>
		<ul id="fw-editor-sections">
			<?php foreach ($allSections as $section): ?>
				<?php \Fw\Partial::render("bs/collapse", array(
					"title" => $section["name"],
					"attr" => array(
						'data-slug' => $section["slug"]
					),
					"content" => $section["option"] ? \Fw\Partial::get( "fw/common/options/set" , array(
						"options" => $section["option"]
					)) : "No options for this section",
					"class" => "fw-editor-section-single"
				)) ?>				
			<?php endforeach ?>
		</ul>
	</div>
	<div class="fw-editor-sections-manager-section">
		<h2 class="fw-editor-section-title">Page sections</h2>
		<ul id="fw-page-sections">
			<?php foreach ((array) $actualSections as $section): ?>
				<?php //var_dump($section); ?>
				<?php if ( !$section["slug"] || !$section["data"] ) {
					continue;
				} ?>
				<?php \Fw\Partial::render("bs/collapse", array(
					"title" => $section["data"]["name"],
					"attr" => array(
						'data-slug' => $section["slug"]
					),
					"content" => $section["data"]["option"] ? \Fw\Partial::get( "fw/common/options/set" , array(
						"options" => $section["data"]["option"],
						"values" => $section["options"]
					)) : "No options for this section",
					"class" => "fw-editor-section-single"
				)) ?>
			<?php endforeach ?>
		</ul>
	</div>
</div>
