<?php
	$btnId = "content-" . $slug;
	$tabContentId = "fw-editor-tab-" . $slug
?>

<script id="fw-new-tab-<?php echo $slug ?>">

	(function($) {


		$(document).ready(function() {
			var lastTab = $("#content-html");
			var tabItem = lastTab.clone().removeAttr("id onclick").attr("id","<?php echo $btnId ?>" ).addClass("fw-editor-tab-switch fade-right").removeClass('switch-html').insertBefore(lastTab).text("<?php echo $name ?>")

		});

		$(document).on('click', '#<?php echo $btnId ?>', function(e) {
			e.preventDefault();

			var ed = tinyMCE.get('content');
			var dom = tinymce.DOM;
			$('#wp-content-editor-container, #post-status-info').hide();
			dom.removeClass('wp-content-wrap', 'html-active');
			dom.removeClass('wp-content-wrap', 'tmce-active');
			$(this).addClass('active');
			$('#<?php echo $tabContentId ?>').removeClass("hidden");
		});

		$(document).on('click', '#content-tmce, #content-html', function(e) {
			e.preventDefault();
			$('#<?php echo $btnId ?>').removeClass('active');
			$('#<?php echo $tabContentId ?>').addClass("hidden");
			$('#wp-content-editor-container, #post-status-info').show();
		});

	})(jQuery);

</script>