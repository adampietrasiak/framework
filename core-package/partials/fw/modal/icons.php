

<?php foreach ( fw_icons() as $icon): ?>
	<?php 

		partial("fw/icon", array(
			"icon" => $icon,
			"attr" => array(
				"data-value" => $icon,
				"title" => $icon
			),
			"class" => "fw-icon-choose"
		)); 

	?>
<?php endforeach ?>