<?php 
	//\Fw\Common::removePrefix($key, "fw-meta-")
	foreach ($meta as $option) {
		
		$option["namePrefix"] = "fw-meta-";

		$slug = !empty($option["slug"]) ? $option["slug"] : \Fw\Common::slugify( $option["name"] );
		
		if ( empty($option["title"]) && !empty($option["name"]) ) {

			$option["title"] = Inflector::humanize($option["name"]);
			unset($option["name"]);
		}
		$option["value"] = \get_post_meta( $id , $slug , true );

		\Fw\Partial::render("fw/option/option", $option);
	}
?>