<?php 
  $id = \Fw\Common::uniqueId("fw-carousel-")
?>

<div id="<?php echo $id ?>" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <?php $i = 0; foreach ($slides as $slide): $i++ ?>
      <li data-target="#<?php echo $id ?>" data-slide-to="<?php echo $i ?>" class="<?php echo $i == 0 ? "active" : "" ?>"></li>
    <?php endforeach ?>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <?php $i = 0; foreach ($slides as $slide): $i++ ?>      
      <div class="item <?php echo $i == 0 ? "active" : "" ?>">
        <img src="<?php echo $slide['image'] ?>">
        <div class="carousel-caption">
          <?php echo $slide['caption'] ?>
        </div>
      </div>
    <?php endforeach ?>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#<?php echo $id ?>" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#<?php echo $id ?>" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>