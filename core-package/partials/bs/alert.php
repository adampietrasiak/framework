<?php 
  $type = $type ?: "danger";
?>

<div class="alert alert-<?php echo $type ?> alert-dismissible fade in" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <h4><?php echo $title ?></h4>
  <p><?php echo $content ?></p>
</div>