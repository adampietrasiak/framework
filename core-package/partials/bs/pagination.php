<?php
  $first = $first ?: 1;
  $last = $last ?: 10;
  $active = $active ?: 1;

  $count = $count ?: 10;

  $enablePrev = $active >= $first && $active > 1;
  $enableNext = $active <= $first && $first + $count < $last;

  $url = $url ?: "?paged=%s";

  $next = $next ?: "&raquo;";
  $prev = $prev ?: "&laquo;";
  
?>

<ul class="pagination <?php echo $class ?>">
  <li class="<?php echo !$enablePrev ? 'disabled' : '' ?>"><a href="<?php echo $enablePrev ? sprintf($url, $i - 1) : '#' ?>">&laquo;</a></li>
  <?php for ($i=$first; $i < $first + $count; $i++) : ?>
    <?php if ( $i > $last ) break; ?>
    <li><a href="<?php echo sprintf($url, $i) ?>" class="<?php echo $i == $active ? 'active' : '' ?>"><?php echo $i ?></a></li>
  <?php endfor; ?>
  <li class="<?php echo !$enableNext ? 'disabled' : '' ?>"><a href="<?php echo $enableNext ? sprintf($url, $i + 1) : '#' ?>">&raquo;</a></li>
</ul>