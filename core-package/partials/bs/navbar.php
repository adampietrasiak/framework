<nav class="navbar navbar-default <?php echo $class ?>" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><?php echo $title ?></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <?php foreach ($item as $item): ?>
          <li class="<?php echo $item['items'] ? 'dropdown' : '' ?>">
            <a href="<?php echo $item['url'] ?>" class="<?php echo $item['class'] ?>"><?php echo $item['label'] ?></a>
            <?php if ($item['items']): ?>
              <ul class="dropdown-menu" role="menu">
                <?php foreach ($item["items"] as $item): ?>
                  <li><a href="<?php echo $item['url'] ?>" class="<?php echo $item['class'] ?>"><?php echo $item['label'] ?></a></li>
                <?php endforeach ?>
              </ul>
            <?php endif ?>
          </li>
        <?php endforeach ?>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>