<ol class="breadcrumb <?php echo $class ?>">
  <?php foreach ($crumbs as $url => $label): ?>
    <li><a href="<?php echo $url ?>"><?php echo $label ?></a></li>
  <?php endforeach ?>
</ol>