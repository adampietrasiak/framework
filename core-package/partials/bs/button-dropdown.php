<div class="btn-group">
  <a href="<?php echo $url ?>" class="btn btn-<?php echo $type ?: "default" ?>"><?php echo $label ?></a>
  <a class="btn btn-<?php echo $type ?: "default" ?> dropdown-toggle" data-toggle="dropdown">
    <span class="caret"></span>
    <span class="sr-only">Toggle Dropdown</span>
  </a>
  <ul class="dropdown-menu" role="menu">
    <?php foreach ($items as $label => $url): ?>
      <li><a href="<?php echo $url ?>"><?php echo $label ?></a></li>
    <?php endforeach ?>
  </ul>
</div>