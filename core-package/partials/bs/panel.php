<?php
  $type = $type ?: "default";
?>
<div class="panel panel-<?php echo $type ?>">
  <div class="panel-heading"><?php echo $title ?></div>
  <div class="panel-body">
    <?php echo $content ?>
  </div>
</div>