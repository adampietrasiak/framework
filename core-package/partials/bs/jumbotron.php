<div class="jumbotron <?php echo $class ?>">
  <h1><?php echo $title ?></h1>
  <p><?php echo $content ?></p>
  <?php if ($action): ?>
    <p><a href="<?php echo $action['href'] ?>" class="btn btn-primary btn-lg" role="button"><?php echo $action["label"] ?></a></p>
  <?php endif ?>
</div>