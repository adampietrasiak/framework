
<div class="tabs-container <?php echo $class ?>">
  <?php foreach ($tabs as $key => $tab): $tabs[$key]["id"] = \Fw\Common::uniqueId("tab-"); endforeach ?>
  <ul class="nav nav-tabs" role="tablist">
    <?php $i = 0; foreach ($tabs as $tab): $i++; ?>
      <li class="<?php echo $i == 0 ? 'active' : '' ?>"><a href="#<?php echo $tab['id'] ?>" role="tab" data-toggle="tab"><?php echo $tab['title'] ?></a></li>
    <?php endforeach ?>
  </ul>


  <div class="tab-content">
    <?php $i = 0; foreach ($tabs as $tab): $i++; ?>
      <li class="<?php echo $i == 0 ? 'active' : '' ?>"><a href="#<?php echo $tab['id'] ?>" role="tab" data-toggle="tab"><?php echo $tab['title'] ?></a></li>
      <div class="tab-pane <?php echo $i == 0 ? 'active' : '' ?>" id="<?php echo $tab['id'] ?>"><?php echo $tab['content'] ?></div>
    <?php endforeach ?>
  </div>
</div>
