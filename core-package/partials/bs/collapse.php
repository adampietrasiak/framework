<?php 
  $id = $id ?: \Fw\Common::uniqueId("collapse-");
  $title = $title ?: '$title';
  $content = $content ?: '$content';
  $attrStr = \Fw\Common::arrayToHtmlAttr($attr);
?>

<div class="panel panel-default no-pad xs-mg-b <?php echo $class ?>" <?php echo $attrStr ?>>
  <div class="panel-heading" data-toggle="collapse-next">
    <h4 class="no-mg">
      <a class="no-color accordion-toggle">
        <?php echo $title ?>
      </a>
    </h4>
  </div>
  <div class="panel-collapse collapse <?php echo $open ? "in" : "" ?>">
    <div class="panel-body">
      <?php echo $content ?>
    </div>
  </div>
</div>
