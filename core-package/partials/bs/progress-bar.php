<div class="progress">
  <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $progress ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $progress ?>%;">
    <?php if ($label): ?>
      <?php echo $progress ?>%
    <?php endif ?>
  </div>
</div>