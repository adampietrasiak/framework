<?php 

	/**
	 * @once true
	 */

?>

<h3><?php _e("Demo content of theme", "fw") ?></h3>

<p>
	<?php echo sprintf( __("Welcome to <strong>%s</strong> theme. Import demo content to quickly start working with this theme.", "fw") , \wp_get_theme() ); ?>
</p>

<p>
	<a class="button button-primary no-underline" href="<?php echo get_admin_url( null , "/themes.php?page=import-demo-data" ) ?>">
		<?php _e("Go to import page", "fw") ?>
	</a>
</p>