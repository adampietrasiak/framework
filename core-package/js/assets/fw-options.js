(function($) {

	$(function(){


		$(document).on("click", ".fw-options-nav-single", function(){
			//debugger;
			var t = $(this);
			$(".fw-options-nav-single").removeClass('active').filter(t).addClass('active');

			var targetSel = t.data("target");
			var target = $(targetSel);

			$(".fw-options-section").addClass('hidden').filter(target).removeClass('hidden');
		});
		$(".fw-options-nav-single").first().click();

		$(document).on("click", "*[fw-options-save]", function(){
			var btn = $(this);
			var optionFields = $(".fw-option-input");
			var options = {};

			var page = $(".fw-options-sections").addClass('tertiary');

			var action = btn.attr("fw-options-save");

			optionFields.each(function(){
				var t = $(this);
				var name = t.attr("name");
				var val = t.is("[type='checkbox']") ? ( t.is(":checked") ? "on" : "off" ) : t.val();

				if ( t.is("textarea") && tinyMCE && tinyMCE.get(name) ) val = tinyMCE.get(name).getContent();

				options[ name ] = val;
				console.log(name);
			});

			fw.ajax(action, {
				"options" : options
			}).always(function(){
				page.removeClass("tertiary");
			});
		});

		$(document).on("input", "input[type='range'].fw-option-input", function(){
			$(this).siblings('.fw-option-range-value').text( $(this).val() );
		});

		$(document).on("click", "[fw-choose]", function(){
			var t = $(this);
			var partial = t.attr("fw-choose");
			var title = t.attr("fw-choose-title");
			var target = t.attr("fw-choose-to");

			var targetObj = $(target);

			fw.partialModal(partial, title, {}, true).on("resolve", function(e,data){
				targetObj.val( data.value ).trigger("change");
			});
		});

		$(document).on("click", "[fw-clear]", function(){
			var t = $(this);
			var target = t.attr("fw-clear");
			var targetObj = $(target).attr("value", "");
		});

		$(document).on("media-library.select", ".fw-option-input", function(e,image){
			var btn = $(this);
			var previewContainer = btn.siblings('.fw-option-image-preview').removeClass("fw-option-image-preview-empty").css("background-image", image.url.cssBg());
			var previewImg = $(".fw-option-image-preview-image", previewContainer).attr("src", image.url);
			var input = btn.siblings('.fw-option-input').attr("value", image.url).show().trigger("change").css("display", "");
		});

		$(document).on("click", ".fw-option-image-preview", function(){
			$(this).siblings(".fw-option-input").eq(0).click();
		});

		$(document).on("click", ".fw-option-image-clear", function(e){
			var btn = $(this);
			var previewContainer = btn.siblings('.fw-option-image-preview').addClass("fw-option-image-preview-empty").css("background-image", "");
			var previewImg = $(".fw-option-image-preview-image", previewContainer).attr("src", "");
			var input = btn.siblings('.fw-option-input').attr("value", "").trigger("change");
		})

		$(document).on("click", ".fw-option-image-preview", function(){
			var img = $(this).removeAttr('src');
			var input = img.siblings('.fw-option-input').attr("value", "").trigger("change");

		});

		
		$(".fw-option-color-input").initialize(function(){
			var t = $(this);

			var options = {
				disabled : false,
				showPalette : true,
				clickoutFiresChange : true,
				//showAlpha : true,
				showButtons : false,
				showInitial : true,
				containerClassName : "fade-in",
				preferredFormat: "hex",
				showInput : true,
				showSelectionPalette : false,
				hide : function(){
					t.trigger("change")
				},
				//allowEmpty : true,
				palette : ["#c91f37","#dc3023","#9d2933","#cf000f","#e68364","#f22613","#cf3a24","#c3272b","#8f1d21","#d24d57","#f08f907","#f47983","#db5a6b","#c93756","#fcc9b9","#ffb3a7","#f62459","#f58f84","#875f9a","#5d3f6a","#89729e","#763568","#8d608c","#a87ca0","#5b3256","#bf55ec","#8e44ad","#9b59b6","#be90d4","#4d8fac","#5d8cae","#22a7f0","#19b5fe","#59abe3","#48929b","#317589","#89c4f4","#4b77be","#1f4788","#003171","#044f67","#264348","#7a942e","#8db255","#5b8930","#6b9362","#407a52","#006442","#87d37c","#26a65b","#26c281","#049372","#2abb9b","#16a085","#36d7b7","#03a678","#4daf7c","#d9b611","#f3c13a","#f7ca18","#e2b13c","#a17917","#f5d76e","#f4d03f","#ffa400","#e08a1e","#ffb61e","#faa945","#ffa631","#ffb94e","#e29c45","#f9690e","#ca6924","#f5ab35","#bfbfbf","#f2f1ef","#bdc3c7","#ecf0f1","#d2d7d3","#757d75"],
			}



			if ( t.val() ) options.value = t.val();

			t.spectrum(options);

		});
		
		

	});

})(jQuery);
