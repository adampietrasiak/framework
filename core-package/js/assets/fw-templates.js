(function($) {

	jQuery(document).ready(function($) {

		var templateOptionsBoxes = $("*[id*=fw-template-options]").hide();

		$(document).on("change", "#page_template", function(){
			var val = $("option:selected" , this).text().slugify();

			$("body").attr("page-template", val );

			$("*[fw-for-template]").hide().filter(function(){
				return $(this).attr("fw-for-template") == val;
			}).show();
		});


		$(document).on("change", "#page_template, #post_template", function(){
			var val = $("option:selected" , this).text().slugify();

			$("body").attr("page-template", val );

			templateOptionsBoxes.hide().filter(function(){
				return $(this).attr("id") == "fw-template-options-" + val;
			}).insertAfter( templateOptionsBoxes.last() ).show();
		});
				
		$("#page_template, #post_template").change();
		
	});



})(jQuery);
