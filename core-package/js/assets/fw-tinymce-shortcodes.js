(function($) {	


	$(function(){	
		if ( !window.tinymce ) return false;
		
		tinymce.PluginManager.add('fwshortcodes', function( editor, url ) {
			editor.addButton('fwshortcodes', {
				text: 'Shortcode',
				//icon: 'wp_code',
				tooltip: "Shortcodes library...",
				onclick: function() {

					var updatePreview = function(){
						var active = $(".sc-details-single.active", modal);
						var tag = active.data("tag");
						var params = $(".fw-option-input[name]", active);
						var attrs = {};

						params.each(function(){
							var t = $(this);
							var checkbox = t.is("[type='checkbox']");
							var val = !checkbox ? t.val() : ( t.is(":checked") );
							attrs[ t.attr("name") ] = val;
						});
						var sc = new wp.shortcode({
							"tag" : tag,
							"attrs" : attrs,
							//"content" : "Content..."
						}).string();

						var preview = $("#fw-shortcode-preview", modal).removeClass("hidden");
						preview.text(sc);
					}

					var modal = fw.partialModal("fw/shortcodes/library", "Shortcodes", {}, true).on("resolve", function(e,data){

					}).on("fw.partialModalReady", function(){
						if ( fw.initColorOptions ) fw.initColorOptions();
					})

					modal.on("click", ".fw-sc-nav-list-single", function(e){
						$(".sc-details-single").addClass('hidden').removeClass('active');


						var t = $(this);
						var target = $( t.data("target") ).removeClass('hidden').addClass('active');



						$(".fw-sc-nav-list-single").removeClass('active').filter(t).addClass('active');

						updatePreview();
					});

					modal.on("change keyup click", ".fw-option-input", function(e){

						updatePreview();
					});

					modal.on("keydown keyup", ".fw-sc-lib-nav-search", function(){
						var val = $(this).val();
						$(".fw-sc-nav-list-single").show();
						if ( val ) {
							$(".fw-sc-nav-list-single").filter(function(){
								return $(this).text().indexOf(val) == -1;
							}).hide();
						}
					});

					//editor.insertContent( $("#fw-shortcode-preview").text() );

					return;

					var win = tinyMCE.activeEditor.windowManager.open({
				        title: 'Browse shortcodes',			        
				        html : $("#fw-sc-lib").html(),
				        height: 400,
				        width: 700,				        
				        inline: 1,
				        resize : true,
				        buttons: [
				        	{
				        		text: "Insert", 
				        		onclick: function() {
				        			
				        			win.close();
				        		}
				        	}
				        ]
					}, {});				
				}
			});
		});






	})
})(jQuery);

