(function($) {

	$(function() {

		var updateData = function(){
			$("#fw-page-sections .fw-editor-section-single").each(function(i){					
				var t = $(this);
				$("input.fw-editor-section-data", t).remove();
				var options = $(":input" , t).serializeJSON();
				var data = {
					options : options,
					slug : t.attr("data-slug")
				};
				var input = fw.elem("input", "fw-editor-section-data").attr({
					"type" : "hidden",
					"name" : "fw-sections[" + i + "]",
					"value" : JSON.stringify(data)
				}).appendTo(t);
			});
		};

		$(document).on("change click keyup", ".fw-editor-section-single :input", function(){
			updateData();
		});

		$("#fw-editor-sections [data-toggle='collapse-next']").click(function(e){
			e.stopPropagation();return false;
		})

		$( "#fw-page-sections" ).sortable({
			//revert: true
			stop : function(){
				updateData();
			},
			distance: 5
		});
		$( "#fw-editor-sections .fw-editor-section-single" ).draggable({
			connectToSortable: "#fw-page-sections",
			helper: "clone"
			//revert: "invalid"
		});
		$( ".fw-editor-section-single" ).disableSelection();

		// $(".fw-editor-section-single").each(function(){
		// 	var t = $(this);
		// 	t.css("width", "").width( t.width() );
		// 	$(window).resize(function(){
		// 		t.css("width", "").width( t.width() );
		// 	});
		// });
	});

})(jQuery);
