(function($) {

	window.fwc = window.fwc || {};

	fwc.wpImageInput = {
		init : function(input) {
			var btn = fw.elem("div", "button").text("Choose image").insertAfter(input);

			var uploader = wp.media.frames.file_frame = wp.media({
			    title: 'Media Library',
			    button: {
			        text: 'Choose File'
			    },
			    multiple: false
			});

			//When a file is selected, grab the URL and set it as the text field's value
			uploader.on('select', function() {
			    var url = uploader.state().get('selection').first().attributes.url;
			    input.attr("value", url);
			    input.siblings('.fw-wp-image-preview').attr("src", url);
			});

			btn.click(function(){
				uploader.open();
			});

			
		}
	}

})(jQuery);
