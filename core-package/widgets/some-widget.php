<?php 
	/**
	 * @title My title
	 * @description Test widget
	 * @option name : Display text | default : My text | Text you want to display
	 * @option name : Bold | default : false | type : checkbox | Should it be bold?
	 * @option name : Color | default : red | type : select | options : red,green,blue | Choose the color?
	 */
?>

<?php if ($bold == "on"): ?>
	<strong style="color: <?php echo $color ?>"><?php echo $displayText ?></strong>
<?php else: ?>
	<span style="color: <?php echo $color ?>"><?php echo $displayText ?></span>
<?php endif ?>