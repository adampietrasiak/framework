<?php 

	/**
	 * @name Developer Menu
	 * @group Framework
	 */

?>

<div class="fw-center-box">
	<h2>
		<strong>Framework</strong> <?php _e("menu") ?>
	</h2>
	<p><?php _e("Click button below to create basic files structure for framework.", "fw") ?></p>
	<p><?php _e("After structure is created, you'll see new admin pages, notices and other features. Explore template directory to see all files explained. Feel free to modify them or remove.", "fw") ?></p>

	<hr/>

	<p>
		<input id="fw-create-structure-hard" type="checkbox">
		<label for="fw-create-structure-hard"><?php _e("Overwrite existing theme files.") ?></label>
	</p>

	<p class="secondary">
		<button class="button button-primary button-hero fw-import-btn" data-action="fw/dev/scaffolding/create" data-confirm="<?php _e("Are you sure?", "fw") ?>" id="fw-create-structure"><span class="va-m dashicons dashicons-download"></span> <?php _e("Create structure", "fw") ?></button>
	</p>

	<p class="tertiary">
		<strong><?php _e("Warning!", "fw") ?></strong> <?php _e("Disable this Framework menu in production mode.", "fw") ?>
	</p>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($) {

		$("#fw-create-structure").click(function(){
			var t = $(this);

			if ( t.data("confirm") && !confirm( t.data("confirm") ) ) return false;

			var hard = $("#fw-create-structure-hard").is(":checked");

			t.addClass('tertiary');

			fw.ajax( "fw/dev/scaffolding/create" , { hard : hard } ).always(function(){
				t.removeClass('tertiary');
			});
		});

	});
</script>


<div class="fw-center-box">
	<h2>
		<?php echo sprintf( __( "<strong>%s</strong> data importer." , "fw" ) , wp_get_theme() ); ?>
	</h2>
	<p><?php _e("Click button below to import prepared data for this theme.", "fw") ?></p>

	<hr/>

	<p class="secondary">
		<?php if ( \Fw\Importer::canLoad() ): ?>
			<p>
				<button class="button button-primary button-hero fw-import-btn" data-confirm="<?php _e("Are you sure?", "fw") ?>" data-success="<?php _e("Content was successfully imported.", "fw") ?>" data-action="fw/import/load" data-progress="<?php _e("Importing data...", "fw") ?>" id="fw-import"><span class="va-m dashicons dashicons-download"></span> <?php _e("Import Data", "fw") ?></button>
			</p>
		<?php endif ?>
		<?php if ( \Fw\Importer::canSave() ): ?>
			<p>
				<button class="button button-default button-hero fw-import-btn" data-confirm="<?php _e("Are you sure?", "fw") ?>" data-success="<?php _e("Content was successfully saved", "fw") ?>" data-action="fw/import/save" data-progress="<?php _e("Saving current data...", "fw") ?>" id="fw-export"><span class="va-m dashicons dashicons-upload"></span> <?php _e("Save current state to import", "fw") ?></button>
			</p>
		<?php endif ?>
	</p>

	<p class="fw-import-status">
		<span class="fw-import-status-text"></span>
		<span class="spinner"></span>
	</p>
	<p class="tertiary">
		<strong><?php _e("Warning!", "fw") ?></strong> <?php _e("After you import content, some of your actual content might be lost.", "fw") ?>
	</p>
</div>



<script type="text/javascript">
	jQuery(document).ready(function($) {

		$("#fw-import").click(function(){
			var t = $(this);

			if ( this.block ) {
				return false;
			}
			this.block = true;

			var data = t.data();

			var status = $(".fw-import-status")
			var statusText = $(".fw-import-status-text", status).text( data.progress );
			var spinner = $(".spinner", status).addClass('visible');

			fw.ajax( t.data("action") ).success(function(){
				fw.updateAttachments(function(progress){
					statusText.text(progress + "%");
				}).done(function(){
					statusText.text(data.success);
				}).always(function(){
					spinner.removeClass('visible');
					this.block = false;
				});
			});


		});

		$("#fw-export").click(function(){
			var t = $(this);

			if ( this.block ) {
				return false;
			}
			this.block = true;

			var data = t.data();

			var status = $(".fw-import-status")
			var statusText = $(".fw-import-status-text", status).text( data.progress );
			var spinner = $(".spinner", status).addClass('visible');

			fw.ajax( t.data("action") ).success(function(){
				statusText.text(data.success);
			}).always(function(){
				spinner.removeClass('visible');
				this.block = false;
			});

		});

	});
</script>