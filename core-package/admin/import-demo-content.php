<?php 

	/**
	 * @name Demo content
	 * @group Import
	 */

?>

<?php if ( !\Fw\Importer::canLoad() && !\Fw\Importer::canSave() ) return false; ?>


<div class="fw-center-box">
	<h2>
		<?php echo sprintf( __( "<strong>%s</strong> data importer." , "fw" ) , wp_get_theme() ); ?>
	</h2>
	<p><?php _e("Click button below to import prepared data for this theme.", "fw") ?></p>

	<hr/>

	<p class="secondary">
		<?php if ( \Fw\Importer::canLoad() ): ?>
			<button class="button button-primary button-hero fw-import-btn" data-confirm="<?php _e("Are you sure?", "fw") ?>" data-success="<?php _e("Content was successfully imported.", "fw") ?>" data-action="fw-load-import" data-progress="<?php _e("Importing data...", "fw") ?>" id="fw-import"><span class="va-m dashicons dashicons-download"></span> <?php _e("Import Data", "fw") ?></button>
		<?php endif ?>
		<?php if ( \Fw\Importer::canSave() ): ?>
			<button class="button button-default button-hero fw-import-btn" data-confirm="<?php _e("Are you sure?", "fw") ?>" data-success="<?php _e("Content was successfully saved", "fw") ?>" data-action="fw-save-import" data-progress="<?php _e("Saving current data...", "fw") ?>" id="fw-export"><span class="va-m dashicons dashicons-upload"></span> <?php _e("Save current state to import", "fw") ?></button>
		<?php endif ?>
	</p>

	<p class="fw-import-status">
		<span class="fw-import-status-text"></span>
		<span class="spinner"></span>
	</p>
	<p class="tertiary">
		<strong><?php _e("Warning!", "fw") ?></strong> <?php _e("After you import content, some of your actual content might be lost.", "fw") ?>
	</p>
</div>



<script type="text/javascript">
	jQuery(document).ready(function($) {

		$("#fw-import").click(function(){
			var t = $(this);

			if ( this.block ) {
				return false;
			}
			this.block = true;

			var data = t.data();

			var status = $(".fw-import-status")
			var statusText = $(".fw-import-status-text", status).text( data.progress );
			var spinner = $(".spinner", status).addClass('visible');

			fw.ajax("fw-load-import").success(function(){
				fw.updateAttachments(function(progress){
					statusText.text(progress + "%");
				}).done(function(){
					statusText.text(data.success);
				}).always(function(){
					spinner.removeClass('visible');
					this.block = false;
				});
			});


		});

		$("#fw-export").click(function(){
			var t = $(this);

			if ( this.block ) {
				return false;
			}
			this.block = true;

			var data = t.data();

			var status = $(".fw-import-status")
			var statusText = $(".fw-import-status-text", status).text( data.progress );
			var spinner = $(".spinner", status).addClass('visible');

			fw.ajax("fw-save-import").success(function(){
				statusText.text(data.success);
			}).always(function(){
				spinner.removeClass('visible');
				this.block = false;
			});

		});

	});
</script>