<?php

	/*
	 *
	 * @name Sections template
	 */

?>

<?php
	$sections = get_post_meta( get_the_id() , "fw-sections", true );

	if ( !count($sections) ) {
		\Fw\Section::render("fw/regular-content");
	} else {
		foreach ( $sections as $section ) {
			\Fw\Section::render($section["slug"], $section["options"]);
		}
	}
?>