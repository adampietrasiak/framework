<?php 

	namespace Fw;

	//FRAMEWORK CONSTANTS. 

	if ( !defined("DS") ) define( "DS" , DIRECTORY_SEPARATOR );


	/** VERSION **/
	define( "FW_VERSION" , "1.1" );
	define( "FW_CHECK_PHP_VERSION" , true );
	define( "FW_REQUIRED_PHP_VERSION" , "5.3.28" );


	/** DIRS AND TEMPLATE **/
	define( "FW_DIR" , __DIR__ );
	define( "FW_URI" , Common::wpDirToUri( FW_DIR ) );
	
	define( "FW_TEMPLATE_DIR" , \get_template_directory() ); //detect child theme
	define( "FW_TEMPLATE_URI" , Common::wpDirToUri(FW_TEMPLATE_DIR) );

	define( "FW_IS_CHILD_THEME" , \is_child_theme() );
	
	define( "FW_CHILD_TEMPLATE_DIR" , FW_IS_CHILD_THEME ? \get_stylesheet_directory() : false  );
	define( "FW_CHILD_TEMPLATE_URI" , FW_IS_CHILD_THEME ? Common::wpDirToUri(FW_CHILD_TEMPLATE_DIR) : false );

	define( "FW_CORE_DIR" , FW_DIR . DS . "core" );
	define( "FW_PACKAGES_DIR" , FW_DIR . DS . "packages" );
	define( "FW_LIBS_DIR" , FW_DIR . DS . "libs" );
	
	define( "FW_ASSETS_DIR" , FW_DIR . DS . "core-package" );
	define( "FW_ASSETS_URI" , Common::wpDirToUri(FW_ASSETS_DIR) );
	
	define( "FW_JS_DIR" , FW_ASSETS_DIR . DS . "js" );
	define( "FW_CSS_DIR" , FW_ASSETS_DIR . DS . "css" );


	/** FW SETTINGS **/
	define( "FW_DEBUG" , false );
	define( "FW_SUPER_DEV" , true ); //allows some additional features like importing all
	define( "FW_OPTIONS_KEY" , "fw-options" );
	define( "FW_ALLOW_TEMPLATE_AUTOCREATE" , true );
	

	/** OUTPUT **/
	define( "FW_AUTO_INCLUDE_HEADER_FOOTER" , true );
	define( "FW_ENABLE_CUSTOM_LOGIN_REGISTER" , true );
	define( "FW_SMART_HEAD" , true );

	

	/** WORDPRESS SETTINGS **/
	define( "FW_EXCERPT_LENGTH" , 120 );
	define( "FW_EXCERPT_MORE" , "..." );
	define( "FW_HIDE_WP_ADMIN_BAR" , false );	


	/** AJAX **/
	define( "FW_ENABLE_PAGE_AJAX" , true );
	define( "FW_IS_AJAX_PAGE" , !is_admin() && isset($_POST["fw-ajax"]) && $_POST["fw-ajax"] == "true" ); //defines if requested ajax page (usually means ommiting header and footer of requested page html)			


	/** APIS **/
	define( "FW_GOOGLE_API_KEY" , "AIzaSyAe6S72vIfHzhPgTNSQuPgQS2NIN76zq6Y" );	


	/** ASSETS **/
	define( "FW_NEEDS" , "jquery,comment-reply" );











