<?php 

	namespace Fw;

	//this component load fonts and auto create proper css of @font-face
	class Fonts extends Component {

		static $fonts = array();

		static $fontFiles = array();

		static $dir = "fonts";
		static $extensions = array("eot", "woff", "ttf", "svg");

		public static function afterInitAll() {


			
			//Partial::render("fw/core/fonts-css")

		}

		public static function fontData($file) {

			
			
			$style = "regular";
			$fontWeight = "normal";

			$pi = pathinfo($file);

			$basename = strtolower($pi["basename"]);
			$filename = $pi["filename"];
			$format = $pi["extension"];

			

			$font = str_replace( "-italic." , "." , strtolower($basename) );
			$font = str_replace( "-bold." , "." , $font);
			$font = str_replace( "-bolder." , "." , $font);
			$font = str_replace( "-light." , "." , $font);
			$font = str_replace( "-lighter." , "." , $font);




			$font = strtr( (string) $font , array("-bold." => ".", "-bolder." => ".", "-light." => ".", "-lighter." => ".") );

			$family = preg_replace( "/(-bold|-bolder|-regular|-italic|-lighter|-light)+/i" , "", $pi["filename"]);


			if ( strpos( $basename , "-italic." ) ) $style = "italic";

			

			if ( strpos( $basename , "-bold" ) ) $fontWeight = "bold";
			if ( strpos( $basename , "-light" ) ) $fontWeight = "light";
			if ( strpos( $basename , "-bolder" ) ) $fontWeight = "bolder";
			if ( strpos( $basename , "-lighter" ) ) $fontWeight = "lighter";



			$finalData = array(
				"uri" => Common::wpDirToUri( $file ),
				"file" => $file,
				"font-family" => $family,
				"font-weight" => $fontWeight,
				"font-style" => $style,
				"format" => $format
			);


			return $finalData;

		}

		public static function init($file) {
			
			self::$fontFiles[] = $file;
			$fontData = self::fontData($file);
			static::$fonts[$fontData["font-family"]][$fontData["font-weight"]][$fontData["font-style"]][$fontData["format"]] = $fontData;
		}
	}

?>