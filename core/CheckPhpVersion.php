<?php 

	

	//returns if php is enought or no
	function fw_check_php_version() {
		return !version_compare(phpversion(), FW_REQUIRED_PHP_VERSION, '<');
	}

	function fw_old_php_notice_render() {

		//get theme using fw
		$theme = $GLOBALS["fw-theme"];

		?>
			<div class="update-nag">
				
				<h3>
					<?php echo sprintf( __("<strong>'%s'</strong> theme cannot be activated :(", "fw") , $theme->get("Name") ? $theme->get("Name") : "This" ); ?>
				</h3>
				
				<p>
					<?php echo sprintf( __("Your server PHP version is <strong>%s (%s)</strong> and <strong>%s</strong> theme requires <strong>%s</strong> PHP version. Update PHP version or contact your server administrator <strong>%s</strong>.", "fw") , phpversion() , phpdate("M Y") , $theme->get("Name") , FW_REQUIRED_PHP_VERSION , getenv("SERVER_ADMIN") ? "(" . getenv("SERVER_ADMIN") . ")" : "" ); ?>
				</p>

				<p>
					<?php echo sprintf( __("Theme you were using before - <strong>%s</strong> is now activated.", "fw") , wp_get_theme() ); ?>
				</p>

				<?php if ( $theme->get("AuthorURI") ): ?>
					<p>
						<a href="<?php echo $theme->get("AuthorURI") ?>" target="_blank" class="button"><?php _e("Visit theme author website", "fw") ?></a>
					</p>
				<?php endif ?>
				
			</div>
		<?php

	}

	function fw_old_php_notice() {

		//get theme that we're trying to activate (one using framework)
		$oldTheme = \get_option('theme_switched');

		//get theme that was active before and save it to global so we can access it's info later
		$GLOBALS["fw-theme"] = wp_get_theme();
		
		//add notice in admin panel using info in GLOBALS
		\add_action("admin_notices" , "fw_old_php_notice_render");

		//switch to theme that was active before
		\switch_theme($oldTheme);


	} 



?>