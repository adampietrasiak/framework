<?php 

	namespace Fw;

	//this componenet adds new pages to admin panel menu
	class AdminPages extends Component {

		static $dir = "admin";

		static $topLevelCreated = array();

		static function init($file, $config) {


			//get slug of page
			$slug = Common::fileSlug($file);
			//get humanized name of page
			$name = !empty($config["name"]) ? $config["name"] : Common::humanize($slug);

			//we need to get html in advance to see if it returns false (to disable menu item)
			$html = get_partial($file);

			//if nothing returned or false dont add menu item
			if ( !$html || !trim( $html ) ) return;

			//set default icon
			$icon = !empty( $config["icon"] ) ? $config["icon"] : "admin-page";

			//add menu page
			add_action("admin_menu", function() use($file, $config, $name, $slug, $html) {

				//check capability
				$capability = !empty($config["capability"]) ? $config["capability"] : "manage_options";

				//if menu group exists
				if ( !empty($config["group"]) ) {
					//slug for root item
					$adminSlug = "admin-" . Common::slugify( $config["group"] );

					//add top level only once
					if ( !in_array( $adminSlug , self::$topLevelCreated ) ) {
						self::$topLevelCreated[] = $adminSlug; //mark as added top level
						//add menu page - fake capability to disable same sub-page as root-page
						\add_menu_page( $config["group"], $config["group"], "nosuchcapability", $adminSlug, function(){} , "dashicons-category" );
					}
					//when root added, add sub-page
					\add_submenu_page( $adminSlug, $name, $name, $capability, $slug, function() use($html) { echo $html; } );

				} else {

					//if no group - add to theme menu
					\add_theme_page( $name , $name , $capability , $slug , function() use($html) {
						echo $html;
					});
				}

			});

			//add main topbar button with theme name
			$parent = WpUtil::addAdminBarItem( __("Theme", "fw")  , \get_admin_url() , "site-name" , false , 1);

			//if group, add top level menu
			if ( !empty($config["group"]) ) {
				$parent = WpUtil::addAdminBarItem( $config["group"] , "#" , $parent , "portfolio" );
			}

			//add topbar item (if had group - add to sub-menu)
			WpUtil::addAdminBarItem($name, self::getUrl($slug , empty($config["group"]) ) , $parent , $icon);

		}

		static function getUrl($name , $themes = false) {
			if ( $themes ) return get_admin_url(null, "themes.php?page=$name" );
			return get_admin_url(null, "admin.php?page=$name" );
		}
	}

?>