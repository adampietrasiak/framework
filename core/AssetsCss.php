<?php 

	namespace Fw;

	//this component automatically load css files
	class AssetsCss extends Component {
		
		static $dir = "css";

		static $extensions = "css";

		static function afterInitAll() {
			
			\add_actions( array("wp_head" , "admin_enqueue_scripts" , "login_head") , function(){
				Partial::render("fw/core/fonts/style", array("fonts" => Fonts::$fonts));
			},9999999);

		}


		static function init($file, $config, $relative) {

			$relativeDir = dirname($relative);
			$base = Common::depsFileBase($file);
			$uri = Common::wpDirToUri($file);

			if ( $relativeDir == "assets" ) {

				WpUtil::autoRegister($uri, $base);

			} elseif ( Localizator::check( $relativeDir ) ) {
				WpUtil::autoEnqueue($uri, "global", false);
			}

		}
	}

 ?>