<?php 

	namespace Fw;
	
	//this class will analise given folder and its config to add proper post type
	//add page for single, for displaying (like blog), for search results page, search result single
	class PostTypes {

		public static function getPostTypeLabels($singular) {
			$plural = \Inflector::pluralize($singular);
			$singularLower = \strtolower($singular);
			$pluralLower = \strtolower($plural);
			return array(
				"name" => $plural,
				"singular_name" => __( \sprintf("%s", $singular) , "fw" ),
				"add_new" => __( \sprintf("Add New", $singularLower) , "fw" ),
				"add_new_item" => __( \sprintf("Add new %s", $singularLower) , "fw" ),
				"edit_item" => __( \sprintf("Edit %s", $singularLower) , "fw" ),
				"new_item" => __( \sprintf("New %s", $singularLower) , "fw" ),
				"view_item" => __( \sprintf("View %s", $pluralLower) , "fw" ),
				"search_items" => __( \sprintf("Search for %s", $pluralLower) , "fw" ),
				"not_found" => __( \sprintf( "No %s found", $pluralLower) , "fw" ),
				"not_found_in_trash" => __( \sprintf("No %s found in trash", $singularLower) , "fw" ),
				"parent_item_colon" => ""
			);
		}

		public static function getTaxonomyLabels($singular, $plural = false) {
			if ( !$plural ) $plural = \Inflector::pluralize($singular);
			$singularLower = \strtolower($singular);
			$pluralLower = \strtolower($plural);
			return array(
				'name'                       => $plural,
				'singular_name'              => $singular,
				'search_items'               => sprintf( __( 'Search for %s' , "fw" ) , $pluralLower ),
				'popular_items'              => sprintf( __( 'Popular %s' , "fw" ) , $pluralLower ),
				'all_items'                  => sprintf( __( 'All %s' , "fw" ) , $pluralLower ),
				'parent_item'                => sprintf( __( 'Parent %s' , "fw" ) , $singularLower ),
				'parent_item_colon'          => sprintf( __( 'Parent %s:' , "fw" ) , $singularLower ),
				'edit_item'                  => sprintf( __( 'Edit %s' , "fw" ) , $singularLower ),
				'update_item'                => sprintf( __( 'Update %s' , "fw" ) , $singularLower ),
				'add_new_item'               => sprintf( __( 'Add new %s' , "fw" ) , $singularLower ),
				'new_item_name'              => sprintf( __( 'New %s name' , "fw" ) , $singularLower ),
				'separate_items_with_commas' => sprintf( __( 'Separate %s with commas' , "fw" ) , $pluralLower ),
				'add_or_remove_items'        => sprintf( __( 'Add or remove %s' , "fw" ) , $pluralLower ),
				'choose_from_most_used'      => sprintf( __( 'Choose from the most used %s' , "fw" ) , $pluralLower ),
				'not_found'                  => sprintf( __( 'No %s found' , "fw" ) , $pluralLower ),
				'choose_from_most_used'      => sprintf( __( 'Choose from the most used %s' , "fw" ) , $pluralLower ),
			);
		}

		public static function registerPostType($niceName, $slug, $data) {

			//$slug = Common::camelize($slug); //wp dont accept dashes...

			if ( !\post_type_exists($slug) ) {
				$labels = self::getPostTypeLabels($niceName);

				$hierarchical = isset($data["hierarchical"]) ? $data["hierarchical"] == "true" : false;

				$thumbnail = isset($data["thumbnail"]) ? $data["thumbnail"] == "true" : true;
				$excerpt = isset($data["excerpt"]) ? $data["excerpt"] == "true" : true;
				$comments = isset($data["comments"]) ? $data["comments"] == "true" : true;
				$categories = isset($data["categories"]) ? $data["categories"] == "true" : true;
				$tags = isset($data["tags"]) ? $data["tags"] == "true" : true;
				$icon = isset($data["icon"]) ? "dashicons-" . $data["icon"] : null;
				
				$supports = array('title','editor');

				if ( $thumbnail ) $supports[] = "thumbnail";
				if ( $excerpt ) $supports[] = "excerpt";
				if ( $comments ) $supports[] = "comments";

				$taxonomies = array();

				if ( $categories ) $taxonomies[] = "category";
				if ( $tags ) $taxonomies[] = "post_tag";
				
				$args = array(
					'labels' => $labels,
					'public' => true,
					'publicly_queryable' => true,
					'show_ui' => true,
					'query_var' => true,
					'rewrite' => true,
					'capability_type' => 'post',
					'hierarchical' => $hierarchical,
					'menu_position' => null,
					'supports' => $supports,
					'taxonomies' => $taxonomies,
					'has_archive' => true,
					'menu_icon' => $icon
				  ); 
				
				\register_post_type( $slug , $args );
			} else {

				add_action( 'admin_init' , function() use( $data, $slug ) {
					if ( !empty($data["categories"]) && Common::maybeBool( $data["categories"] ) ) \register_taxonomy_for_object_type('category', $slug);
					if ( !empty($data["tags"]) && Common::maybeBool( $data["tags"] ) ) \register_taxonomy_for_object_type('post_tag', $slug);
				});

			}


		}

		public static function registerMetabox($niceName, $slug, $data) {
			if ( isset($data["meta"]) && is_array($data["meta"]) && count($data["meta"]) ) {


				$postType = $data["@node"];

				$nicePostType = Common::humanize($postType);

				$meta = $data["meta"];
				//add slugs to meta
				foreach ( $meta as $i => $singleMeta ) {
					$meta[$i]["slug"] = isset($meta[$i]["slug"]) ? $meta[$i]["slug"] : \Fw\Common::slugify( $meta[$i]["name"] );
				}


				$id = "fw-meta-box-" . $slug;
				$title = __("$nicePostType options", "fw");
				$postType = $slug;
				$callback = function($post) use($data, $postType, $meta) {
					$id = $post->ID;
					PostTypes::renderMetaBox($meta, $postType, $id);
				};

				//add saving action
				add_action( 'save_post', function($id) use($meta, $postType) {
					
					$actualPostType = isset($_POST['post_type']) ? $_POST['post_type'] : "post";
					if ( $postType == $actualPostType ) {

						foreach ( $_POST as $key => $val ) {
							if ( \Fw\Common::startsWith($key, "fw-meta-") ) {
								$metaKey = \Fw\Common::removePrefix($key, "fw-meta-");									
								update_post_meta( $id, $metaKey, $val );
							}
						}
					}
					
				});

				add_action( "add_meta_boxes" , function() use($id, $title, $callback, $postType){
					\add_meta_box( $id, $title, $callback, $postType );
				});
				
			}
		}

		public static function registerTaxonomy($config, $postType) {

			if ( !\post_type_exists($postType) ) return false;

			extract($config);

			if ( $name == "tag" || $name == "tags" || $name == "post_tag" ) {
				add_action( 'admin_init' , function() use( $postType ) {
					\register_taxonomy_for_object_type('post_tag', $postType);
				});
				return;
			}

			if ( $name == "category" || $name == "categories" ) {
				add_action( 'admin_init' , function() use( $postType ) {
					\register_taxonomy_for_object_type('category', $postType);
				});
				return;
			}

			if ( empty($label) ) $label = Common::humanize($name);
			if ( empty($slug) ) $slug = Common::slugify($name);
			if ( empty($plural) ) $plural = \Inflector::pluralize($label);


			\register_taxonomy( $slug , $postType , array(
				"label" => $label,
				"labels" => self::getTaxonomyLabels($label, $plural),
				"hierarchical" => !isset($tree) || Common::maybeBool($tree)
			));

		}



		public static function renderMetaBox($meta, $postType, $id) {


			//render options
			\Fw\Partial::render("fw/meta/box", array(
				"meta" => $meta,
				"type" => $postType,
				"id" => $id
			));
		}


	}
 ?>