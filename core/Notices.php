<?php 

	namespace Fw;

	//this component adds admin panel notices
	class Notices extends Component {

		static $dir = "notices";

		static function init($file, $config) {

			$config = (array) $config;
			$config["message"] = get_partial($file);


			if ( !empty($config["once"]) && $config["once"] ) {
				add_action("after_switch_theme", function() use($config) {
					WpUtil::notice($config);
				});
			} else {
				WpUtil::notice($config);
			}

			
		}
	}



?>