<?php 
	
	namespace Fw;

	//this class is able to get informations about packages inside theme
	Class PackageManager {



		public static function getExternalPackagesFiltered() {
			$packages = self::getExternalPackages();

			foreach ( $packages as $path => $name ) {
				$filterPath = $path . DS . "use-package.php";

				if ( is_file($filterPath) ) {
					$passedFilter = include $filterPath;

					if ( !is_null($passedFilter) && $passedFilter !== true ) {
						unset( $packages[$path] );
					}	
				}
			}

			return $packages;
		}

		public static function getExternalPackages() {
			$themePackagesDir = FW_TEMPLATE_DIR . DS . "packages";


			$fwPackages = is_dir(FW_PACKAGES_DIR) ? scandir(FW_PACKAGES_DIR) : array();
			$themePackages = is_dir($themePackagesDir) ? scandir($themePackagesDir) : array();

			$all = array();
			foreach ( $fwPackages as $package ) {
				if ( $package == "." || $package == ".." ) continue;
				if ( is_dir(FW_PACKAGES_DIR . DS . $package) ) {
					$all[FW_PACKAGES_DIR . DS . $package] = $package;
				}
			}

			foreach ( $themePackages as $package ) {
				if ( $package == "." || $package == ".." ) continue;
				if ( is_dir($themePackagesDir . DS . $package) ) {
					$all[$themePackagesDir . DS . $package] = $package;
				}
			}

			$all = array_diff((array)$all, array(".", ".."));

			$all[ FW_TEMPLATE_DIR ] = "template";

			return $all;
		}

		//get all packages list
		public static function getPackages() {

			$packages = self::getExternalPackages();
			$packages[ FW_TEMPLATE_DIR ] = "template";

			return $packages;
		}

		public static function getPackagesFiltered() {
			$packages = self::getExternalPackagesFiltered();
			$packages[ FW_TEMPLATE_DIR ] = "template";

			return $packages;
		}

		public static function loadLibs() {
			//get all libs
			$libs = scandir(FW_LIBS_DIR);
			//auto require files of libs (folder and init file name must be the same - dir-to-libs/SameName/SameName.php)
			foreach( $libs as $lib ) {
				$libInitFile = FW_LIBS_DIR . DS . $lib . DS . $lib . ".php";
				if ( is_file( $libInitFile ) ) {
					require_once($libInitFile);
				}
			}
		}

	}

?>