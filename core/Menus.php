<?php 

	namespace Fw;

	//this component adds menu areas
	class Menus extends Component {

		static $menus = array();

		static $dir = "menus";

		static function init($file, $config) {
			$slug = Common::fileSlug($file);

			$name = !empty($config["name"]) ? $config["name"] : Common::humanize($slug);

			register_nav_menu( $slug, $name );
			self::$menus[$slug] = $file;

		}

		public static function render($slug) {
			if ( isset(self::$menus[$slug]) ) {
				$path = self::$menus[$slug];

				$items = Menu::find($slug)->items();
				
				Partial::render($path, array(
					"items" => $items
				));
			}
		}
	}
?>