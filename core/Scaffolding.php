<?php 


	namespace Fw;

	class Scaffolding {

		public static function generateStructure($overwrite = false) {

			$structureToCreate = \Fw\Common::getFlattenDirTree( FW_DIR . DS . "scaffold" );


			foreach ( $structureToCreate as $relative => $full ) {
				if ( Common::pathinfo($full , "extension") ) {

					$content = file_get_contents($full);

					\Fw\WpUtil::saveFile( FW_TEMPLATE_DIR . DS . $relative , $content , $overwrite );

				} else {
					\Fw\WpUtil::makeThemeDir($relative);
				}
				
			}

		}

	}

 ?>