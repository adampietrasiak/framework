<?php 

	namespace Fw;

	class Variables extends Component {
		static $dir = "variables";

		static $variables = array();

		static $extensions = array("php");

		static function init($file) {

			$value = include $file;	

			$pi = pathinfo($file);

			$name = $pi["filename"];
			static::$variables[$name] = $value;

		}
	}

 ?>