<?php 
	
	namespace Fw;

	define("FW_CACHE_MINUTE", 60);
	define("FW_CACHE_QUATER", 60 * 15);
	define("FW_CACHE_HOUR", 60 * 60);
	define("FW_CACHE_DAY", 60 * 60 * 24);
	define("FW_CACHE_WEEK", 60 * 60 * 24 * 7);
	define("FW_CACHE_MONTH", 60 * 60 * 24 * 7 * 30);

	//example of use
	// echo Cache::get("cache-test", function(){
	// 	return file_get_contents("http://www.phpfastcache.com/testing.php");
	// });

	class PhpCache {

		static $callbacks = array();

		public static function get($key, $function = false, $time = FW_CACHE_QUATER) {
			$cached = \phpFastCache::get($key);
			if ( $cached !== null ) return $cached;
			if ( is_callable( $function ) ) {
				$cached = $function();
				\phpFastCache::set($key,$cached,$time);
				self::registerCallback($key,$function);
				return $cached;
			} elseif ( is_callable(self::$callbacks[$key]) ) {
				$function = self::$callbacks[$key];
				$cached = $function();
				\phpFastCache::set($key,$cached,$time);
				return $cached;
			} else {
				return null;
			}
		}

		public static function set($key, $val, $time = FW_CACHE_QUATER) {
			\phpFastCache::set($key,$val,$time);
		}

		public static function delete($key) {
			\phpFastCache::delete($key);
		}

		public static function clearCache($sure = false) {
			if ( $sure ) \phpFastCache::clean();
		}

		public static function touch($key, $time = FW_CACHE_QUATER) {
			return \phpFastCache::touch($key, $time);
		}

		public static function has($key) {
			return \phpFastCache::exists($key);
		}
		//add method of getting given key
		public static function registerCallback($key, $cb){
			if ( is_callable($cb) ) {
				self::$callbacks[$key] = $cb;
			}
		}
	}


?>