<?php 

	namespace Fw;

	class MultipleFeatured {

		static $register = array();

		const FW_SLUG_BASE = "fw-featured-image";

		// public '_thumbnail_id' => string '144' (length=3)
		// public 'post_fw-featured-image-2_thumbnail_id' => string '142' (length=3)

		

		public static function addNFeatured($postType, $numberOfFeatured = 1) {
			
			$num = (int) $numberOfFeatured;

			if ( $num > 0 ) {

				for ($i=1; $i <= $num; $i++) {

					$label = $i == 1 ? "Featured image" : "Featured image $i";
					self::addFeatured($postType , $label);
				}
			}
		}

		public static function addFeatured($postType, $name) {
			new \MultiPostThumbnails(
			    array(
			        'label' => Common::humanize($name),
			        'id' => Common::slugify($name),
			        'post_type' => $postType
			    )
			);
		}

		public static function getThumbnailsOfPost($id, $size = "full") {
			$type = get_post_type( $id );

			$first = wp_get_attachment_image_src( get_post_thumbnail_id( $id ), $size );

			$all = array($first);


			$key = 2;
			$metaKey = $type . "_" . self::FW_SLUG_BASE . "-" . $key . "_thumbnail_id";
			$featured = get_post_meta( $id, $metaKey , true );
			while ( $featured ) {
				$all[] = wp_get_attachment_image_src( (int) $featured , $size );
				$key++;
				$metaKey = $type . "_" . self::FW_SLUG_BASE . "-" . $key . "_thumbnail_id";
				$featured = get_post_meta( $id, $metaKey , true );
			}

			foreach ($all as $key => $single) {
				$obj = (object) array();
				$obj->url = $single[0];
				$obj->width = $single[1];
				$obj->height = $single[2];
				$obj->cssBg = "background-image: url(" . $obj->url . "); ";
				$all[$key] = $obj;
			}

			return $all;
		}

	}

 ?>