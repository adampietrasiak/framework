<?php 

	namespace Fw;

	//this component just include php files
	class Includes extends Component {
		static $dir = "includes";

		static $extensions = "php";

		static function init($file) {
			require_once($file);
		}
	}

 ?>