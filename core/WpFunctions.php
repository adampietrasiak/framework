<?php

function get_the_slug() {

	global $post;

	if ( is_single() || is_page() ) {
		return $post->post_name;
	}
	else {
		return "";
	}

}