<?php 

	namespace Fw;

	class Common {
		
		//returns array of files from given array respectin their dependencies in file names for example jquery.easing.myfile.js will be after jquery and easing files
		public static function resolveDependencies($files) {

			$waitingFiles = array();
			$lastFiles = array();
			$queue = array();

			foreach ( $files as $key => $filename ) {
				$info = pathinfo($filename);
				$pieces = explode('.' , $info['filename']);
				$name = array_pop($pieces);

				$deps = count($pieces) ? $pieces : false;
				if ( isset( $waitingFiles[$name] ) ) {
					$name = $name . "." . uniqid();
				}

				$info = array(
					'deps' => $deps,
					'name' => $name,
					'file' => $filename,
					"originalKey" => $key
				);

				if ( $deps[0] == "last" && count($deps) == 1 ) {
					$lastFiles[$name] = $info;
					continue;
				}

				$waitingFiles[$name] = $info;
			}
			unset($waitingFiles[""],$waitingFiles["."],$waitingFiles[".."]);

			$is_enqueued = function($name) use(&$is_enqueued, &$queue, &$waitingFiles) {				
				foreach ( $queue as $item ) {
					if ( $name == $item['name'] ) return true;
				}				
				return false;
			};

			$is_waiting = function($name) use(&$is_waiting, &$queue, &$waitingFiles) {	
				foreach ( $waitingFiles as $file ) {			
					if ( $name == $file['name'] ) return true;
				}
				return false;
			};

			$enqueue = function($name) use(&$enqueue, &$queue, &$waitingFiles) {						
				$queue[$waitingFiles[$name]["file"]] = $waitingFiles[$name];
				unset( $waitingFiles[$name] );
			};

			//resolve dependecies
			while ( count( $waitingFiles ) ) {
				$enqueued = false;

				foreach ( $waitingFiles as $file ) {
					$deps = $file['deps'];

					if ( !$deps ) {
						$enqueue($file['name']);
						$enqueued = true;						
						continue;
					}					
					$canqueue = true;
					foreach ( $deps as $dep ) {
						if ( $is_waiting($dep) ) {							
							$canqueue = false;					
						}
					}
					if ( $canqueue ) {
						$enqueue($file['name']);
						$enqueued = true;
					}
				}		
				if ( !$enqueued ) break;
			}

			$queue = array_merge( $queue , $lastFiles );
			return $queue;
		}

		//require every file in given dir (once)
		public static function requireDir($dir) {
			$files = scandir($dir);

			$depenenciesResolvedList = self::resolveDependencies($files);

			unset( $depenenciesResolvedList[".."] );

			foreach( $depenenciesResolvedList as $fileName => $depsData ) {
				$fullPath = $dir . DS . $fileName;
				$pathinfo = pathinfo($fileName);
				if ( is_file($fullPath) && $pathinfo["extension"] == "php" ) require_once($dir . DS . $fileName);
				if ( is_dir($fullPath) ) self::requireDir($fullPath);
			}
			return true;
		}

		public static function trimArray($arr) {
			if (!is_array($arr)){ return $arr; }
			
			while (list($key, $value) = each($arr)){
			    if (is_array($value)){
			        $arr[$key] = TrimArray($value);
			    }
			    else {
			        $arr[$key] = trim($value);
			    }
			}
			return $arr;
		}

		public static function fileUseFunction($file, $function) {
			$start = microtime(true);
			$content = \file_get_contents($file);
			$tokens = \token_get_all($content);

			if ( $tokens ) foreach ( $tokens as $token ) {
				if ( is_array($token) && $token[0] == T_STRING && $token[1] == $function ) return true;
			}


			return false;
		}

		public static function pathinfo($path, $what = "") {
			$pi = pathinfo($path);

			if ( $what && !empty($pi[$what]) ) {
				return $pi[$what];
			} else if ( $what && empty($pi[$what]) ) {
				return null;
			}

			return $pi;

		}

		public static function prepareSelectOptions($options) {
			$options = self::maybeCallback($options);

			if ( is_string($options) ) $options = explode(",", $options);

			$options = array_map('trim', (array) $options);
			return $options;
		}

		public static function prepareOptionData($data) {

			extract($data);
			unset($data);

			if ( empty($name) && !empty($title) ) $name = \Fw\Common::slugify($title);
			if ( empty($slug) && !empty($name) ) $slug = \Fw\Common::slugify($name);
			if ( empty($title) && !empty($name) ) {
				$title = \Fw\Common::humanize($name);
				$name = $slug;
			}
			
			if ( empty($id) ) $id = Common::slugify($slug);

			$id = Common::slugify($id);
			$slug = Common::slugify($slug);
			$name = Common::slugify($name);

			if ( !empty($type) && $type == "icon" ) {
				$type = "choose";
				$partial = "fw/modal/icons";
			}


			if ( !empty($namePrefix) ) $name = $namePrefix . $name;
			if ( !empty($nameSuffix) ) $name = $name . $nameSuffix;

			if ( empty($suboptions) && !empty($suboption) ) $suboptions = $suboption;
			if ( empty($placeholder) ) $placeholder = $title;

			//try to get from _POST or _GET
			if ( empty($value) && !empty($slug) ) $value = \fw_option( $slug );
			if ( empty($value) && !empty($name) ) $value = \fw_option( $name );
			if ( empty($value) && \Fw\Common::sentValue($name) !== null ) $value = \Fw\Common::sentValue($name);

			if ( !empty($multiple) && $multiple ) {
				$containerClass = !empty($containerClass) ? "fw-option-container-multiple " . $containerClass : "fw-option-container-multiple ";
			}

			return \get_defined_vars();
		}

		public static function getOutFromArray(&$array, $filter) {
			foreach ( $array as $key => $val ) {
				if ( $filter($val) ) {
					unset($array[$key]);
					return $val;
				}
			}
			return null;
		}

		public static function maybeBool($value) {

			if ( !is_scalar($value) ) return $value;

			$oryginalValue = $value;

			$value = strtolower((string) $value);
			if( $value == "on" || $value == "true" || $value == "1" || $value === true || $value == "yes" || $value == \strtolower(__("yes", "fw")) ) return true;
			if( $value == "off" || $value == "false" || $value == "0" || $value === false || $value == "no" || $value == \strtolower(__("no", "fw")) ) return false;

			return $oryginalValue;
		}


		public static function sentValue($key) {
			if ( !empty($_POST[$key]) ) return $_POST[$key];
			if ( !empty($_GET[$key]) ) return $_GET[$key];

			return null;
		}


		public static function maybeCallback($input) {

			if( is_string($input) ) $input = trim($input);

			if ( is_string($input) && self::startsWith($input, "=>") ) {
				 $func = \Fw\Common::removePrefix($input, "=>");
				 $func = \Fw\Common::removeSuffix($func, "()");

				 if ( is_callable($func) ) {
				 	$result = call_user_func($func);
				 } else {
				 	return $input;
				 }
			} elseif( is_array($input) ) {
				foreach( $input as $key => $val ) {
					$input[$key] = self::maybeCallback($val);
				}
			} else {
				return $input;
			}

			return !empty($result) ? $result : $input;

		}





		public static function moveToFrontOfArray(&$array, $filter) {
			$val = self::getOutFromArray($array, $filter);
			if ( $val ) array_unshift( $array , $val);
		}

		public static function addParamToUrl($url, $param, $val) {
			$query = parse_url($url, PHP_URL_QUERY);

			// Returns a string if the URL has parameters or NULL if not
			if( $query ) {
			    $url .= "&{$param}={$val}";
			}
			else {
			    $url .= "?{$param}={$val}";
			}

			return $url;
		}

		public static function addHttp($url) {
			if ( $url == "#" ) return $url;
			if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
			    $url = "http://" . $url;
			}
			return $url;
		}

		public static function combineFiles($files, $limiter = "\n\n\n") {
			$output = "";
			foreach ( (array) $files as $file) {
				if ( is_file($file) ) {
					$output .= file_get_contents($file) . (string) $limiter;
				}
			}
			return $output;
		}

		public static function combineFilesAndSave($files, $limiter = "\n\n\n", $targetFile, $suffix = "") {
			$output = self::combineFiles($files, $limiter);
			$output .= $suffix;

			$pi = pathinfo( $targetFile );

			self::mkdir( $pi["dirname"] );

			\file_put_contents( $targetFile , $output );
		}

		//adds stamps of modification times of given files. It's useful to check if any of given files has been changed comparing to value from before
		public static function getFilesModifySume($files) {
			$sume = 0;
			foreach ((array) $files as $file) {
				if ( is_file($file) ) $sume += \filemtime($file);
			}
			return $sume;
		}

		public static function mkdir($dir, $chmod = 0777) {
			if ( !is_dir($dir) ) { 
				$made = mkdir($dir, $chmod, true);
				if ( $made ) {
					chmod($dir, $chmod);
				}
				return $made;

			} else {
				return true;
			}

		}

		public static function depsFileBase($file) {
			$full = self::pathinfo($file , "filename");

			$parts = explode(".", $full);
			$base = array_pop($parts);
			
			return $base;
		}

		public static function timeAgo($datetime, $short = false) {
		    $now = new \DateTime;

		    if ( is_numeric($datetime) ) $datetime = date('m/d/Y', (int) $datetime);

		    $ago = new \DateTime($datetime);
		    $diff = $now->diff($ago);

		    $diff->w = floor($diff->d / 7);
		    $diff->d -= $diff->w * 7;

		    $string = array(
		        'y' => 'year',
		        'm' => 'month',
		        'w' => 'week',
		        'd' => 'day',
		        'h' => 'hour',
		        'i' => 'minute',
		        's' => 'second',
		    );

		    if ( $short ) {
		    	$string = array_map(function($a){
		    		return (string) $a[0];
		    	}, $string);
		    }

		    foreach ($string as $k => &$v) {
		        if ($diff->$k) {
		            $v = $diff->$k . ( $short ? '' : ' ') . $v . ($diff->$k > 1 && !$short ? 's' : '');
		        } else {
		            unset($string[$k]);
		        }
		    }

		    $ago = $short ? "" : " ago";

		    //if (!$full) $string = array_slice($string, 0, 1);
		    $string = array_slice($string, 0, 1);
		    return $string ? implode(', ', $string) . $ago : 'just now';
		}

		public static function arrayToHtmlAttr($arr) {
			if ( isset($arr) && is_array($arr) ) {
				$attrStr = " ";
				foreach ( $arr as $name => $val ) {
					$attrStr = $attrStr . " " . $name . "=" . "\"" . $val . "\" "; 
				}
				return $attrStr;
			}
			return "";
		}

		public static function fileSlug($path) {
			$pi = pathinfo($path);
			return self::slugify( $pi["filename"] );
		}

		public static function fileHumanize($path) {
			$pi = pathinfo($path);
			return self::humanize( $pi["filename"] );
		}		

		public static function arrayRandom($array, $seed = false) {
			if ( !$seed ) {
				return $array[array_rand($array)];
			} else {
				$l = count($array);				
				return $array[self::seedRandom($seed, 0 , $l - 1)];
			}			
		}

		public static function randomString($length = 32) {
		    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    $charactersLength = strlen($characters);
		    $randomString = '';
		    for ($i = 0; $i < $length; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }
		    return $randomString;
		}

		public static function seedRandom($seed, $min, $max){
			mt_srand($seed);
			return mt_rand($min, $max);
		}

		public static function isUrl($url) {
			if(filter_var($url, FILTER_VALIDATE_URL) === FALSE)
			{
			        return false;
			}else{
			        return true;
			}
		}

		//translate dir to wordpress url of same file
		public static function wpDirToUri($dir) {
			$wpContentDir = defined("WP_CONTENT_FOLDERNAME") ? WP_CONTENT_FOLDERNAME : "wp-content";
			preg_match_all("/(.+)(" . $wpContentDir . ".+)/", $dir, $parts);
			$home = get_home_url();
			$home = explode("/?", $home);
			$home = $home[0]; //make sure to remove get part
			$home = explode("?", $home); //make sure to remove get part
			$home = $home[0];

			$uri = $home . DS . $parts[2][0];			
			return preg_replace("/\\\/", "/", $uri);
		}

		public static function removeAccents( $string )
		{
		    if ( ! preg_match( '/[\x80-\xff]/', $string ) ) {
		        return $string;
		    }

		    if ( self::seems_utf8( $string ) ) {
		        $chars = array(

		            // Decompositions for Latin-1 Supplement
		            chr(194).chr(170) => 'a', chr(194).chr(186) => 'o',
		            chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
		            chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
		            chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
		            chr(195).chr(134) => 'AE',chr(195).chr(135) => 'C',
		            chr(195).chr(136) => 'E', chr(195).chr(137) => 'E',
		            chr(195).chr(138) => 'E', chr(195).chr(139) => 'E',
		            chr(195).chr(140) => 'I', chr(195).chr(141) => 'I',
		            chr(195).chr(142) => 'I', chr(195).chr(143) => 'I',
		            chr(195).chr(144) => 'D', chr(195).chr(145) => 'N',
		            chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
		            chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
		            chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
		            chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
		            chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
		            chr(195).chr(158) => 'TH',chr(195).chr(159) => 's',
		            chr(195).chr(160) => 'a', chr(195).chr(161) => 'a',
		            chr(195).chr(162) => 'a', chr(195).chr(163) => 'a',
		            chr(195).chr(164) => 'a', chr(195).chr(165) => 'a',
		            chr(195).chr(166) => 'ae',chr(195).chr(167) => 'c',
		            chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
		            chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
		            chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
		            chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
		            chr(195).chr(176) => 'd', chr(195).chr(177) => 'n',
		            chr(195).chr(178) => 'o', chr(195).chr(179) => 'o',
		            chr(195).chr(180) => 'o', chr(195).chr(181) => 'o',
		            chr(195).chr(182) => 'o', chr(195).chr(184) => 'o',
		            chr(195).chr(185) => 'u', chr(195).chr(186) => 'u',
		            chr(195).chr(187) => 'u', chr(195).chr(188) => 'u',
		            chr(195).chr(189) => 'y', chr(195).chr(190) => 'th',
		            chr(195).chr(191) => 'y', chr(195).chr(152) => 'O',

		            // Decompositions for Latin Extended-A
		            chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
		            chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
		            chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
		            chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
		            chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
		            chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
		            chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
		            chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
		            chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
		            chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
		            chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
		            chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
		            chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
		            chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
		            chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
		            chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
		            chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
		            chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
		            chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
		            chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
		            chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
		            chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
		            chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
		            chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
		            chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
		            chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
		            chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
		            chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
		            chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
		            chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
		            chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
		            chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
		            chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
		            chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
		            chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
		            chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
		            chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
		            chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
		            chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
		            chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
		            chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
		            chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
		            chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
		            chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
		            chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
		            chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
		            chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
		            chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
		            chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
		            chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
		            chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
		            chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
		            chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
		            chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
		            chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
		            chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
		            chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
		            chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
		            chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
		            chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
		            chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
		            chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
		            chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
		            chr(197).chr(190) => 'z', chr(197).chr(191) => 's',

		            // Decompositions for Latin Extended-B
		            chr(200).chr(152) => 'S', chr(200).chr(153) => 's',
		            chr(200).chr(154) => 'T', chr(200).chr(155) => 't',

		            // Euro Sign
		            chr(226).chr(130).chr(172) => 'E',
		            // GBP (Pound) Sign
		            chr(194).chr(163) => ''
		        );

		        $string = strtr( $string, $chars );
		    } else {

		        // Assume ISO-8859-1 if not UTF-8
		        $chars['in'] = chr(128).chr(131).chr(138).chr(142).chr(154).chr(158)
		             .chr(159).chr(162).chr(165).chr(181).chr(192).chr(193).chr(194)
		             .chr(195).chr(196).chr(197).chr(199).chr(200).chr(201).chr(202)
		             .chr(203).chr(204).chr(205).chr(206).chr(207).chr(209).chr(210)
		             .chr(211).chr(212).chr(213).chr(214).chr(216).chr(217).chr(218)
		             .chr(219).chr(220).chr(221).chr(224).chr(225).chr(226).chr(227)
		             .chr(228).chr(229).chr(231).chr(232).chr(233).chr(234).chr(235)
		             .chr(236).chr(237).chr(238).chr(239).chr(241).chr(242).chr(243)
		             .chr(244).chr(245).chr(246).chr(248).chr(249).chr(250).chr(251)
		             .chr(252).chr(253).chr(255);

		        $chars['out'] = 'EfSZszYcYuAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy';

		        $string = strtr( $string, $chars['in'], $chars['out'] );
		        $double_chars['in'] = array( chr(140), chr(156), chr(198), chr(208), chr(222), chr(223), chr(230), chr(240), chr(254) );
		        $double_chars['out'] = array( 'OE', 'oe', 'AE', 'DH', 'TH', 'ss', 'ae', 'dh', 'th' );
		        $string = str_replace( $double_chars['in'], $double_chars['out'], $string );
		    }

		    return $string;
		}

		public static function caller($depth = 0) {
			$e = new \Exception();
			$trace = $e->getTrace();
			//position 0 would be the line that called this function so we ignore it
			$last_call = $trace[2 + (int) $depth];

			foreach ($last_call["args"] as $key => $value) {
				if ( is_object($value) ) $value = "object";
				if ( is_array($value) ) $value = "array";

				$last_call["args"][$key] = (string) $value;
			}
			$full = $last_call["class"] . $last_call["type"] . $last_call["function"] . "(" . implode(",", $last_call["args"]) . ") on " . $last_call["file"] . " line " . $last_call["line"];
			$nice = $last_call["class"] . $last_call["type"] . $last_call["function"];
			
			return $full;
			
		}

		public static function slugify( $text )
		{
			//camelCase to camel-case
			//$text = self::camelToDash($text);
			
			// replace non letter or digits by -
			$text = preg_replace('~[^\\pL\d]+~u', '-', $text);
			
			// trim
			$text = trim($text, '-');
			
			// transliterate
			if (function_exists('iconv'))
			{
			    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
			}
			
			// lowercase
			$text = strtolower($text);
			
			// remove unwanted characters
			$text = preg_replace('~[^-\w]+~', '', $text);
			
			if (empty($text))
			{
			    return 'n-a';
			}
			
			return $text;
		}

		public static function uniqueId($prefix = "") {
			return uniqid($prefix);
		}

		// public static function googleFonts() {
		// 	return Cache::get("google-fonts", function(){
		// 		$url = "https://www.googleapis.com/webfonts/v1/webfonts?key=" . FW_GOOGLE_API_KEY;
		// 		$ch = curl_init();
		// 		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		// 		curl_setopt($ch, CURLOPT_HEADER, false);
		// 		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		// 		curl_setopt($ch, CURLOPT_URL, $url);
		// 		curl_setopt($ch, CURLOPT_REFERER, $url);
		// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		// 		$result = curl_exec($ch);
		// 		curl_close($ch);

		// 		return json_decode($result);
		// 	}, FW_CACHE_WEEK);
		// }

		public static function arrayToObject($arr) {
			return json_decode(json_encode($arr), FALSE);
		}

		public static function hash($thing) {
			return md5(serialize($thing));
		}

		public static function applyPropToObj( $prop, $value = null , &$obj ) {
		    $curObj = &$obj;
		    $properties = explode('.',$prop);
		    foreach ( $properties as $prop ) {
		    	if ( !isset($curObj->{$prop}) || !is_object($curObj->{$prop}) ) {
		    		$curObj->{$prop} = new \stdClass();
		    	}		    	
		    	$curObj = &$curObj->{$prop};
		    }

		    $curObj = $value;
		    return $obj;
		}

		public static function getFilesInDir($dir, $extension) {
			if ( !is_dir($dir) ) return false;
			$files = scandir($dir);
			$resultFiles = array();

			foreach( $files as $file ) {
				$pi = pathinfo($file);
				if ( isset($pi["extension"]) && in_array( $pi["extension"] , (array) $extension ) ) {
					$resultFiles[$pi["filename"]] = $dir . DS . $file;
				}
			}
			return count($resultFiles) ? $resultFiles : false;
		}

		public static function humanize($str) {

			if ( empty($str) ) return "";

			$result = strtolower(preg_replace('/(?<=\w)([A-Z])/', '_\\1', $str));
			        $delimiter = '\\';
			        if (false === strpos($delimiter, $result)) {
			            $delimiter = '_';
			        }
			        $result = str_replace($delimiter, ' ', $result);
			        $result = ucwords($result);

			$str = $result;

		 	$str = preg_replace('/-/', ' ', $str);
			$str = explode(' ', $str);
		 
			$str = array_map('ucwords', $str);
			$str = implode(' ', $str);
		 
			return $str;
		}

		//only once do action for given key
		private static $doOnceRegistry = array();
		public static function doOnce($key, $function) {
			if ( isset(self::$doOnceRegistry[$key]) ) return;
			if ( is_callable($function) ) {
				$function();
				$doOnceRegistry[$key] = true;
			}
		}

		public static function didOnce($key) {
			return isset(self::$doOnceRegistry[$key]);
		}

		public static function startsWith($haystack, $needle) {
			if ( !is_string($haystack) || !is_string($needle) ) return false;
		    return $needle === "" || strpos($haystack, $needle) === 0;
		}

		public static function endsWith($haystack, $needle) {
		    return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
		}

		public static function removePrefix($text, $prefix) {

			if ( !is_string($text) ) return $text;
			$text = trim($text);
		    if ( strpos($text, $prefix) == 0 ) $text = substr($text, strlen($prefix)) . "";
		    return $text;
		}

		public static function removeSuffix($text, $suffix) {
			if ( !is_string($text) ) return $text;

			$text = trim($text);

		    $suffixLen = strlen($suffix);
		    $textLen = strlen($text);

		    if ( strpos( $text, $suffix ) == $textLen - $suffixLen ) {
		    	$text = substr( $text , 0 , $textLen - $suffixLen );
		    }

		    return $text;
		}

		public static function textMore($text, $length = 100, $more = "...") {

			if ( strlen($text) < $length ) return $text;

			$text = substr($text, 0, $length);

			$text = trim($text) . $more;

			return $text;

		}

		public static function camelize($input) {
			$input = strtr(ucwords(strtr($input, array('_' => ' ', '.' => '_ ', '\\' => '_ '))), array(' ' => ''));
			$input = lcfirst( $input );
			$input = trim( $input );
			return $input;
		}

		public static function camelToDash($intup) {
			return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', $className));
		}

		public static function isArrayAssoc($array) {
			
			$keyAssoc = is_array($array) && (bool)count(array_filter(array_keys($array), 'is_string'));

			$oldArray = $array;
			\ksort($array);

			$assocOrder = $array === $oldArray;
			
		  return $keyAssoc || !$assocOrder;
		}

		public static function filterArrayByKeys($array, $keys) {
			return array_intersect_key($array, array_flip($keys));
		}

		public static function getDirTree($dir, $dirs = false) {
			$data = array();

			if ( !is_dir($dir) || !count(scandir($dir)) ) return $data;
			
			$dir = new \DirectoryIterator( $dir );
			foreach ( $dir as $node )
			{
				if ( $node->isDir() && !$node->isDot() )
				{
					$data[$node->getFilename()] = self::getDirTree( $node->getPathname() );
				}
				else if ( $node->isFile() )
				{
					$data[] = $node->getFilename();
				}
			}
			
			return $data;
		}

		public static function getFlattenDirTree($dir, $extensions = false , $includeEmptyDir = false ) {
			$tree = self::getDirTree($dir);
			$flatTree = self::flattenArray($tree, DIRECTORY_SEPARATOR, true);

			//var_dump($flatTree, "d");
			$fullDirTree = array();
			foreach( $flatTree as $relPath => $file ) {
				$pi = pathinfo($dir . DS . $relPath);
				if ( !$extensions || in_array($pi["extension"], (array)$extensions) )
					$fullDirTree[$relPath] = $dir . DS . $relPath;
			}
			return $fullDirTree;
		}

		public static function flattenArray( $array , $keySeparator = '.' , $firstLevel = false ) {
			
			if( is_array( $array ) ) {

				foreach( $array as $name => $value ) {	


					if ( !empty($value) ) {
						$f = self::flattenArray( $value , $keySeparator );
						if( is_array( $f ) ) {
							foreach( $f as $key => $val ) {
								//if ( !is_array($val) ) $key = !self::isArrayAssoc($f) ? $key . DS . $val : $val;
								//$val = self::isArrayAssoc($f);
								if ( is_int($key) ) $key = $val;
								$array[ $name . $keySeparator . $key ] = $val;
							}
							unset( $array[ $name ] );
							continue;
						}
					}

					if ( $firstLevel && !is_array($value) ) {

						$array[$value] = $value;
						unset( $array[ $name ] );
					}
				}
			}

			return $array;
		}
		//makes slashes in dir same 'direction'  path/to\my/dir\with\bad\slashes
		public static function unifySlashes($str) {			
			return preg_replace('/\\/|\\\/', DIRECTORY_SEPARATOR, $str);
		}

		public static function trimSlashes($str) {
			$str = trim($str, '/');
			$str = trim($str, '\\');
			return $str;
		}

		public static function removeExtension($path) {
			return preg_replace('/\\.[^.\\s]{2,10}$/', '', $path);
		}

		public static function toNumeric($string) {
			if ( !is_scalar($string) ) return $string;
			if ( !is_numeric($string) ) return $string;

			if ( (int) $string == (float) $string ) return (int) $string;
			return (float) $string;
		}

		public static function readFileConfig($file) {
			if ( !is_file((string)$file) ) return false;
			$pi = pathinfo($file);
			if ( $pi["extension"] !== "php" ) return false;
			$content = file_get_contents($file);

			//regex for cutting first block comment out of file
			$phpBlockComment = "/\\/\\*\\*.*\\n*(.|\\v)*?(?:\\*\\/)/m";

			preg_match($phpBlockComment, $content, $match);					


			if ( !isset($match[0]) ) return false;

			//saving comment
			$configContent = $match[0];

			//single param regex
			$singleParam = "/@(\w+)\W+(.+)/";

			//get all params
			preg_match_all($singleParam, $configContent, $params);

			
			$finalData = array();
			foreach ( $params[0] as $i => $fullLine ) {
				$paramName = $params[1][$i]; //param name @name
				$paramLine = $params[2][$i]; //rest of line


				$details = explode("|", $paramLine); //if line contain | - means details of param as array like @post $author : me | $date : now
				//if no details - line is value of param
				//
				
				if ( count($details) == 1 && strpos($details[0],":") === false ) {
					$finalData[$paramName][] = trim( self::maybeCallback( $details[0] ) );
				} else {
					$paramDetails = array();
					//detail need be as detailKey : Detail value
					foreach ( $details as $detail ) {
						$detailData = explode(":" , $detail);

						//get detail data
						$paramDetailKey = trim($detailData[0]);
						unset($detailData[0]);

						$paramDetailValue = trim( implode(":",$detailData) ); 
						$paramDetailValue = self::toNumeric($paramDetailValue);

						$paramDetailValue = self::maybeCallback( $paramDetailValue );

						$paramDetails[ $paramDetailKey ] = $paramDetailValue; //add to list of details 
					}
					//add to final data
					$finalData[$paramName][] = $paramDetails;
				}
			}


			//convert plain data to not be 1 item long arrays 
			foreach ( $finalData as $key => $val ) {
				if ( count($val) == 1 && !is_array($val[0])  ) {
					$val = $val[0];
					$finalData[$key] = $val;
				}

				$finalData[$key] = self::maybeBool($finalData[$key]);
			}

			return $finalData;
		}

		//Common::routeMatch( "some/amazing/thing/3" , "some/:type/thing/:id" ); => array( "type" => "amazing" , "id" => 3 )
		public static function routeMatch( $path , $pattern ) {
			\preg_match_all("/:([\w-%]+)/", $pattern, $argument_keys);
			$pattern = \preg_replace( "/\//" , "\/" , $pattern );

			$pathRegEx = "/" . \preg_replace( "/:([\w-%]+)/" , "(.+)" , $pattern ) . "$/";

			$isPathMatching = \preg_match_all($pathRegEx, $path, $path_values);

			if ( $isPathMatching ) {
				$data = array();
				foreach ( $argument_keys[1] as $i => $key ) {
					$value = $path_values[$i + 1][0];

					$data[$key] = $value;
				}
				return count($data) ? $data : true;
			}

			return false;
		}
		
	}


?>