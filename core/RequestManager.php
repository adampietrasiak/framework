<?php 
	//this class will recognise request and call propoer files to build output

	namespace Fw;

	class RequestManager {

		public static function isLoginPage() {
			return false;
			$isLoginPage = Common::startsWith($page_viewed, "wp-login.php") 
				&& $_SERVER['REQUEST_METHOD'] == 'GET' 
				&& $_GET["action"] !== "register" 
				&& $_GET["checkemail"] !== "registered";

			return $isLoginPage;
		}

		public static function isRegisterPage() {
			return false;
			$isRegisterPage = Common::startsWith($page_viewed, "wp-login.php") 
				&& $_SERVER['REQUEST_METHOD'] == 'GET' 
				&& ( $_GET["action"] == "register" || $_GET["checkemail"] == "registered" );

			return $isRegisterPage;
		}

		public static function adjustMainQuery($file){

			$config = Common::readFileConfig($file);
			$foundType = false;
			$args = false;

			//read from file config
			if ( isset($config["query"]) ) {
				if ( is_string($config["query"]) && post_type_exists( $config["query"] ) ) {
					$args = array("post_type" => $config["query"]);
				} elseif ( is_array( $config["query"] ) ) {
					$args = (array) $config["query"][0];
				}
			}

			//make sure its good post type
			if ( $args ) {
				//get params from get (excluding page id) so it will support native features like pagination url.com?paged=5
				$get = $_GET;
				unset($get["page_id"]);
				//add params to query
				$args = array_merge($get, $args);
				//make query
				query_posts( $args );

				//let know we've changed query 
				return true;
			}

			return false;			
		}

		public static function filterInput() {
			$_POST = array_map( 'stripslashes_deep', $_POST );
			$_GET = array_map( 'stripslashes_deep', $_GET );
			$_COOKIE = array_map( 'stripslashes_deep', $_COOKIE );
			$_REQUEST = array_map( 'stripslashes_deep', $_REQUEST );
		}



	}

 ?>