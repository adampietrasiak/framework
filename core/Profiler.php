<?php 

	namespace Fw;

	class Profiler {

		
		static $log = array();
		static $started = array();

		private static $level = 0;

		static function mensure($key = false, $func = false) {
			if ( is_callable($func) ) {
				self::start($key);
				$func();
				return self::end($key);
			}
		}

		static function start($key = false) {
			self::$level++;
			self::$started[$key] = microtime(true);
		}

		static function end($key = false) {

			if ( isset(self::$started[$key]) ) {
				$end = microtime(true);
				$total = $end - self::$started[$key];
				unset(self::$started[$key]);
				self::$log[] = array(
					"key" => $key,
					"time" => $total,
					"depth" => count(self::$started),
					"stamp" => microtime(true),
					"level" => self::$level
				);

				self::$level--;

				return $total;
			}

		}

		static function results() {
			var_dump(self::$log);
		}


	}
?>