<?php 

	namespace Fw;

	class ThemeSupport {

		public static function init() {

			\add_theme_support("post-thumbnails");
			\add_theme_support("automatic-feed-links");
			\add_theme_support("menus");
			\add_theme_support("widgets");

			if ( FW_HIDE_WP_ADMIN_BAR ) {
				add_filter( 'show_admin_bar', '__return_false' );
			}

		}

	}


?>