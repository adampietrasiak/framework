<?php 
	namespace Fw;

	class Fw {
		//init framework


		public static function init() {

			//load fw files
			self::loadCore();

			//when WordPress is prepared, run framework
			\add_action( \is_admin() ? "init" : "wp", function(){
				Fw::run();
			});
			
		}

		public static function loadCore() {

			require_once("Profiler.php");
			require_once("Component.php");	
			require_once("WpUtil.php");	
			require_once("PackageManager.php");

			PackageManager::loadLibs();

			Common::requireDir(FW_CORE_DIR);

			//filter _POST _GET etc
			RequestManager::filterInput();

			//this needs to be done before init action
			ThemeSupport::init();

			\do_action("fw_core_loaded");

		}

		public static function run(){
			//init all components

				
				Options::getDefaults();
				Options::getAllData();
				Config::initAll();
				Includes::initAll();
				Partial::initAll();
				AdminPages::initAll();
				Notices::initAll();
				Shortcodes::initAll();
				Menus::initAll();
				Ajax::initAll();
				Fonts::initAll();
				Section::initAll();
				Widgets::initAll();
				WidgetAreas::initAll();
				Variables::initAll();
				Templates::initAll();
				AssetsJs::initAll();
				AssetsCss::initAll();

				self::misc();

				self::render();
		}

		public static function render() {

			add_filter( "template_include" , function($template){
				
				//in case, we need to adjust main query to get posts to display info
				RequestManager::adjustMainQuery($template);

				$template = Templates::checkCustomTemplate($template, $GLOBALS["post"]);

				$config = Common::readFileConfig($template);

				//check if custom header or footer
				$headerFiles = isset($config["header"]) ? array( "header-" . $config["header"] , "header" ) : "header";
				$footerFiles = isset($config["footer"]) ? array( "footer-" . $config["footer"] , "footer" ) : "footer";


				//capture actions before template is included
				Actions::capture();

				//localize files for footer and header. If no found, use automatic one partials
				$header =  WpUtil::templateHasFile( $headerFiles , array("php") ) ?: "fw/core/fw-auto-header";
				$footer =  WpUtil::templateHasFile( $footerFiles , array("php") ) ?: "fw/core/fw-auto-footer";
				
				$login = WpUtil::templateHasFile("login", array("php"));
				$register = WpUtil::templateHasFile("register", array("php"));

				WpUtil::manageFileNeeds($template);

				//now if needed add header and footer with content in middle
				if ( !FW_IS_AJAX_PAGE && FW_AUTO_INCLUDE_HEADER_FOOTER && !did_action("wp_head") ) Partial::render( $header  );
				remove_all_actions("wp_head");remove_all_actions("get_header");

				\do_action("fw_output_start");

				//////////////CONTENT////////////////////////

					
					if ( FW_ENABLE_CUSTOM_LOGIN_REGISTER && RequestManager::isLoginPage() && $login ) {
						Partial::render( $login );
					} else if ( FW_ENABLE_CUSTOM_LOGIN_REGISTER && RequestManager::isRegisterPage() && $register ) {
						Partial::render( $register );
					} else {
						\do_action( "fw_body_start" );
						Partial::render( $template );
						\do_action( "fw_body_end" );

					}

				//////////////END CONTENT//////////////////////


				if ( !FW_IS_AJAX_PAGE && FW_AUTO_INCLUDE_HEADER_FOOTER && !did_action("wp_footer") ) $footerHtml = Partial::render( $footer  );
				remove_all_actions("wp_footer");remove_all_actions("get_footer");

				\do_action("fw_output_end");
				

				die;
				
			});
		}



		public static function misc() {

			if ( FW_SMART_HEAD ) add_action("wp_head", function(){
				Partial::render("fw/core/fw-smart-head");
			}, 1);

			WpUtil::detectFavIcon();
			WpUtil::detectLoginLogo();


		}


	}
	
?>