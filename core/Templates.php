<?php
	
	namespace Fw;

	class Templates extends Component {

		static $dir = "templates";

		static $templates = array();

		public static function init($file, $config) {
			$type = basename(dirname($file));

			if ( !\post_type_exists($type) ) return;

			$name = !empty($config["name"]) ? $config["name"] : Common::fileHumanize($file);


			static::$templates[$type][$file] = $name;
		}

		public static function afterInitAll() {
			$allTemplates = static::$templates;

			foreach ( $allTemplates as $type => $templates ) {

				if ( !\post_type_exists($type) ) continue;

				if ( $type !== "page" ) {
					self::addPostTypeTemplates($type, $templates);
				} else {
					self::addPageTemplates($templates);
				}

			}
		}


		public static function checkCustomTemplate($file, $post) {
			if ( empty($post) || empty($post->ID) ) return $file;
			$id = $post->ID;

			$pageTemplate = \get_post_meta( $id, "_wp_page_template", true );
			$postTypeTemplate = \get_post_meta( $id, "_post_type_template", true );

			if ( $post->post_type == "page" && $pageTemplate ) {
				if ( is_file( $pageTemplate ) ) return $pageTemplate;
				if ( is_file( FW_TEMPLATE_DIR . DS . $pageTemplate ) ) return FW_TEMPLATE_DIR . DS . $pageTemplate;
			} elseif( $postTypeTemplate ) {
				if ( is_file( $postTypeTemplate ) ) return $postTypeTemplate;
				if ( is_file( FW_TEMPLATE_DIR . DS . $postTypeTemplate ) ) return FW_TEMPLATE_DIR . DS . $postTypeTemplate;
			}

			return $file;
		}

		//http://www.wpexplorer.com/wordpress-page-templates-plugin/
		//adds page template tricking wordpress cache
		public static function addPageTemplates($newTemplates) {

			$adjustTemplates = function($actual) use ($newTemplates) {
		        // Create the key used for the themes cache
		        $cache_key = 'page_templates-' . md5( get_theme_root() . '/' . get_stylesheet() );

		        // Retrieve the cache list. 
				// If it doesn't exist, or it's empty prepare an array
		        $templates = wp_get_theme()->get_page_templates();
		        if ( empty( $templates ) ) $templates = array();

		        // New cache, therefore remove the old one
		        wp_cache_delete( $cache_key , 'themes');

		        // Now add our template to the list of templates by merging our templates
		        // with the existing templates array from the cache.
		        $templates = array_merge( $templates, $newTemplates );
		        

		        // Add the modified cache to allow WordPress to pick it up for listing
		        // available templates
		        wp_cache_add( $cache_key, $templates, 'themes', 1800 );

		        return $actual;
			};

			//we need add slashes fix in case of long path of custom template like path\to\template.php
			Common::doOnce("fw-page-templates-meta-fix" , function(){
				add_action( 'save_post', function($id) {
					if ( isset($_POST["page_template"]) ) {
						$fixedTemplate = wp_slash( $_POST["page_template"] );
						update_post_meta( $_POST["ID"], "_wp_page_template", $fixedTemplate );
						unset($_POST["page_template"]);
					}
				});
			});

			
			add_filter("page_attributes_dropdown_pages_args", $adjustTemplates);			
			add_filter("wp_insert_post_data", $adjustTemplates);		
		}

		public static function addPostTypeTemplates($postType, $templates) {

			if ( empty( $templates ) || !\post_type_exists($postType) ) return;

			//make sure to add it only once (in case different packages adds templates to same post type)
			Common::doOnce("fw-post-type-templates-" . $postType , function() use($postType, $templates) {
				//add box

				add_action( 'add_meta_boxes', function() use($postType, $templates) {
					$nicePostType = Common::humanize($postType);
					add_meta_box( "fw-post-type-templates", sprintf( "%s attributes" , $nicePostType ) , function() use($postType, $nicePostType, $templates) {
						Partial::render("fw/templates/post-type-templates", array(
							"templates" => $templates,
							"niceType" => $nicePostType,
							"type" => $postType
						));
					}, $postType, "side" , 'core' );
				});	
				//add save action to save template as meta
				add_action("save_post", function($id){
					if ( isset($_POST['_post_type_template']) ) {
						update_post_meta( $id, '_post_type_template', wp_slash($_POST['_post_type_template']) );
					}
				});
			});
		}

		


	}