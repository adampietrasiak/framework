<?php 

	namespace Fw;

	class Widgets extends Component{

		static $cacheUrls = array();

		static $dir = "widgets";

		//static $files = array();
		//static $data = array();

		public static function init($file, $config) {

			add_action( "widgets_init", function() use($file, $config){
				
				$config["name"] = isset($config["name"]) ? $config["name"] : ( isset($config["title"]) ? $config["title"] : $name );
				$config["description"] = isset($config["description"]) ? $config["description"] : "";
				
				//standard register widget function is ommited here as we need to have access to construction of new object as we use same class for all widgets with different build parameters
				//it's quite easy as register_widget is 1line function - just copy-pasted with modification of new object parameters
				global $wp_widget_factory;
				$wp_widget_factory->widgets[ "widget-" . Common::slugify($config["name"]) ] = new Fw_Widget_Factory($config , $file);
			});
		}
	}

	class Fw_Widget_Factory extends \WP_Widget {

		private $config = null;
		private $sourceFile = null;

		function __construct($config, $sourceFile) {

			$this->config = $config;
			$this->sourceFile = $sourceFile;
			// Instantiate the parent object
			parent::__construct( "widget-" . Common::slugify( $config["name"] ) , $config["name"] );
		}

		function widget( $args, $instance ) {
	    	\partial("fw/widgets/display", array(
				"file" => $this->sourceFile,
				"data" => $instance,
				"name" => $this->config["name"]
	    	));
			// Widget output
		}

		function form( $instance ) {
	    	$data = $this->config;
	    	$data["object"] = $this;
	    	$data["instance"] = $instance;

			\partial("fw/widgets/options", $data );
			// Output admin widget options form
		}
	}

	

?>