<?php 

	namespace Fw;

	class WidgetAreas extends Component {

		static $dir = "widget-areas";

		static $areas = array();

		public static function init($file, $config) {

			$slug = Common::fileSlug($file);

			self::$areas[$slug] = $file;

			$niceName = isset($config["name"]) ? $config["name"] : Common::humanize($slug);

			$args = array(
				'name'          => $niceName,
				'id'            => $slug,
				'description'	=> isset($config["description"]) ? $config["description"] : $niceName . " widget area",
				'before_widget' => '<div id="widget-%1$s" class="widget %2$s">',
				'after_widget'  => '</div>',
			);

			register_sidebar($args);
		}

	}

 ?>