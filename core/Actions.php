<?php 

	namespace Fw;

	class Actions {

		public static function capture(){
			if ( !isset($_GET["fwaction"]) ) return;
						
			$actions = $_GET["fwaction"];
			$knownActions = self::getActions();


			foreach( explode(",", $actions) as $action ) {
				if ( $action && isset( $knownActions[$action] ) ) {
					Partial::render( $knownActions[$action] );
				}
			}

		}

		public static function getActions(){
			return PackageLoader::collectPackagesFilesOfType("actions", "php");
		}

	}

?>