<?php 

	namespace Fw;

	//this component automatically load js files
	class AssetsJs extends Component {
		
		static $dir = "js";

		static $extensions = "js";


		static function afterInitAll() {
			\add_actions( array("wp_print_footer_scripts" , "admin_enqueue_scripts" , "login_head") , function() {
				Partial::render("fw/core/fw-js-variables", array("tag" => true));
			});
		}

		static function init($file, $config, $relative) {

			$relativeDir = dirname($relative);
			$base = Common::depsFileBase($file);
			$uri = Common::wpDirToUri($file);

			if ( $relativeDir == "assets" ) {

				WpUtil::autoRegister($uri, $base);

			} elseif ( Localizator::check( $relativeDir ) ) {
				WpUtil::autoEnqueue($uri, "global", false, array("jquery"));
			}

		}

		static function beforeInitAll() {

			$function = function(){
				WpUtil::needs(FW_NEEDS);
			};

			add_action( "wp_head", $function );
			add_action( "admin_enqueue_scripts", $function );
			add_action( "login_head", $function );

		}
	}

 ?>