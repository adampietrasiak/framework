<?php 

	namespace Fw;


	//config manager
	class Config extends Component {

		static $config = array();

		static $dir = "config";
		static $extensions = array("xml");

		public static function afterInitAll() {

			$config = self::$config;



			if ( !empty($config["options"]) ) self::manageOptions($config["options"]);
			if ( !empty($config["post-types"]) ) self::managePostTypes($config["post-types"]);
			if ( !empty($config["google-font"]) ) self::manageGoogleFonts($config["google-font"]);

			if ( !empty($config["needs"]) ) foreach ( (array) $config["needs"] as $need ) {
				if ( !empty( $need["attr"] ) ) {
					foreach ( $need["attr"] as $urlOrSlug ) {
						\Fw\WpUtil::needs($urlOrSlug);
					}
				}
			}
			
		}

		public static function getAll() {
			return self::$config;
		}

		public static function init($file, $config, $relative) {

			$xml = Xml::parse($file);

			self::$config[$relative] = $xml;
		}

		private static function manageOptions($options) {


			self::manageThemeOptions($options);

			if ( !empty($options["section"]) ) foreach ($options["section"] as $section) {
				OptionsPage::addSection($section);
			}

			$niceName = !empty($options["options"][0]["name"]) ? $options["options"][0]["name"] : __("Theme options", "fw");

			OptionsPage::execute($niceName);



		}

		private static function manageThemeOptions($options) {


			if ( !empty( $options["section"] ) ) foreach ( $options["section"] as $section ) {
				ThemeOptions::addSection($section);
			}

			if ( !empty( $options["option"] ) ) foreach ( $options["option"] as $option ) {
				ThemeOptions::add($option, false);
			}

		}

		private static function manageGoogleFonts($data) {

			if ( count($data) == 1 ) $data = array($data);

			foreach( $data as $i => $font ) {
				$font = $font["attr"];
				if ( isset($font["family"]) ) {
					$family = urlencode( trim($font["family"]) );
					
					add_action('wp_print_styles', function() use($family){
						wp_enqueue_style( 'font-' . $family , 'http://fonts.googleapis.com/css?family=' . $family . ':100,100italic,200,200italic,300,300italic,400,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic&subset=latin-ext');
					});

				}
			}
		}

		private static function managePostTypes($data) {


			//for each post type in config
			foreach ($data as $type => $typeOptions) {
				//var_dump($typeOptions);
				if ( Common::startsWith( $type , "@" ) ) continue;

				$typeOptions = reset($typeOptions);
				
				$niceName = Common::humanize($type);


				PostTypes::registerPostType($niceName, $type, isset($typeOptions) ? $typeOptions : array() );

				//add meta options
				PostTypes::registerMetabox($niceName, $type, $typeOptions);

				
				if ( isset( $typeOptions["taxonomy"] ) ) {
					foreach( (array) $typeOptions["taxonomy"] as $tax ) {
						PostTypes::registerTaxonomy($tax, $type);
					}
				}

				//if needed - add multiple featured images ability
				if ( isset($typeOptions["thumbnails"]) && (int) $typeOptions["thumbnails"] > 0 ) {
					MultipleFeatured::addNFeatured($type, (int) $typeOptions["thumbnails"]);
				}

				if ( !empty($typeOptions["thumbnail"]) ) foreach( $typeOptions["thumbnail"] as $thumb ) {
					if ( !empty($thumb["name"]) ) MultipleFeatured::addFeatured($type, \Inflector::humanize( $thumb["name"] ) );
				}
			}

		}

	}

	

?>