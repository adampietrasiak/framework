<?php 

	namespace Fw;

	class OptionsPage {

		private static $data = array(
			"sections" => array(),
			"name" => "",
			"description" => ""
		);


		public static function addSections($sections) {

			foreach ( $sections as $section ) {
				self::addSection($section);
			}

		}

		public static function addSection($section) {
			if ( empty($section["name"]) ) return;
			$slug = !empty( $section["slug"] ) ? $section["slug"] : Common::slugify( $section["name"] );
			self::$data["sections"][$slug] = $section;
		}

		public static function addOption($option , $section) {

			if ( isset( self::$data["sections"][$section] ) ) {
				self::$data["sections"][$section]["option"][] = $option;
			}

		}

		private static function initOptionFilters() {

			add_filter( "partial/fw/option/option" , function($vars){
				return Common::prepareOptionData($vars);
			});

		}

		public static function execute($name) {

			self::initOptionFilters();

			$options = self::$data;

			$options["name"] = $name;

			$slug = "fw-theme-options";

			$renderOptionsPage = function() use ($options) {
				Partial::render( "fw/options/page", $options );
			};

			add_action('admin_menu', function() use($renderOptionsPage, $slug, $name) {
				\add_menu_page( $name , $name , "manage_options" , $slug , $renderOptionsPage , "dashicons-edit" );
			});

			$parent = WpUtil::addAdminBarItem( __("Theme", "fw") , "#", "site-name", false , 1);
			$parent = WpUtil::addAdminBarItem(__("Options"), "#", $parent , "portfolio" );
			
			WpUtil::addAdminBarItem($name, get_admin_url(null, "admin.php?page=$slug" ), $parent , "edit");


		}


	}










 ?>