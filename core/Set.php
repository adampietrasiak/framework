<?php 

	namespace Fw;


	class Set implements \ArrayAccess {

		private $data = array();

		function __construct($set = false) {

			if ( $set ) $this->data = (array) $set;

			return $this;

		}

		public static function instance($set = false) {
			return new self($set);
		}

		public static function inst($set = false) {
			return self::instance($set);
		}


		public static function convert(&$arr) {
			$arr = new self($arr);
			return $arr;
		}

		public function raw() {
			return $this->data;
		}

		public function offsetSet($offset, $value) {
		    if (is_null($offset)) {
		        $this->data[] = $value;
		    } else {
		        $this->data[$offset] = $value;
		    }
		}

		public function offsetExists($offset) {
		    return isset($this->data[$offset]);
		}

		public function offsetUnset($offset) {
		    unset($this->data[$offset]);
		}

		public function offsetGet($offset) {
		    return isset($this->data[$offset]) ? $this->data[$offset] : null;
		}

		public function &__get ($key) {
		    return $this->data[$key];
		}

		public function __set($key,$value) {
		    $this->data[$key] = $value;
		}

		public function __isset ($key) {
		    return isset($this->data[$key]);
		}

		public function __unset($key) {
		    unset($this->data[$key]);
		}

		public function push($val , $key = false) {
			if ( $key ) {
				$this->data[$key] = $val;
			} else {
				$this->data[] = $val;
			}
			return $this;
		}

		public function pop() {
			return \array_pop($this->data);
		}

		public function copy() {
			return self::instance( $this->data );
		}

		public function reverse() {
			$this->data = \array_reverse($this->data);
			return $this;
		}

		public function clear() {
			$this->data = array();
			return $this;
		}

		public function sort($func = false) {
			if ( !$func ) $func = function($a , $b) { return (float) $b - (float) $a; };
			\usort($this->data , $func);
			return $this;
		}

		public function filter($func) {
			$this->data = \array_filter($this->data , $func);
			return $this;
		}

		public function length() {
			return count( $this->data );
		}

		public function isEmpty() {
			return $this->length() == 0;
		}

		public function first($n = 1) {

			if ( $n > 1 ) {
				return new Set( \array_slice( $this->data , 0 , $n ) );
			} else {
				$values = \array_values($this->data);
				return \array_shift($values);
			}

		}

		public function last($n = 1) {

			if ( $n > 1 ) {
				return new Set( \array_slice( $this->data , -$n ) );
			} else {
				return \array_pop(\array_values($this->data));
			}

		}

		public function eq($n = null) {
			$n--;
			if ( isset($this->data[$n]) ) return $this->data[$n];
		}

		public function choose($what) {

			foreach ( $this->data as $key => $val ) {
				if ( is_callable($what) && $what($val , $key) ) return $val;
				if ( $what == $val ) return $val;
			}

			return false;

		}

		public function max($func = false, $count = 1) {
			return $this->copy()->sort($func)->first($count);
		}

		public function min() {
			return $this->copy()->sort($func)->last($count);
		}

		public function slice($start , $end) {
			$this->data = array_slice( $this->data , $start , $end );
			return $this;
		}

		public function sume($func = false) {
			if ( !$func ) $func = function($val) { return (float) $val; };

			$sume = 0;

			foreach ( $this->data as $key => $val ) {
				$sume += $func($val , $key);
			}

			return $sume;
		}

		public function shuffle($preserveKeys = false) {

			if ( !$preserveKeys ) {
				\shuffle( $this->data );
			} else {
				$keys = array_keys($this->data); 
				\shuffle($keys); 

				$random = array(); 
				foreach ($keys as $key) { 
				  $random[$key] = $this->data[$key]; 
				}

				$this->data = $random;
			}

			return $this;
		}

		public function flip() {
			$this->data = array_flip($this->data);
			return $this;
		}

		public function map($func) {
			foreach ( $this->data as $key => $val ) {
				$this->data[$key] = $func($val , $key);
			}
			return $this;
		}

		public function merge($arr) {
			$this->data = \array_merge_recursive($this->data , $arr);
			return $this;
		}

		public function has($what) {
			return !$this->choose($what , 1)->isEmpty();
		}

		public function hasKey($key) {
			return \array_key_exists($key , $this->data);
		}

		public function unique() {
			$this->data = \array_unique( $this->data );
			return $this;
		}

		public function each($func) {
			foreach ( $this->data as $key => $val ) {
				$func( $val , $key );
			}
			return $this;
		}

	}


 ?>