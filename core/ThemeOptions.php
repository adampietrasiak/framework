<?php 

	namespace Fw;

	class ThemeOptions {

		public static function add($option, $section = false, $suboption = false) {
			$option = \Fw\Common::prepareOptionData($option);


			\add_action("customize_register", function($wp_customize) use( $option , $section ){
				

				extract($option);

				$wp_customize->add_setting( $id , array(
				    'default' => \fw_option($name),
				    'value' => \fw_option($name)
				));

				$control = new ThemeOptionsControl( $wp_customize, $id , array(
					'label'    => $title,
					'section'  => $section,
					'settings' => $id
				));
				$control->setOptionData( $option );

				$wp_customize->add_control( $control );

				return;

				if ( $type == "html" ) $type = "textarea";

				$controlData = array(
					'label'    => $title,
					'section'  => $section,
					'settings' => $id
				);

				if ( $suboption ) $controlData["class"] = "suboption";

				if ( $type == "image" ) {
					$control = new \WP_Customize_Image_Control( $wp_customize, $id, $controlData );
				} elseif( $type == "color" ) {
					$control = new \WP_Customize_Color_Control( $wp_customize, $id, $controlData );
				} else {
					if ( !empty($options) ) $controlData["choices"] = Common::prepareSelectOptions( $options );
					if ( !empty($type) ) $controlData["type"] = $type;
					$control = new \WP_Customize_Control( $wp_customize, $id, $controlData );
				}

				$wp_customize->add_control( $control );
			});

		}

		public static function addSection($section) {
			

			if ( !empty( $section["option"] ) ) {

				$slug = Common::slugify( $section["name"] );
				$title = Common::humanize( $section["name"] );

				\add_action("customize_register", function($wp_customize) use( $slug , $title ){
					$wp_customize->add_section( $slug , array(
					    'title'      => $title,
					    'priority'   => 30,
					));
				}, 1);

				foreach ( $section["option"] as $option ) {
					self::add( $option, $slug );
				}

				
			}

		}

	}

	if ( class_exists('WP_Customize_Control') ) {


		class ThemeOptionsControl extends \WP_Customize_Control {

			public function setOptionData($data) {
				$this->optionData = $data;
				$this->value = !empty($data["value"]) ? $data["value"] : "";
			}

			public function render_content() {
				$data = $this->optionData;
				$data["attr"] = !empty( $data["attr"] ) ? $data["attr"] : array();
				$data["attr"]["data-customize-setting-link"] = esc_attr( $this->settings[ "default" ]->id );

				$data["loadValue"] = true;

				$data["filter"] = function($option) {
					$option["loadValue"] = true;
					if ( empty($option["title"]) && !empty($option["name"]) ) {
						$option["title"] = $option["name"];
						unset($option["name"]);
					}

					if ( !empty($option["slug"]) ) $option["name"] = $option["slug"];

					return $option;
				};

				\partial("fw/option/option", $data);
			}

		}
	}





?>