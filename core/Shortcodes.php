<?php

	namespace Fw;

	class Shortcodes extends Component {

		static $dir = "shortcodes";

		static $shortcodes = array();

		public static function init($file, $config) {

			$pi = pathinfo($file);


			$config["tag"] = $pi["filename"];

			$scVars = array();
			if ( !empty($config["param"]) ) foreach	( (array) $config["param"] as $i => $paramInfo ) {				
				$scVars[ $paramInfo["name"] ] = !empty($paramInfo["default"]) ? $paramInfo["default"] : "";
			};

			$cb = function($atts , $content) use ($scVars, $file) {					
				$passedAttributes = shortcode_atts( $scVars , $atts );
				$passedAttributes["content"] = $content;

				return Partial::get($file, $passedAttributes);
			};  

			$scName = $pi['filename'];
			
			add_shortcode( $scName , $cb );

			if ( !isset( self::$shortcodes[$scName] ) ) self::$shortcodes[$scName] = $config;

		}

		public static function afterInitAll() {

			if ( !is_admin() ) return;

			$shortcodes = self::$shortcodes;

			add_action( "edit_form_after_editor" , function() use($shortcodes) {

				\needs( "fw-options" );
				\needs( "spectrum" );

			} );

			WpUtil::add_tinymce_button("fwshortcodes", Common::wpDirToUri(FW_JS_DIR . DS . "assets" . DS . "fw-tinymce-shortcodes.js") );

		}

	}




?>