<?php 

	//just to make it easier to use functions out of classes or fw namespace
	//
	


	function partial($name, $data = array()) {
		return \Fw\Partial::render($name, $data);
	}

	function partials($name, $items) {
		foreach ((array)$items as $item) {
			partial($name, $item);
		}
	}

	function is_location($loc) {
		return \Fw\Localizator::check($loc);
	}

	function has_partial($name) {
		return \Fw\Partial::has($name);
	}

	function get_partial($name, $data = array()) {
		return \Fw\Partial::get($name, $data);
	}

	function renderQuery($partial, $args) {
		\Fw\WpUtil::renderQuery($partial, $args);
	}

	function go_home() {
		\Fw\WpUtil::goHome();exit;
	}

	function menu($slug) {
		\Fw\Menus::render($slug);
	}

	function fw_head(){
		wp_head();
	}

	function fw_header() {
		\Fw\Partial::render("fw/core/fw-auto-header");
	}

	function fw_icon($name, $class = "") {
		partial("fw/icon", array("icon" => $name, "class" => $class));
	}

	function fw_config($key, $default = null) {
		$option = \Fw\FwConfig::get($key);
		return !empty($option) ? $option : $default;
	}

	function profile($key = false, $func = false) {
		\Fw\Profiler::auto($key, $func, true);
	}

	function fw_post($post) {
		return \Fw\Post::find($post);
	}

	function needs($slug) {
		\Fw\WpUtil::needs($slug);
	}

	function fw_option($name) {
		return \Fw\Options::get($name);
	}

	function fw_option_bool($name) {
		return \Fw\Options::getBool($name);
	}	

	function add_actions($actions, $callback, $priority = null) {
		foreach ( (array) $actions as $action) {
			add_action($action, $callback, $priority);
		}
	}

	function fw_dashicon($name, $class = "") {
		partial("wp/dashicon", array("icon" => $name , "class" => $class));
	}

	function fw_loop($name, $args = false) {
		\Fw\WpUtil::loop($name, $args);
	}

	function widget_area($name, $fallback = null) {
		partial("fw/widgets/widget-area", array("name" => $name, "fallback" => $fallback));
	}

	function fw_cache($key , $val = null , $time = 600) {
		\Fw\Cache::auto($key , $val , $time);
	}

	function fw_admin_notice($message, $title = false, $type = "updated") {
		\Fw\WpUtil::notice( array(
			"message" => $message,
			"title" => $title,
			"type" => $type
		));
	}

?>