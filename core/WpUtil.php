<?php 

	namespace Fw;

	class WpUtil {

		public static function add_tinymce_button($name, $jsFile) {
			// Hooks your functions into the correct filters

			
			add_action('admin_head', function() use( $name, $jsFile ) {
				// check user permissions
				if ( !current_user_can( 'edit_posts' ) && !current_user_can( 'edit_pages' ) ) {
					return;
				}
				// check if WYSIWYG is enabled
				if ( 'true' == get_user_option( 'rich_editing' ) ) {
					
					add_filter( 'mce_external_plugins', function( $plugin_array ) use( $name, $jsFile ) {											
						$plugin_array[Common::slugify($name)] = $jsFile;
						return $plugin_array;
					} );

					add_filter( 'mce_buttons', function( $buttons ) use( $name ) {						
						array_push( $buttons, Common::slugify($name) );
						return $buttons;
					} );

				}
			});
		}

		public static function requireFileSystem() {
			require_once(ABSPATH . 'wp-admin/includes/file.php');
			require_once(ABSPATH . 'wp-admin/includes/image.php');
		}

		public static function useFileSystem() {
			global $wp_filesystem;
			if( empty( $wp_filesystem ) ) {
				self::requireFileSystem();
				\WP_Filesystem();
			}
		}

		public static function getMainThemeFolder() {
			$currentTheme = \wp_get_theme(); //current theme object
			$currentThemeFolder = $currentTheme->template; //current theme folder

			return $currentThemeFolder;
		}

		public static function getChildThemeFolder() {
			$childThemeFolder = basename(dirname( \get_bloginfo('stylesheet_url') )); //child theme folder (if exist)
		
			$mainThemeFolder = self::getMainThemeFolder();

			if ( $childThemeFolder == $mainThemeFolder ) return false;
			return $childThemeFolder;

		}

		public static function makeThemeDir($relative) {
			\wp_mkdir_p(FW_TEMPLATE_DIR . DS . $relative);
		}

		public static function saveFile($path, $content, $overwrite = false) {
			$fullPath = FW_TEMPLATE_DIR . DS . $path;

			if ( $overwrite || !is_file( $path ) ) {
				
				if ( empty($GLOBALS["wp_filesystem"]) ) {

					require_once(ABSPATH . 'wp-admin/includes/file.php');
					\WP_Filesystem();
				}

				global $wp_filesystem;


				$result = $wp_filesystem->put_contents(
					$path,
					$content,
					FS_CHMOD_FILE // predefined mode settings for WP files
				);

			}
		}

		public static function putContents($path, $content) {
			self::saveFile($path, $content, true);
			return $path;
		}

		public static function relatedPosts($id, $count = 10) {
			$tags = array_map( function($tag) {
				return $tag->term_id;
			}, \wp_get_post_tags($id) );
			$categiries = \wp_get_post_categories($id);


			$args=array(
				'tag__in' => (array) $tags,
				'cat' => implode(",", $categiries),
				'post__not_in' => array($id),
				'posts_per_page'=> $count, // Number of related posts that will be shown.
			);

			return \get_posts($args);
		}

		public static function postExists($id) {
			return \get_post_status( (int) $id) !== FALSE;
		}

		public static function getPostTerms($id) {
			return \wp_get_post_terms( $id , \get_taxonomies() );
		}


		//this function creates attachment basing or url or if have existing attachment id it will update this attachment file
		public static function importAttachment($url, $attachmentId = 0 ) {
			
			self::requireFileSystem();

			$fileData = array(
				'name' => basename($url), // ex: wp-header-logo.png
				'tmp_name' => download_url( $url, \ini_get('max_execution_time') ),
				'error' => 0
			);

			//handle uploading
			$uploaded = \wp_handle_sideload( $fileData , array(
				"test_form" => false
			));

			//if uploading was successful
			if ( !isset($uploaded["error"]) ) {

				//check if we need to create new attachment
				if ( !self::postExists( $attachmentId ) ) {

					//if so, prepare data
					$postData = array(
						"post_title" => $uploaded["file"],
						"post_content" => "",
						"post_status" => "publish",
						"post_mime_type" => $uploaded["type"],
						"post_type" => "attachment"
					);

					//if post dont exist but we've got attachment id, try to make new attachment have this id
					if ( !empty($attachmentId) ) {
						$postData["import_id"] = (int) $attachmentId;
					}

					//insert post and get real id
					$attachmentId = wp_insert_post( $postData );
				}

				//and now compute file, create different sizes etc and upload attachment data
				$attach_data = \wp_generate_attachment_metadata( $attachmentId, $uploaded["file"] );
				\wp_update_attachment_metadata( $attachmentId, $attach_data );
				\update_attached_file( $attachmentId , $uploaded["file"] );

				return $attachmentId;
			}

			return false;

		}

		//return equvalent child theme file to given file or false if no file in child theme. If checking child theme file it's always false
		public static function childThemeFile($parentFile) {

			//no way it's possible if there is no child theme activated
			if ( !FW_IS_CHILD_THEME ) return $parentFile;

			//check if file is inside parent theme
			if ( Common::startsWith( $parentFile , FW_TEMPLATE_DIR ) ) {
				//if so, calculate it's relative to parent theme dir
				$relativePath = str_replace( FW_TEMPLATE_DIR , "" , $parentFile );
				//then using relative path, create potential child theme path where child file could be
				$potentialChildPath = FW_CHILD_TEMPLATE_DIR . $relativePath;

				//if such child file exists, return it
				if ( \is_file( $potentialChildPath ) ) return $potentialChildPath;
			} 

			return $parentFile; //if couldnt find child file, return oryginal path

		}

		public static function getWPContentFolderName() {
			return defined("WP_CONTENT_FOLDERNAME") ? WP_CONTENT_FOLDERNAME : "wp-content";
		}

		public static function addAdminBarItem($title, $href = "#", $parent = false, $icon = false, $priority = 100) {

			$id = Common::slugify($title);

			if ( $parent ) {
				$id = $parent . "-" . $id;
			}

			add_action('admin_bar_menu', function($bar) use($title, $href, $parent, $id, $icon) {

				if ( $icon ) {
					$title = "<span class='ab-icon ab-icon-fw dashicons dashicons-{$icon}'></span>" . $title . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
				}

				$args = array(
				    'title' => $title,
				    'id' => $id,
				    'href'  => $href
				);

				if ( $parent ) $args["parent"] = Common::slugify($parent);

				$bar->add_menu( $args );

			}, $priority);

			return $id;
		}


		public static function cookieDone($name) {
			
			if ( !isset($_COOKIE["fw-done"]) ) return false;
			$done = (array) json_decode( $_COOKIE["fw-done"]);
			if ( !isset( $done[$name] ) ) return false;
			return (bool) $done[$name];
		}

		public static function errorHeader() {
			\header('HTTP/1.1 500 Internal Server Booboo'); //make js ajax return error
		}

		public static function jsonHeader() {
			//\header('Content-Type: application/json');
		}

		public static function bodyAttr() {
			$attrs = array();
			$attrs = apply_filters( "fw_body_attrs" , $attrs );

			$html = Common::arrayToHtmlAttr($attrs);

			echo $html;
		}

		public static function notice($data, $echo = true) {

			if ( is_string($data) ) $data = array(
				"message" => $data,
				"type" => "updated"
			);

			if ( $echo ) {
				\add_action("admin_notices", function() use($data){
					\partial("fw/admin/notice", $data);
				});
			} else {
				return \get_partial("fw/admin/notice", $data);
			}
		}

		public static function isAjax() {
			if( ! empty( $_SERVER[ 'HTTP_X_REQUESTED_WITH' ] ) &&
			      strtolower( $_SERVER[ 'HTTP_X_REQUESTED_WITH' ]) == 'xmlhttprequest' ) {
			    return true;
			}

			if ( defined("DOING_AJAX") && DOING_AJAX ) {
				return true;
			}

			return false;
		}

		public static function archiveTitle() {
			$base = self::pageTitle();

			$parts = explode("-", $base);

			return $parts[0];


		}

		public static function pageTitle(){
			$title = "";
			

			global $paged;
			if (function_exists('is_tag') && is_tag()) {		
				$title .= single_tag_title("Tag Archive for &quot;",false); 
				$title .= '&quot; - '; 
			}
			elseif (is_archive()) {
				$title .= wp_title('',false); 
				//$title .= __(' Archive - ');
				$title .= ' - ';

			}
			elseif (is_search()) {
			$title .= 'Search for &quot;' . esc_html(get_search_query()).'&quot; - '; 
			}
			elseif (!(is_404()) && (is_single()) || (is_page())) {
				$title .= wp_title('',false); 
				$title .= ' - '; 
			}
			elseif (is_404()) {
				$title .= 'Not Found - '; 
			}
			if (is_home()) {
				$title .= get_bloginfo('name'); 
				$title .= ' - '; 
				$title .= get_bloginfo('description'); 
			}
			else {
				$title .= get_bloginfo('name'); 
			}
			if ($paged>1) {
				$title .= ' - page ' . $paged; 
			}
			
			return $title;
		}

		public static function getUserByMeta($key, $val) {
			
			// Query for users based on the meta data
			$user_query = new \WP_User_Query(
				array(
					'meta_key'	  =>	$key,
					'meta_value'	=>	$val
				)
			);

			// Get the results from the query, returning the first user
			$users = $user_query->get_results();

			return count($users) ? $users[0] : false;

		}

		public static function goHome() {
			\wp_redirect( \home_url() ); exit;
		}
		
		public static function postContent($id) {
			//$post = \get_post($id);
			//$content = \apply_filters( "post_content" , $post->post_content );
			//return $content;
			return get_post_field('post_content', (int) $id);
		}

		public static function getPlainCategories() {
			$cats = \get_categories();
			$final = array();
			foreach ($cats as $cat) {
				$final[$cat->slug] = $cat->name;
			}

			return $final;
		}



		public static function manageFileNeeds($file) {
			$templateData = Common::readFileConfig($file);

			if ( isset($templateData["needs"]) ) {
				self::needs( $templateData["needs"] );
			}
		}

		public static function needs($slug) {
			foreach ( explode(",", $slug) as $name) {
				$name = trim($name);


				if ( !Common::isUrl($name) ) {
					wp_enqueue_script($name);
					wp_enqueue_style($name);
				} else {
					$parts = explode(".", $name);
					$extension = array_pop( $parts );
					switch( $extension ) {
						case "js" : 
							wp_enqueue_script($name, $name);
						break;
						case "css" :
							wp_enqueue_style($name, $name);
						break;
					}

				}

			}
		}

		//return array of shortcodes inside content with parsed content, attrs and nested shortcodes
		public static function getContentShortcodes($content) {
			$reg = get_shortcode_regex();
			preg_match_all( '/'. $reg .'/s', $content, $matches );

			$list = array();
			if ( $matches ) foreach ( $matches[0] as $i => $shortcode ) {
				$name = $matches[2][$i];
				$content = $matches[5][$i];
				$attrs = shortcode_parse_atts($matches[3][$i]);

				$data = array(
					"content" => $content,
					"attrs" => $attrs
				);

				if ( $content ) {
					$subShortcodes = self::getContentShortcodes($content);
					if ( $subShortcodes ) {
						$data["shortcodes"] = $subShortcodes;
					}
				}

				$list[$name][] = $data;
			}

			return count($list) ? $list : false;

		}

		public static function bloginfo(){
			$bloginfos = array('name','description','wpurl','url','admin_email','charset','version','html_type','language','stylesheet_url','stylesheet_directory','template_url','template_directory','pingback_url','atom_url','rdf_url','rss_url','rss2_url','comments_atom_url','comments_rss2_url');
			$info = array();
			foreach ( $bloginfos as $key ) {
				$info[$key] = get_bloginfo( $key );
			}
			return $info;
		}

		public static function templateHasFile($name, $extensions = array()) {
			$filesInMainDir = Common::getFilesInDir( FW_TEMPLATE_DIR , (array) $extensions );
			if ( $filesInMainDir ) foreach ( $filesInMainDir as $fileName => $path ) {
				if ( is_string($name) && $fileName == $name ) return $path;
				if ( is_array($name) && !empty($name) ) {
					foreach ( $name as $singleName ) {
						if ( is_string($singleName) && $fileName == $singleName ) return $path;
					}
				}
			}
			return false;
		}

		public static function getPagesByTemplate($template) {
			$the_query = new \WP_Query(array(
			    'post_type'  => 'page',  /* overrides default 'post' */
			    'meta_key'   => '_wp_page_template',
			    'meta_value' => $template
			));

			return $the_query->posts;
		}

		public static function getPageByTemplate($template) {
			$the_query = new \WP_Query(array(
			    'post_type'  => 'page',  /* overrides default 'post' */
			    'meta_key'   => '_wp_page_template',
			    'meta_value' => $template
			));

			$pages = (array) $the_query->posts;

			return reset($pages);
		}

		public static function autoCreatePageWithTemplate($template) {
			if ( !FW_ALLOW_TEMPLATE_AUTOCREATE ) return false;
			$template = Common::removePrefix($template , FW_TEMPLATE_DIR . DS );
			$pagesWithThisTemplate = \Fw\WpUtil::getPagesByTemplate($template);

			if ( !$pagesWithThisTemplate ) {

				$data = Common::readFileConfig(FW_TEMPLATE_DIR . DS . $template);
				$name = $data["name"] ?: Common::humanize( Common::removeExtension($template) );

				$post = array(
				  'post_title'     => $name, // The title of your post.
				  'post_status'    => 'publish', // Default 'draft'.
				  'post_type'      => 'page', // Default 'post'.
				  'post_author'    => 1,
				  'page_template'  => $template // Requires name of template file, eg template.php. Default empty.
				);

				\wp_insert_post( $post , $wp_error );
			}
		}

		public function renderQuery($partial, $args) {
			foreach ( get_posts( $args ) as $post) {
				$vars = $post;
				$vars["post"] = $post;
				Partial::render($partial, $vars );
			}
		}

		public static function detectFavIcon() {

			$fun = function(){
				$favicon = WpUtil::templateHasFile("favicon", array("png", "ico", "gif", "jpg"));
				
				if ( $favicon ) {
					$favicon = Common::wpDirToUri($favicon);
					Partial::render("common/favicon", array( "url" => $favicon ));
				}
			};
			
			\add_action('wp_head', $fun);
			\add_action('admin_head', $fun);
			\add_action('login_head', $fun);
			
		}

		public static function isLoginPage() {
			return in_array( $GLOBALS['pagenow'], array( 'wp-login.php', 'wp-register.php' ) );
		}

		public static function detectLoginLogo() {
			
			$logo = self::templateHasFile("login-logo", array("png", "gif", "jpg"));

			\add_action('login_head', function() use($logo) {
				
				if ( $logo ) {
					$logo = Common::wpDirToUri($logo);
					Partial::render("common/login-logo", array( "logo" => $logo ));
				}
			});

		}

		public static function breadcrumbs() {
			return \get_full_breadcrumb(array(
				"type" => "array",
				"separator" => false
			));
		}

		public static function addTabToEditor($name, $content) {
			$slug = Common::slugify($name);
			\add_filter('edit_form_after_editor', function() use($name, $slug, $content) {

				Partial::render("fw/editor/new-tab-js", array(
					"name" => $name,
					"slug" => $slug
				));

				Partial::render("fw/editor/new-tab-content", array(
					"name" => $name,
					"slug" => $slug,
					"content" => $content
				));	
							
			});
		}

		//enqueue entire folder of css / js files with dependencies basing on dep.dep.name.js order
		public static function enqueueDir($dir) {			
			
			if ( !is_dir($dir) ) return false;
			$files = scandir($dir);
			$depenenciesResolvedList = Common::resolveDependencies($files);




			foreach( $depenenciesResolvedList as $fileName => $depsData ) {	
				$uri = Common::wpDirToUri($dir . DS . $fileName);

				$pi = pathinfo($fileName);

				if ( isset($pi["extension"]) ) {
					switch ($pi["extension"]) {
						case 'js':
							wp_enqueue_script( $uri , $uri );
						break;
						case 'css':
							wp_enqueue_style( $uri , $uri );					
						break;
					}
				}		

			}
		}

		public static function loop($name, $args = false) {

			$loopDir = FW_TEMPLATE_DIR . DS . "loop" . DS . $name . DS;
			if ( !is_dir( $loopDir ) ) return;

			if ( !empty($args) ) {
				\query_posts($args);
			}

			\wp_reset_query();

			\partial("fw/loop/show", array( "name" => $name ));

		}

		public static function hasPostMoreTag($post) {
			$post = \get_post($post);
			return (bool) strpos($post->post_content, '<!--more-->');
		}

		public static function autoEnqueue($uri, $where = "site", $name = false, $deps = array()) {
			$pi = pathinfo($uri);

			if ( !$name ) $name = $uri;

			$type = $pi["extension"];

			$actions = array();

			if ( in_array( $where , array("global", "front", "front-end", "site") ) ) $actions[] = "wp_enqueue_scripts";
			if ( in_array( $where , array("global", "back", "back-end", "admin") ) ) $actions[] = "admin_enqueue_scripts";
			if ( in_array( $where , array("global", "login") ) ) $actions[] = "login_head";
			
			$enqueueFunc = function() use($type, $uri, $name, $deps) {
				if ( $type == "js" ) \wp_enqueue_script( $uri , $name , $deps , null, true );
				if ( $type == "css" ) \wp_enqueue_style( $uri , $name );
			};

			\add_actions($actions , $enqueueFunc);
		}

		public static function autoRegister($uri, $name = false) {
			$pi = pathinfo($uri);
			if ( !$name ) $name = $uri;
			$type = $pi["extension"];

			$registerFunc = function() use($uri, $name, $type) {
				if ( $type == "js" ) {
					\wp_deregister_script( $name );
					\wp_register_script( $name, $uri );
				}
				if ( $type == "css" ) {
					\wp_deregister_style( $name );
					\wp_register_style( $name, $uri );
				}
			};

			\add_actions( array("wp_enqueue_scripts", "admin_enqueue_scripts", "login_head") , $registerFunc);
		}

		public static function postApetizer($post = false) {

			if ( !$post ) $post = \get_the_id();

			if ( self::hasPostMoreTag($post) ) {
				\the_content();
			} else {
				\the_excerpt();
			}
		}

		public static function registerAssetsDir($dir) {			
			
			if ( !is_dir($dir) ) return false;
			$files = scandir($dir);
			$depenenciesResolvedList = Common::resolveDependencies($files);

			foreach( $depenenciesResolvedList as $fileName => $depsData ) {	
				$uri = Common::wpDirToUri($dir . DS . $fileName);	
				
				$pi = pathinfo($fileName);

				switch ($pi["extension"]) {
					case 'js':
						wp_register_script( $depsData["name"] , $uri );
					break;
					case 'css':
						wp_register_style( $depsData["name"] , $uri );					
					break;
				}
			}
		}


	}

?>