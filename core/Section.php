<?php 

	namespace Fw;

	class Section extends Partial{		

		const META_KEY = "fw-sections";

		static $dir = "sections";


		//get data out of section slug
		public static function sectionData($slug) {
			$dir = self::getDir($slug);
			if ( $dir ) {
				$data = Common::readFileConfig($dir);
				$name = $data["name"] ?: Common::humanize($slug);
				$data["name"] = $name;
				$data["slug"] = $slug;
				$data["file"] = $dir;
			}
			return $data;
		}



		//add tab to tinymce wordpress editor
		public static function addEditorTab() {



			//we'll need sortable/draggable
			add_action("admin_enqueue_scripts", function(){
				wp_enqueue_script("jquery-ui-draggable");
				wp_enqueue_script("jquery-ui-sortable");
			});

			//only for page editor
			if ( isset($_GET["post"]) ) {
				if ( get_post_type( (int) $_GET["post"] ) !== "page" ) return;
			} else if ( !isset($_GET["post_type"]) || $_GET["post_type"] !== "page" ) {
				return;
			}

			//get all av sections
			$sectionsList = self::getAllData();

			//and all its data
			$sectionsData = array();
			foreach ($sectionsList as $slug => $path) {
				$sectionsData[] = self::sectionData($slug);
			}


			//if there is edition of page
			$id = isset($_GET["post"]) ? (int) $_GET["post"] : 0;
			
			$actualSectionsList = get_post_meta( $id , \Fw\Section::META_KEY , true );	//get actual sections				
			if ($actualSectionsList) foreach ( (array) $actualSectionsList as $key => $section) {
				$actualSectionsList[$key]["data"] = self::sectionData($section["slug"]);
			} else $actualSectionsList = false;
			
			$metaKey = static::META_KEY;



			//add save action
			add_action( 'save_post', function($id) use( $metaKey ) {
							
				if ( isset($_POST[$metaKey]) ) {
					foreach ($_POST[$metaKey] as $key => $value) {
						$_POST[$metaKey][$key] = json_decode($value,true);
					}

					update_post_meta( $id, static::META_KEY, wp_slash($_POST["fw-sections"]) );					
				}
			});



			//add tab to tinymce
			WpUtil::addTabToEditor("Sections", Partial::get("fw/editor/editor-tab-sections", array(
				"allSections" => $sectionsData,
				"actualSections" => $actualSectionsList ?: array()
			)));

		}

	}
?>