<?php 

	namespace Fw;

	//this class is responsible for creating options page basing on config
	class Options {


		//flat data of all options
		private static $data = array();
		private static $defaults = array();




		//convert data from xml to unified format and adds slug if needed
		public static function configToOptionData($optionConfig) {

			if ( !isset( $optionConfig["attr"] ) ) return $optionConfig;

			$data = $optionConfig["attr"];

			$data["suboptions"] = isset($optionConfig["suboption"]) ? (array) $optionConfig["suboption"] : false;

			if ( !isset($data["name"]) ) return null;

			if ( !isset($data["slug"]) ) $data["slug"] = Common::slugify($data["name"]);

			return $data;
		}


		//recursive method that convert array of xml options data to flatten array
		public static function flattenOptionsData($optionsData) {
			$flatten = array();

			if ( empty($optionsData) ) return $flatten;

			if ( isset( $optionsData["attr"] ) ) $optionsData = array( self::configToOptionData($optionsData) );

			foreach ( $optionsData as $option ) {
				
				$option = self::configToOptionData($option);

				$flatten[] = $option;



				if ( isset( $option["suboptions"] ) ) {

					$flatten = array_merge( $flatten , self::flattenOptionsData( $option["suboptions"] ) );
				}
			}

			return $flatten;


		}
		//gets all sections of options from xml config and return flatten data about every option
		public static function getAllData() {
			if ( !empty( self::$data ) ) return self::$data;
			$config = Config::$config;


			$optionsData = array();

			if ( isset( $config["options"]["section"] ) ) {
				$sections = (array) $config["options"]["section"];

				foreach ( $sections as $section ) {
					$sOptions = $section["option"];

					$sOptions = self::flattenOptionsData( $sOptions );

					$optionsData = array_merge( $optionsData , $sOptions );
				}

			}

			$finalData = array();

			foreach ( $optionsData as $option ) {
				unset( $option["suboptions"] );
				if ( !empty( $option["slug"] ) ) $finalData[ $option["slug"] ] = $option;
			}

			self::$data = $finalData;

			return $finalData;
		}

		//returns key val array of defaults of every option
		public static function getDefaults() {
			if ( !empty( self::$defaults ) ) return self::$defaults;

			$optionsData = self::getAllData();

			$defaults = array_map( function($option) {
				return isset( $option["default"] ) ? $option["default"] : null;
			} , $optionsData);

			self::$defaults = $defaults;

			return $defaults;
		}

		public static function restoreDefaults() {
			\delete_option( FW_OPTIONS_KEY );
		}

		public static function getDefault($name) {
			$defaults = self::getDefaults();
			return isset($defaults[$name]) ? $defaults[$name] : null;
		}

		public static function getAll() {
			$all = \get_option( FW_OPTIONS_KEY );

			$all = \maybe_unserialize($all);

			$defaults = self::getDefaults();

			$all = !empty($all) ? (array) $all : array();

			return array_merge( $defaults , $all);
		}

		public static function saveAll($options) {
			
			foreach ( $options as $key => $val ) {
				\set_theme_mod($key, $val);
			}

			$serialized = serialize($options);
			update_option( FW_OPTIONS_KEY , $serialized );
		}

		public static function set($option, $value) {
			$all = self::getAll();
			$all[$option] = $value;
			self::saveAll($all);
		}

		public static function get($option) {


			if ( \get_theme_mod($option, null) !== null && \get_theme_mod($option) !== null ) return \get_theme_mod($option);

			$all = self::getAll();
			$val = isset($all[$option]) ? $all[$option] : ( isset($all[ Common::slugify($option) ]) ? Common::slugify($option) : null );

			//try to slugify key of option and chack again
			if ($val === null) {
				$option = Common::slugify($option);
				$val = isset($all[$option]) ? $all[$option] : ( isset($all[ Common::slugify($option) ]) ? Common::slugify($option) : null );
			}

			return $val;
		}

		public static function getBool($option) {
			$value = self::get($option);
			return Common::maybeBool($value);
		}

	}

 ?>