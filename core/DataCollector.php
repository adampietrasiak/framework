<?php 
	namespace Fw;

	//this class is to prepare useful data to front end so you can use variables like $post->parent->thumbnail->url on template files
	class DataCollector {

		static $data = null;


		public static function postThumbs($post) {	

			require_once( "MultipleFeatured.php" );

			$full = MultipleFeatured::getThumbnailsOfPost($post->ID);

			$sizes = array();

			foreach ( \get_intermediate_image_sizes() as $size ) {
				$featuredOfSize = MultipleFeatured::getThumbnailsOfPost($post->ID, $size);
				foreach ($featuredOfSize as $key => $featured) {
					if ( !isset( $full[$key]->sizes ) ) $full[$key]->sizes = (object) array(); 
					$full[$key]->sizes->{$size} = $featured;
				}
			}
			
			return $full;
		}

		
		//get post thumb info $post->thumbnail->url/width/height
		public static function postThumb($post) {
			return $post->thumbnails[0];
		}

		
		



		public static function paginationData($query = false) {		
			global $wp_query;
			$query = $query ?: $wp_query;

			//if less than 2 pages, no pagination			
			if ( $query->max_num_pages < 2 ) {
				return false;
			}

			$pagination = new \stdClass();

			//compute query info to nice pagination data
			$pagination->last = (int) $query->max_num_pages;
			$pagination->first = 1;
			$pagination->actual = $query->query_vars["paged"];
			if ( $pagination->actual === 0 ) $pagination->actual = 1;

			$pagination->isLast = $pagination->actual == $pagination->last;
			$pagination->isFirst = $pagination->actual == $pagination->first;
			
			$pagination->next = $pagination->isLast ? false : $pagination->actual + 1;
			$pagination->prev = $pagination->isFirst ? false : $pagination->actual - 1;

			$pagination->default_per_page = (int) get_option( 'posts_per_page' );
			
			return $pagination;
		}




		public static function menuItemData($item) {
			if ( $item->object == "page" ) {
				$item = self::postData($item->object_id);
				$item->permalink = !empty($item->permalink) ? $item->permalink : $item->guid;
			} else {
				$item->permalink = $item->url;
			}
			return $item;
		}

		public static function menuData() {
			$menus = get_terms( 'nav_menu', array( 'hide_empty' => true ) );

			$menu = count($menus) ? reset($menus) : (object) array( 
				"name" => "Main menu" 
			);

			$items = count($menus) ? wp_get_nav_menu_items( reset($menus)->term_id ) : get_pages();



			foreach ($items as $key => $item) {
				$items[$key] = self::menuItemData($item);
			}

			$menu->items = $items;


			return $menu;
		}

		public static function menusData() {
			$theme_locations = get_nav_menu_locations();

			$results = array();
			foreach ($theme_locations as $location => $id) {
				$menu_obj = get_term( $theme_locations[$location], 'nav_menu' );
				$items = wp_get_nav_menu_items( $menu_obj->term_id );

				foreach ($items as $key => $item) {
					$items[$key] = self::menuItemData($item);
				}

				$results[$location] = $items;
			}

			return count($results) ? (object) $results : false;
		}

		public static function optionsData() {
			$wpOptions = wp_load_alloptions();
			
			foreach ($wpOptions as $key => $option) {
				if ( is_serialized($option) ) {
					$wpOptions[$key] = unserialize($option);
				}
			}

			$fwOptions = Options::getAll();

			$allOptions = array_merge($wpOptions, $fwOptions ?: array());

			$objectOptions = Common::arrayToObject($allOptions);
			return $objectOptions;
		}

		public static function breadcrumbsData(){
			$breadcrumbs = WpUtil::breadcrumbs();
			if (is_array($breadcrumbs)) unset($breadcrumbs[0]);
			return $breadcrumbs;
		}
	}
?>