<?php 


	namespace Fw;

	class FwConfig {


		const FW_DIR = __DIR__;
		const FW_DEBUG = false;

		const FW_CHECK_PHP_VERSION = true;
		const FW_REQUIRED_PHP_VERSION = "5.3.28";

		const FW_WORDPRESS_REQUIRED_SCRIPTS = "jquery,comment-reply";
		const FW_AUTO_INCLUDE_HEADER_FOOTER = true;
		const FW_ENABLE_CUSTOM_LOGIN_REGISTER = true;
		const FW_PREPARE_VARIABLES = true;
		const FW_TEMPLATE_SYSTEM = true;
		const FW_SMART_HEAD = true;
		const FW_ALLOW_TEMPLATE_AUTOCREATE = true;


		const FW_ENABLE_PAGE_AJAX = true;

		const FW_ENABLE_CSS_COMBINE = false;
		const FW_ENABLE_JS_COMBINE = false;


		const FW_EXCERPT_LENGTH = 120;
		const FW_EXCERPT_MORE = "...";

		
		const FW_AUTOCREATE_STRUCTURE = 1;

		const FW_HIDE_WP_ADMIN_BAR = false;

		const FW_GOOGLE_API_KEY = "AIzaSyAe6S72vIfHzhPgTNSQuPgQS2NIN76zq6Y";


		const FW_NEEDS = "";

		const FW_SUPER_DEV = true; //allows some additional features like importing a;

		private static $r = null;



		public static function getDefault($key) {
			if ( !self::$r ) {
				self::$r = new \ReflectionClass( get_called_class() );
			}
			$r = self::$r;
			$defaultValue = $r->getConstant($key);

			if ( $defaultValue ) return $defaultValue;
		}


		public static function get($key) {
			$themeConfig = Config::getAll();
			
			$themeFwConfig = !empty( $themeConfig["framework"]["option"] ) ? $themeConfig["framework"]["option"] : null;

			var_dump($themeFwConfig);

			$themeFwOptions = (array) array_filter( $themeFwConfig , function($elem) use($key) {
				return !empty( $elem["name"] ) && $elem["name"] == $key;
			} );

			$themeFwOption = array_shift( $themeFwOptions );

			$themeFwOptionValue = !empty( $themeFwOption["value"] ) ? $themeFwOption["value"] : null;

			if ( $themeFwOptionValue ) return $themeFwOptionValue;

			$r = new \ReflectionClass( get_called_class() );
			$defaultValue = $r->getConstant($key);

			return self::getDefault($key);

		}


	}


?>