<?php
	namespace Fw;

	class Term extends AssignClass {


		protected static $fieldPrefix = "term_";

		public function __construct($id , $taxonomy ) {
			$term = \get_term( $id , $taxonomy , "ARRAY_A" );
			foreach ( $term as $key => $val ) {
				$this->{$key} = $val;
			}

			return $this;
		}

		public static function find($id , $taxonomy) {
			return new self($id , $taxonomy);
		}

		public static function findAll($taxonomies , $args = array()) {
			return Set::inst( \get_terms($taxonomies , $args) )->map(function($t){
				return Term::find( $t->term_id , $t->taxonomy );
			});
		}

		public function taxonomy() {
			return TermTaxonomy::find($this->taxonomy);
		}

		public function posts() {
			return Post::findAll(array(
			  'numberposts' => -1,
			  'tax_query' => array(
			    array(
			      'taxonomy' => $this->taxonomy(),
			      'field' => 'id',
			      'terms' => $this->term_id, // Where term_id of Term 1 is "1".
			      'include_children' => false
			    )
			  )
			));
		}

		public function addToPost($post) {
			\wp_set_object_terms( $post , $this->id , $this->taxonomy , true );
			return $this;
		}

		public function removeFromPost($post) {
			\wp_remove_object_terms( $post , $this->id , $this->taxonomy );
			return $this;
		}

		public function save() {
			\wp_update_term( $this->id , $this->taxonomy , $this->data );
		}

		public function remove() {
			return \wp_delete_term( $this->id , $this->taxonomy );
		}

		public function isIn($post) {
			return \has_term( $this->id , $this->taxonomy , $post );
		}

	}
	