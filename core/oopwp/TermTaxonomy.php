<?php
	namespace Fw;

	class TermTaxonomy {

		public function __construct($id , $taxonomy ) {
			$term = \get_term( $id , $taxonomy , "ARRAY_A" );
			foreach ( $term as $key => $val ) {
				$this->{$key} = $val;
			}
			return $this;
		}

		public static function getAllNames() {
			return \get_taxonomies();
		}

		public static function find($id , $taxonomy) {
			return new self($id , $taxonomy);
		}

		public function terms() {
			return Term::findAll( $this->name );
		}

		public function addTerm($term , $args = array()) {
			\wp_insert_term( $term , $this->name , $args );
			return $this;
		}

		public function removeTerm($term) {
			Term::find($term)->remove();
			return $this;
		}

		public function removeAllTerms() {
			$this->terms()->each(function($t){
				$t->remove();
			});
			return $this;
		}

		public function hasTerms() {
			return !$this->terms()->isEmpty();
		}

		public function termsCount() {
			return $this->terms()->length();
		}

	}
	

//var_dump(  Post::find(1)->terms()->first());