<?php 

	namespace Fw;

	class Menu extends AssignClass {

		protected static $fieldPrefix = "term_";

		public function __construct($id) {
			$menu = \wp_get_nav_menu_object($id);

			if ( !$menu ) {
				$theme_locations = \get_nav_menu_locations();
				if ( !empty( $theme_locations[$id] ) ) {
					$menu = get_term( $theme_locations[$id], 'nav_menu' );
					$menu->location = $id;
				}
			}

			if ( $menu ) {
				foreach ( (array) $menu as $key => $val ) {
					$this->{$key} = $val;
				}
			}
		}

		public static function find($id) {
			return new self($id);
		}

		public function items($rootOnly = true) {
			$menuId = $this->id;

			return Set::inst( \wp_get_nav_menu_items( $this->id ) )->filter(function($i) use($rootOnly) {
				return !$rootOnly || $i->menu_item_parent == "0";
			})->map(function($i) use($menuId) {
				return MenuItem::find($i->ID , $menuId);
			});
		}

		public function itemsCount($rootOnly = true) {
			return $this->items($rootOnly)->length();
		}

	}





 ?>