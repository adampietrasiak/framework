<?php 

	namespace Fw;

	class PostAttachment {

		private $size = "full";

		public function __construct( $id , $size = "full" ) {
			$this->id = $id;
			$this->size = $size;
		}

		public static function find($id) {
			return new self($id);
		}

		public function url($size = false) {
			if ( !$size ) $size = $this->size;

			$data = \wp_get_attachment_image_src( $this->id , $size );
			return $data[0];
		}

		public function path() {
			return \get_attached_file( $this->id );
		}

		public function size() {
			return \filesize( $this->path() );
		}

		public function update($file) {
			if ( \is_file($file) ) {
				update_attached_file( $this->id, $file );
			}
			return $this;
		}

		public function width($size = false) {
			if ( !$size ) $size = $this->size;

			$data = \wp_get_attachment_image_src( $this->id , $size );
			return $data[1];
		}

		public function height($size = false) {
			if ( !$size ) $size = $this->size;

			$data = \wp_get_attachment_image_src( $this->id , $size );
			return $data[2];
		}

		public function ratio($size = false) {
			if ( !$size ) $size = $this->size;

			return $this->width($size) / $this->height($size);

		}

		public function cssBg($size = false) {
			if ( !$size ) $size = $this->size;

			return "background-image: url(" . $this->url($size) . ");";
		}

		public function exif() {
			$data = \wp_get_attachment_metadata( $this->id );
			if ( $data && !empty($data["image_meta"]) ) {
				return (object) $data["image_meta"];
			}
			return false;
		}

		public function render($size = false, $return = false) {
			if ( !$size ) $size = $this->size;

			$html = \wp_get_attachment_image( $this->id , $size );
			if ( !$return ) {
				echo $html;
				return $this;
			} else {
				return $html;
			}
		}

		public function posts() {
			return Post::findAll( array(
				"meta_key" => "_thumbnail_id",
				"meta_value" => $this->id
			));
		}

		public function remove() {
			return \wp_delete_attachment( $this->id );
		}

		public function age() {
			return Post::find($this->id)->age();
		}

	}

 ?>