<?php 

	namespace Fw;

	class PostMeta {

		private $key;
		private $post;

		public function __construct($post , $key, $value = null) {
			$this->post = $post;
			$this->key = $key;

			if ( $value ) {
				$this->value = $value;
				$this->save();
			} else {
				$this->value = \get_post_meta( $this->post , $this->key , true );
			}

			return $this;
		}

		public function __toString() {
			return !empty($this->value) ? $this->value : "";
		}

		public static function inst($post , $key, $value = null) {
			return new self($post , $key , $value);
		}

		public function save() {
			\update_post_meta( $this->post , $this->key , $this->value );
			return $this;
		}

		public function remove() {
			\delete_post_meta( $this->post , $this->key );
		}

		public function key() {
			return $this->key;
		}

		public function post() {
			return Post::find($this->post);
		}

		public function clear() {
			return $this->set(null);
		}

		public function set($val) {
			$this->value = $val;
			return $this->save();
		}

	}

?>