<?php 

	namespace Fw;

	class MenuItem extends AssignClass {

		private $menuId;
		private $rawSiblings;

		protected static $fieldPrefix = "post_";


		public function __construct($id , $menuId) {

			$this->menuId = $menuId;
			$rawSiblings = Set::inst( \wp_get_nav_menu_items( $this->menuId ) );

			
			$item = $rawSiblings->choose(function($i) use($id) {
				return $i->ID == $id;
			});

			foreach ( (array) $item as $key => $val ) {
				$this->{$key} = $val;
			}

			$this->rawSiblings = $rawSiblings;

		}

		public static function find($id , $menuId) {
			return new self($id , $menuId);
		}

		public function children() {
			$id = $this->id;
			$menuId = $this->menuId;
			return $this->rawSiblings->filter(function($i) use($id){
				return $i->menu_item_parent == $id;
			})->map(function($i) use ($menuId) {
				return MenuItem::find( $i->ID , $menuId );
			});
		}

		public function hasChildren() {
			return !$this->children()->isEmpty();
		}

		public function childrenCount() {
			return $this->children()->length();
		}

		public function menu() {
			return Menu::find( $this->menuId );
		}

	}

?>