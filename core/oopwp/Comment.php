<?php 

	namespace Fw;

	class Comment extends AssignClass {


		protected static $fieldPrefix = "comment_";

		public function __construct($id) {
			$comment = \get_comment($id , "ARRAY_A");
			foreach ( $comment as $key => $val ) {
				$this->{$key} = $val;
			}
			return $this;
		}

		public function set($key , $val) {
			$this->{$key} = $val;
			return $this;
		}

		public static function find($id) {
			return new self($id);
		}

		public static function findAll($args = array()) {
			return Set::inst( \get_comments( $args ) )->map(function($c){
				return Comment::find( $c->comment_ID );
			});
		}

		public function author() {
			return new User($this->user_id);
		}

		public function parent() {
			return new self($this->parent);
		}

		public function post() {
			return new Post($this->post_id);
		}

		public function replies() {
			$args = array(
				"parent" => $this->id,
				"post_id" => $this->post_id
			);
			
			return Set::inst( \get_comments($args) )->map( function($c){
				return Comment::find($c->id);
			});
		}

		public function delete() {
			\wp_delete_comment($this->id , true);

			return true;
		}

		public function save() {
			if ( !empty($this->id) ) {
				$this->data["ID"] = $this->data["id"];
				\wp_update_comment( $this->data );
			} else {
				$this->id = \wp_insert_comment( $this->data );
			}
			return $this;
		}

		public function approve() {
			return $this->set("approved" , "1")->save();

		}

		public function hide() {
			return $this->set("approved" , "0")->save();
		}

		public function age($short = false) {
			return Common::timeAgo($this->date , $short);
		}



	}







 ?>