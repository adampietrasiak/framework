<?php 

	namespace Fw;

	class Post extends AssignClass {

		protected static $fieldPrefix = "post_";

		public function __construct($id) {
			$post = \get_post($id , "ARRAY_A");
			if ( !$post ) return false;
			foreach ( $post as $key => $val ) {
				$this->{$key} = $val;
			}
			return $this;
		}

		public function set($key , $val) {
			$this->{$key} = $val;
			return $this;
		}

		public static function find($id) {
			return new self($id);
		}

		public static function findAll($args) {
			return Set::inst( \get_posts($args) )->map(function($p){
				return Post::find($p->ID);
			});
		}

		public static function reset($postId = false) {
			if ( !$postId ) $postId = $this->id;
			return self::find($postId);
		}

		public static function active() {
			$id = \get_the_id();
			if ( $id ) return self::find($id);
		}

		public function author() {
			return new User($this->author);
		}

		public function isActive() {
			return \get_the_id() == $this->id;
		}

		public function setActive() {
			global $post; 
			$post = get_post( $this->id , OBJECT );
			setup_postdata( $post );

			wp_reset_postdata();

			return $this;
		}

		public function comments($rootOnly = true) {
			return Set::inst( \get_approved_comments($this->id) )->filter(function($c) use($rootOnly) {
				return !$rootOnly || $c->comment_parent == 0;
			})->map(function($c) {
				return new Comment($c->comment_ID);
			});
		}

		public function parent() {
			return new self($this->parent);
		}

		private function adjacent($taxonomy = false , $prev = false) {
			global $post;
			if ( $post ) {
				$cachePost = $post;
				$post = \get_post($this->id);
			}
			$next = \get_adjacent_post( (bool) $taxonomy , false , (bool) $prev , $taxonomy );
			if ( !empty($cachePost) ) $post = $cachePost;
			return self::find($next->ID);
		}

		public function next($taxonomy = false) {
			return $this->adjacent( $taxonomy , false );
		}

		public function prev($taxonomy = false) {
			return $this->adjacent( $taxonomy , true );
		}

		public function siblings() {
			if ( $this->parent ) {
				$args = array(
				    'child_of' => $this->parent,
				    'exclude' => $this->id
				);

				return self::findAll($args);
			}
			return false;
		}

		public function delete() {
			\wp_delete_post($this->id , true);

			return true;
		}

		public function save() {
			if ( !empty($this->id) ) {
				$this->data["ID"] = $this->data["id"];
				\wp_update_post( $this->data );
			} else {
				$this->id = \wp_insert_post( $this->data );
			}
			return $this;
		}

		public function meta($key = false , $val = false) {
			$id = $this->id;

			if ( !$key ) {
				return Set::inst( \get_post_meta( $this->id ) )->map(function($value , $key) use($id) {
					if ( is_array($value) && count($value) == 1 ) $value = array_pop($value);
					return PostMeta::inst( $this->id , $key , $value );
				});
			} elseif ( !$val ) {
				return PostMeta::inst( $this->id , $key );
			} elseif ( $key && $val ) {
				PostMeta::inst( $this->id , $key )->set($val)->save();
				return $this;
			}
		}

		public function publish() {
			$this->status = "publish";
			return $this->save();

		}

		public function hide() {
			$this->status = "hidden";
			return $this->save();
		}

		public function terms($taxonomy = false) {
			if ( !$taxonomy ) $taxonomy = TermTaxonomy::getAllNames();

			$terms = wp_get_object_terms( $this->id , $taxonomy );

			return Set::inst( \wp_get_object_terms( $this->id , $taxonomy ) )->map(function($t){
				return Term::find( $t->term_id , $t->taxonomy );
			});

		}

		public function thumbnail($name = false , $size = "full") {
			$name = Common::slugify($name);
			$key = $name ? "_thumbnail_{$name}_id" : "_thumbnail_id";
			$thumbnailId = \get_post_meta( $this->id , $key , true );

			if ( $thumbnailId ) {
				return PostAttachment::find( $thumbnailId , $size );
			} else {
				$thumbnails = $this->thumbnails($size);
				if ( !$thumbnails->isEmpty() ) return $thumbnails->first();
			}
			return false;
		}

		public function thumbnails($size = "full") {
			return $this->meta()->filter(function($m){
				return Common::startsWith( $m->key() , "_thumbnail" );
			})->map(function($m) use ($size) {
				return PostAttachment::find( $m->value , $size );
			});
		}

		public function permalink() {
			return \get_permalink($this->id);
		}

		public function age($short = false) {
			return Common::timeAgo($this->date , $short);
		}



	}









 ?>