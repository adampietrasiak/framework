<?php 

	namespace Fw;

	class User {

		private $wpInstance;

		public function __construct($id) {
			$this->wpInstance = new \WP_User($id);
		}

		public static function find($id) {
			return new self($id);
		}

		public static function active() {
			return self::find( \get_current_user_id() );
		}

		public function posts($args = array()) {
			$args["author"] = $this->id;
			return Post::findAll($args);
		}

		public function comments($args = array()) {
			$args["user_id"] = $this->id;
			Comments::findAll($args);
		}

	}





 ?>