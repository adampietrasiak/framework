<?php 

	namespace Fw;

	class AssignClass {

		protected static $fieldPrefix;
		protected $assignData;

		public function &__get ($key) {
			if ( !empty(static::$fieldPrefix) && strpos($key, static::$fieldPrefix) === 0 ) $key = str_replace( static::$fieldPrefix , "" , $key );
			$key = strtolower($key);
			return $this->assignData[$key];
		}

		public function __set($key,$value) {
			$key = strtolower($key);
		    $this->assignData[$key] = $value;
			if ( strpos($key, static::$fieldPrefix) === 0 ) {
				$key = str_replace( static::$fieldPrefix , "" , $key );
				$this->assignData[$key] = $value;
			}
		}

		public function __isset ($key) {
		    return isset($this->assignData[$key]);
		}

		public function __unset($key) {
		    unset($this->assignData[$key]);
		}

	}

?>