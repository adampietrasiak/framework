<?php 

	namespace Fw;

	class Xml {

		private static function getAttrs($node) {
			$attrs = array();
			if($node->hasAttributes) {
				while($node->moveToNextAttribute()) $attrs[$node->name] = Common::maybeCallback( $node->value );
			}
			return $attrs;
		}

		public static function xmlToAssoc($xml, $recurs = false) {
			$assoc = array(); 
			$n = 0; 
			while($xml->read()){ 
			    if($xml->nodeType == \XMLReader::END_ELEMENT) break; 


			    if($xml->nodeType == \XMLReader::ELEMENT and !$xml->isEmptyElement){ 

			    	$node = array();
			    	$nodeType = $xml->name; 



			        $node["@node"] = $nodeType;

			        $node["@attr"] = self::getAttrs($xml);

			        $node = array_merge( $node["@attr"] , $node );

			        $children = self::xmlToAssoc($xml, true);


			        if ( $children ) foreach ( $children as $childrensType => $childrenOfType ) {
			        	$node[ $childrensType ] = $childrenOfType;

			        }

			        $assoc[$nodeType][] = $node;


			    } else if($xml->isEmptyElement){ 
			    	$node = array();
			    	$nodeType = $xml->name; 
			    	$node["@node"] = $nodeType;
			    	$node["@attr"] = self::getAttrs($xml);

			    	$node = array_merge( $node["@attr"] , $node );

			        $assoc[$nodeType][] = $node;


			    } else if( $xml->nodeType == \XMLReader::TEXT ) $assoc["value"] = Common::maybeCallback( $xml->value );
			} 

			

			if ( !$recurs ) {
				
				foreach ( $assoc as $key => $val ) {
					if ( count($val) == 1 ) {
						$assoc[$key] = reset($val);
					}
				}

				//normal files have added root element so they can have multiple root elements
				if ( count($assoc) == 1 && !empty( $assoc["root"] ) ) {
					$assoc = $assoc["root"];
					unset( $assoc["@node"], $assoc["@attr"] );
				}

			}

			

			return $assoc; 
		}

		public static function parse($xmlFile, $array = false) {
			if ( !is_file($xmlFile) ) return false;

			$xml = new \XMLReader(); 

			$source = file_get_contents($xmlFile);

			$xml->XML("<root>" . $source . "</root>"); 
			$assoc = self::xmlToAssoc($xml);
			
			return $assoc;
		}
	}

?>