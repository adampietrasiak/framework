<?php 

	namespace Fw;
	
	//this is mother class for all the components based on file structure
	class Component {

		static $dir;

		static $extensions = false;


		public static function initAll() {


			if ( !method_exists(get_called_class(), "init") ) {
				throw new \Exception("No init method of component");
			}

			if ( !static::$dir ) {
				throw new \Exception("No component dir set");
				
			}
			
			$allFiles = self::getAllFiles();
			static::beforeInitAll($allFiles);

			

			foreach ( $allFiles as $name => $depsInfo ) {

				$path = $depsInfo["file"];
				$name = $depsInfo["originalKey"];

				if ( !is_file($path) ) continue;

				//check child themes
				$childPath = WpUtil::childThemeFile($path);
				if ( $childPath ) $path = $childPath;

				$class = get_called_class();
				$dir = static::$dir;

				Common::doOnce("component-init." . $path, function() use($path, $name, $class, $dir){

					$pi = pathinfo($path);

					if ( Common::startsWith( $pi["basename"] , "__" ) ) return;

					

					$config = Common::readFileConfig($path);
					$config["file"] = $path;

					if ( \method_exists( $class ,'init') ) {
						\call_user_func( $class . "::init" , $path, $config, Common::removeExtension($name), $dir);
					}

				});

			}

			static::afterInitAll($allFiles);

		} 

		//get all files of given type (folder) with given extension in package
		public static function collectPackageFilesOfType($package) {
			$type = static::$dir;
			$extension = static::$extensions;

			$filesList = array();
			$folderDir = $package . DS . $type;

			if ( is_dir( $folderDir ) ) {
				
				$files = Common::getFlattenDirTree($folderDir);
				
				foreach ( $files as $name => $file ) {
					$fullPath = $file;		

					$pi = pathinfo($fullPath);	

					if ( is_file( $fullPath ) && ( !$extension || in_array($pi["extension"] , (array) $extension ) ) ) {
						$filesList[ $name ] = $fullPath;
					}
				}				
			}
			return $filesList;
		}

		public static function getAllFiles() {
			//get all files in all packages of given type with extension
			$type = static::$dir;
			$extension = static::$extensions;

			$packagesFilesList = array();
			
			$packages = PackageManager::getPackagesFiltered();

			
			foreach( $packages as $dir => $package ) {
				$packagesFilesList = array_merge( $packagesFilesList , self::collectPackageFilesOfType( $dir  ) );
			}

			$themeFilesList = self::collectPackageFilesOfType( FW_TEMPLATE_DIR , $type , $extension );
			$fwFilesList = self::collectPackageFilesOfType( FW_ASSETS_DIR , $type , $extension );

			$all = array_merge($fwFilesList , $packagesFilesList , $themeFilesList );


			$resolved = Common::resolveDependencies($all);

			
			return $resolved;

			
		}

		public static function beforeInitAll() {

		}

		public static function afterInitAll() {

		}

	}


?>