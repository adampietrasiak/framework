<?php

	namespace Fw;

	//this component creates new ajax action of WordPress
	class Ajax extends Component{

		static $dir = "ajax";

		static function init($file, $config, $relative) {

			//get action name as relative path to from package
			$actionName = $relative;
			$actionName = str_replace(array("@", "$"), "", $actionName); //remove admin @ or user $ marks from action name

			//get filename
			$filename = Common::pathinfo($file , "filename");

			//check if is user or admin action
			//for admin file begins with @ or is in admin folder
			//for user file begins with $ or is in user folder
			$isAdminAction = Common::startsWith( $relative , "admin" . DS ) || Common::startsWith( $filename , "@" );
			$isUserAction = Common::startsWith( $relative , "user" . DS ) || Common::startsWith( $filename , "$" );

			//if current user cannot use action, dont register it
			if ( $isUserAction && !\is_user_logged_in() ) return;
			if ( $isAdminAction && !\is_super_admin() ) return;

			//create ajax action function
			$ajaxCallback = function() use($file) {

				if ( !defined('DOING_AJAX') ) {
					define("DOING_AJAX", true);
				}

				//if there are post or get variables, extract them so they can be used as normal variables
				if( isset($_POST) ) extract($_POST);
				if( isset($_GET) ) extract($_GET);

				$vars = array_merge($_POST,$_GET);
				unset($vars["action"]);

				//execute file but in try closure so if error occurs we return 500 header
				try {
					$result = include($file); //execute action file
					if ( $result && $result !== 1 ) {
						//if returned -1 - return 500 error
						if ( $result === -1 ) {
							WpUtil::errorHeader();die;
						} else {
							WpUtil::jsonHeader();
							echo json_encode($result);
							die;
						}
					} elseif( $result ) {
						die;
					}
				} catch( \Exception $e ) {
					WpUtil::errorHeader();
					die;
				}
				die;
			};



			//add ajax hooks
			add_action( "wp_ajax_nopriv_{$actionName}", $ajaxCallback );
			add_action( "wp_ajax_{$actionName}", $ajaxCallback );

		}

	}

?>