<?php 

	namespace Fw;

	class Localizator {

		private static $rules; 

		private static function setRules() {

			if ( !empty(self::$rules) ) return;

			self::$rules = array(
				"site" => !\is_admin(),
				"." => !\is_admin(),
				"admin" => \is_admin(),
				"admin/bar" => \is_admin_bar_showing(),
				"admin/:slug" => function($slug){
					return is_admin() && !empty( $_GET["page"] ) && $_GET["page"] == $slug;
				},
				"global" => true,
				"single" => \is_single(),
				"single/:type" => function($type){
					return \is_single() && \get_post_type() == $type;
				},
				"single/:slug" => function(){
					return \is_single() && ( \get_the_slug() == $slug || \get_the_id() == $slug );
				},
				"single/:type/:template" => function($type, $template){
					if ( \get_post_type() == $type ) {
						if ( $type == "page" ) return \is_page_template( $template );
					}
				},
				"single/preview" => \is_preview(),
				"archive" => \is_archive(),
				"archive/user" => \is_author(),
				"archive/time" => \is_time(),
				"archive/:type" => function($type){
					return \is_post_type_archive( $type );
				},
				"archive/:taxonomy" => function($taxonomy){
					if ( $taxonomy == "category" ) return \is_category();
					if ( $taxonomy == "tag" ) return \is_tag();
					return \is_tax( $taxonomy );
				},
				"archive/:taxonomy/:term" => function($taxonomy, $term){
					return \is_tax( $taxonomy , $term );
				},
				"search" => \is_search(),
				"404" => \is_404(),
				"home" => \is_home() || \is_front_page(),
				"rtl" => \is_rtl()
			);
		}

		public static function addRule($rule, $checker) {
			self::setRules();
			if ( is_callable($checker) && is_scalar($rule) ) self::$rules[ (string) $rule ] = $checker;
		}

		public static function check($route) {
			self::setRules();

			$route = Common::unifySlashes($route);
			$route = Common::trimSlashes($route);

			foreach ( self::$rules as $pattern => $checker ) {
				$matching = Common::routeMatch( $route , $pattern );
				
				if ( $matching && !is_array($matching) ) {
					if ( is_bool($checker) && $checker ) return true;
					if ( is_callable($checker) && $checker() ) return true;
				} elseif ( $matching ) {
					if ( is_bool($checker) && $checker ) return true;
					if ( is_callable($checker) && \call_user_func_array( $checker , $matching ) ) return true;
				}
			}

			return false;
		}

	}

?>