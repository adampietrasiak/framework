<?php 

	namespace Fw;

	//this class is able to save entire state of WordPress instance and save it to json, and later import such json
	class Importer {

		public static function getFileDir() {
			$file = FW_TEMPLATE_DIR . DS . "import" . DS . 'import.json';
			return $file;
		}

		public static function canLoad() {
			$file = self::getFileDir();
			return file_exists($file);
		}

		public static function canSave() {
			return defined( "FW_SUPER_DEV" ) && FW_SUPER_DEV && is_super_admin();
		}

		public static function getWidgets() {
			$all_options = wp_load_alloptions();

			$widgetsOptions = array();

			foreach( $all_options as $key => $opt ) {
				if ( \Fw\Common::startsWith($key, "widget_") || $key == "sidebars_widgets" ) {
					$widgetsOptions[$key] = $opt;
				}
			}


			return $widgetsOptions;
		}

		public static function getPosts() {
			$postTypes = \get_post_types();
			$postTypes[] = "attachment";

			$args = array( 
				'post_type' => $postTypes, 
				'posts_per_page' => -1,
				'post_status' => 'any'
			);
			$loop = new \WP_Query( $args );

			$posts = $loop->posts;


			$posts = array_filter( $posts , function($post){
				return !empty( $post->post_type ) && $post->post_type !== "revision";
			} );

			$posts = array_map( function($post){

				$post->meta = Post::find($post)->meta()->raw();
				$post->terms = Post::find($post)->terms()->raw();
				return $post;

			} , $posts );

			return $posts;
		}

		public static function getThemeOptions() {
			return \get_option( FW_OPTIONS_KEY );
		}

		public static function getCommentsData() {
			$comments = \get_comments();

			return $comments;
		}

		public static function getOptionsData() {
			$optionsToSave = array("posts_per_page", "date_format", "time_format", "page_for_posts", "page_on_front");

			$data = array();

			foreach ( $optionsToSave as $key ) {
				$data[$key] = \get_option( $key );
			}

			return $data;
		}

		public static function save() {

			$uploadInfo = \wp_upload_dir();


			$saveData = array(
				"widgets" => self::getWidgets(),
				"posts" => self::getPosts(),
				"themeOptions" => self::getThemeOptions(),
				"comments" => self::getCommentsData(),
				"options" => self::getOptionsData(),
				"uploadUrl" => $uploadInfo["baseurl"]
			);

			$file = self::getFileDir();

			$dir = dirname($file);

			if ( !is_dir($dir) ) { 
				mkdir($dir, 0777, true);
				chmod($dir, 0777);
			}


			file_put_contents($file, json_encode( $saveData ) );
			

		}

		public static function load() {
			$file = self::getFileDir();
			$json = file_get_contents( $file );

			$data = json_decode($json, true);

			$sourceUploadUrl = $data["uploadUrl"];






			if ( !empty($data["widgets"]) ) foreach ( $data["widgets"] as $opt => $value ) {
				if ( !empty($value) ) \update_option( $opt , \maybe_unserialize($value) );
			}

			if ( !empty($data["posts"]) ) foreach ( $data["posts"] as $post ) {
				$post = (array) $post;

				if ( !WpUtil::postExists($post["ID"]) ) {
					
					$post["import_id"] = $post["ID"];
					unset( $post["ID"] );

					$managedPostId = wp_insert_post($post);
				} else {
					wp_update_post($post);
					$managedPostId = $post["ID"];
				}

				//we cant update all attachments at once as it takes too much time and risk of execution time limit is high
				//so we mark those items for ajax smaller requests for later
				if ( $post["post_type"] == "attachment" ) {
					$meta = $post["meta"];

					if ( isset( $meta["_wp_attachment_metadata"] ) ) {
						$attachmentInfo = \maybe_unserialize( $meta["_wp_attachment_metadata"] );
						$sourceFileUrl = $sourceUploadUrl . DS . $attachmentInfo["file"];
						
						\update_post_meta( $managedPostId , "_fw_need_image_update" , $sourceFileUrl );
					}

				}

				foreach ( $post["meta"] as $key => $val ) {
					\update_post_meta( $managedPostId , $key , $val );
				}

				if ( !empty( $post["terms"] ) ) self::updateTerms( $managedPostId , $post["terms"] );

			}

			if ( !empty($data["options"]) ) foreach( $data["options"] as $key => $val ) {
				if ( !empty($val) ) \update_option( $key, \maybe_unserialize($val) );
			}

			if ( !empty($data["themeOptions"]) ) {
				\update_option( FW_OPTIONS_KEY , \maybe_unserialize($data["themeOptions"]) );
			}

			if ( !empty( $data["comments"] ) ) foreach( $data["comments"] as $comment ) {
				$comment = (array) $comment;
				$commentId = (int) $comment["comment_ID"];
				if ( get_comment( $commentId ) ) {
					wp_update_comment( $comment );
				} else {
					wp_insert_comment($comment);
				}
			}

			return true;

		}

		public static function updateTerms($postId, $terms) {
			
			foreach ( \get_taxonomies() as $taxonomy ) {
				wp_delete_object_term_relationships( $postId , $taxonomy );
			}

			foreach ( $terms as $term ) {
				if ( !\term_exists( $term["name"] , $term["taxonomy"] ) ) {
					$newTerm = \wp_insert_term( $term["name"] , $term["taxonomy"] , $term );
					\wp_update_term( $newTerm["term_id"] , $term["taxonomy"] , $term );
				}
				\wp_set_object_terms( $postId , $term["name"] , $term["taxonomy"] , true );
			}
		}

		public static function getAttachmentsToUpdate() {
			$args = array( 
				'post_type' => "attachment", 
				'posts_per_page' => -1,
				'post_status' => 'any',
				'meta_key' => '_fw_need_image_update'
			);
			$loop = new \WP_Query( $args );

			$posts = $loop->posts;

			return $posts;
		}

		public static function updateAttachment($id) {

			if ( WpUtil::postExists($id) ) {

				$url = \get_post_meta( $id , "_fw_need_image_update" , true );
				$attachment = WpUtil::importAttachment( $url , $id );

				if ( $attachment ) {
					\delete_post_meta( $id, "_fw_need_image_update" );
				}
			}

		}

		public static function addImportPage() {
			$file = self::getFileDir();

			$canLoad = self::canLoad();
			$canSave = defined( "FW_SUPER_DEV" ) && FW_SUPER_DEV && is_super_admin();


			if ( $canLoad || $canSave ) {
				\add_theme_page( "Import demo data" , "Import demo data" , "manage_options" , "import-demo-data" , function() use($canLoad, $canSave) {
					partial("fw/admin/import", array( "load" => $canLoad , "save" => $canSave ));
				});
			}
		}

	}



	 //var_dump( \get_taxonomies() ); die;

	// var_dump( \get_terms( \get_taxonomies() ) );

	// global $wpdb;
	// $r = $wpdb->get_results( "SELECT * FROM {$wpdb->prefix}terms" );

	// var_dump($r);die;

	//\wp_set_post_terms( 140 , "LOL" , "category" );die;

	//Importer::save();
	//Importer::load();
	//die;

	
	

?>