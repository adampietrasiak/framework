<?php 
	
	namespace Fw;

	class Cache {

		private static $cache = null;

		private static $initialized = false;

		private static $file = null;

		public static function getCache() {

			if ( !self::$cache ) {
				\phpFastCache::setup("storage","auto");
				self::$cache = \phpFastCache();
			}

			return self::$cache;

		}

		public static function maskFile() {
			$cacheDir = FW_DIR . DS . "cache";

			if ( !is_file( $cacheDir . DS . "index.php" ) ) {
				WpUtil::putContents( $cacheDir . DS . "index.php" , "<?php //silence is golden" );
			}
		}

		private static function cacheFile() {

			$cacheDir = FW_DIR . DS . "cache";

			$files = Common::getFilesInDir( $cacheDir , "json" );

			if ( empty($files) ) {
				$randomName = Common::randomString(32) . ".json";
				self::$file = WpUtil::putContents( $cacheDir . DS . $randomName , "" );
			} else {
				self::$file = array_pop($files);
			}

			return self::$file;

		}

		private static function saveCache() {
			WpUtil::putContents( self::$file , json_encode( self::$cache ) );
		}

		private static function init() {

			if ( !self::$initialized ) {
				$file = self::cacheFile();
				self::$cache = json_decode( file_get_contents( $file ) , true );

				self::$initialized = true;
			}

		}

		private static function getData($key) {
			if ( isset( self::$cache[$key] ) ) return \utf8_decode( self::$cache[$key]["data"] );
		}

		public static function has($key) {
			self::init();
			return isset( self::$cache[$key] ) && !self::expired($key);
		}

		public static function expired($key) {
			if ( !isset( self::$cache[$key] ) ) return true;

			return self::$cache[$key]["expire"] < \time();
		}

		public static function get($key, $longFunc = false, $time = 600) {

			self::init();

			if ( self::has($key) ) {
				return self::getData($key);
			}

			if ( is_callable($longFunc) ) {

				$value = $longFunc();
				self::set( $key , $value , $time );
				return $value;
			} else {
				return null;
			}
		}

		public static function set($key, $value, $time = 600) {

			self::init();

			self::$cache[$key] = array(
				"data" => \utf8_encode($value),
				"expire" => \time() + $time
			);

			self::saveCache();

			return $value;
		}

		public static function delete($key) {

			self::init();

			unset( self::$cache[$key] );
			self::saveCache();
		}

		public static function clear($sure = false) {
			self::init();

			WpUtil::putContents( self::$file , "" );

			self::init();
		}

		public static function auto($key , $val = null, $time = 600) {
			if ( !is_null($val) && !is_callable($val) ) {
				return self::set( $key , $val , $time );
			} else if ( !is_null($val) && is_callable($val) ) {
				return self::get( $key , $val , $time );
			} else if ( is_null($val) ) {
				return self::get( $key );
			}
		}

	}

	\add_action("init", function(){
		Cache::maskFile();
	});

	





 ?>