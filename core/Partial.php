<?php 

	namespace Fw;

	//this component makes it possible to create and render partials with variables system
	class Partial extends Component{

		static $registered = array();


		static $extensions = array("php");

		static $dir = "partials";

		public static function prepareVariables($file, $vars = array()) {
			
			if ( is_object($vars) ) $vars = (array) $vars;

			$config = Common::readFileConfig($file);



			$partialName = self::getName($file);

			$defaults = !empty($config["default"][0]) ? $config["default"][0] : null;

			if ( $partialName ) {
				$vars = apply_filters( "partial" . DS . $partialName , $vars , $defaults );
			}

			if ( !empty($defaults) && \is_array($defaults) ) {
				$vars = array_merge( $defaults , $vars );
			}
			
			$vars = Common::maybeCallback($vars);

			return $vars;
		}

		public static function resolvePath($name) {
			if ( !self::has( $name ) ) {
				if ( WpUtil::templateHasFile( $name ) ) {
					return FW_TEMPLATE_DIR . DS . $name;
				} elseif ( WpUtil::templateHasFile( $name . ".php" ) ) {
					return FW_TEMPLATE_DIR . DS . $name . ".php";
				} elseif ( is_file($name) ) {
					return $name;
				} elseif ( is_file( $name . ".php" ) ) {
					return $name . ".php";
				} else {
					return false;
				}
			} else {
				return self::getDir($name);
			}
		}


		public static function execute($partialFilePath, $vars = array()) {

			if ( \is_file($partialFilePath) && Common::pathinfo($partialFilePath , "extension") == "php" ) {
				$vars = self::prepareVariables($partialFilePath, $vars);
				WpUtil::manageFileNeeds($partialFilePath);

				extract($vars);
				ob_start();						
					include($partialFilePath);
				$output = ob_get_clean();
				return $output;

			}

			return false;
		}

		public static function render($name, $vars = array(), $echo = true) {

			$path = self::resolvePath($name);

			if ( $path && \is_file($path) ) {
				$result = static::execute($path, $vars);
				if ( $echo ) {
					echo $result;
				} else {
					return $result;
				}
				
			} else {
				return false;
			}


		}

		public static function get($name, $vars = array() ) {
			return static::render($name, $vars , false);
		}

		public static function has($name) {
			$class = get_called_class();
			$name = Common::unifySlashes($name);
			return isset( static::$registered[$class][$name] ) && static::$registered[$class][$name];
		}

		public static function getAllData() {
			$class = get_called_class();
			return self::$registered[$class] ?: array();
		}

		public static function getDir($name) {
			$class = get_called_class();
			return self::$registered[$class][$name] ?: false;
		}

		public static function getName($dir) {
			$class = get_called_class();
			if ( !empty(self::$registered[$class]) ) foreach ( self::$registered[$class] as $name => $singleDir ) {
				if ( $singleDir == $dir ) return $name;
			}
			return false;
		}

		public static function renderAll($startsWith, $data = array()) {
			if ( !$startsWith ) return;

			$class = get_called_class();

			foreach ( static::$registered[$class] as $name => $path ) {
				if ( Common::startsWith($name, $startsWith) ) {
					static::render($name, $data);
				}
			}
		}

		public static function init($file, $config, $name) {
			
			$class = get_called_class();

			$pi = pathinfo($file);

			$name = Common::removeExtension($name) ?: $pi["filename"];
			$name = Common::unifySlashes($name);



			static::$registered[$class][$name] = $file;
		}


	}

?>