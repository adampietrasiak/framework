<?php

	class Tweets {

		//selectors to get elements on page that consists author informations
		const AUTHOR_AVATAR_SELECTOR = ".ProfileAvatar-image";
		const AUTHOR_FULLNAME_SELECTOR = ".ProfileHeaderCard-nameLink";
		const AUTHOR_SCREENNAME_SELECTOR = ".ProfileHeaderCard-screennameLink";
		const AUTHOR_DESCRIPTION_SELECTOR = ".ProfileHeaderCard-bio";
		const AUTHOR_WEBSITE_SELECTOR = ".ProfileHeaderCard-urlText";
		const AUTHOR_LOCATION_SELECTOR = ".ProfileHeaderCard-locationText";


		//selector of single tweet element
		const SINGLE_TWEET_SELECTOR = ".Grid";

		//selectors of elements that are INSIDE single tweet element and consist informations about tweet
		const TWEET_CONTENT_SELECTOR = ".ProfileTweet-text";
		const TWEET_RETWEETS_SELECTOR = ".ProfileTweet-actionCountForPresentation";
		const TWEET_FAVS_SELECTOR = ".ProfileTweet-actionCountForPresentation";
		const TWEET_FULLNAME_SELECTOR = ".ProfileTweet-fullname";
		const TWEET_SCREENNAME_SELECTOR = ".ProfileTweet-screenname";
		const TWEET_AVATAR_SELECTOR = ".ProfileTweet-avatar";
		const TWEET_TIME_SELECTOR = ".ProfileTweet-timestamp .js-short-timestamp";
		const TWEET_ID_SELECTOR = ".ProfileTweet";


		public static function feed($nickname) {

			$html = file_get_contents("http://twitter.com/" . $nickname);
			$doc = phpQuery::newDocument($html);


			$tweets = array();
			$user = array(
				"avatar" => trim( pq(self::AUTHOR_AVATAR_SELECTOR)->attr("src") ),
				"fullname" => trim( pq(self::AUTHOR_FULLNAME_SELECTOR)->filter(":not(:empty)")->eq(0)->text() ),
				"screenname" => trim( pq(self::AUTHOR_SCREENNAME_SELECTOR)->filter(":not(:empty)")->eq(0)->text() ),
				"bio" => trim( pq(self::AUTHOR_DESCRIPTION_SELECTOR)->filter(":not(:empty)")->eq(0)->text() ),
				"website" => trim( pq(self::AUTHOR_WEBSITE_SELECTOR)->filter(":not(:empty)")->eq(0)->text() ),
				"location" => trim( pq(self::AUTHOR_LOCATION_SELECTOR)->filter(":not(:empty)")->eq(0)->text() ),
			);

			$tweetsNodes = pq(self::SINGLE_TWEET_SELECTOR);

			foreach ( $tweetsNodes as $tweetNode ) {
				$tweet = pq($tweetNode);

				$content = $tweet->find(self::TWEET_CONTENT_SELECTOR)->filter(":not(:empty)")->eq(0)->text();
				if ( empty($content) || strlen($content) > 300 ) continue;

				$retweets = $tweet->find(self::TWEET_RETWEETS_SELECTOR)->filter(":not(:empty)")->eq(0)->text();
				$favs = $tweet->find(self::TWEET_FAVS_SELECTOR)->filter(":not(:empty)")->eq(0)->text();
				$fullname = $tweet->find(self::TWEET_FULLNAME_SELECTOR)->filter(":not(:empty)")->eq(0)->text();
				$screenname = $tweet->find(self::TWEET_SCREENNAME_SELECTOR)->filter(":not(:empty)")->eq(0)->text();
				$avatar = $tweet->find(self::TWEET_AVATAR_SELECTOR)->eq(0)->attr("src");
				$time = $tweet->find(self::TWEET_TIME_SELECTOR)->eq(0)->attr("data-time");
				$id = $tweet->find(self::TWEET_ID_SELECTOR)->eq(0)->attr("data-tweet-id");

				//extract hashtags
				preg_match_all('/#([^\s]+)/', $content, $matches);
				$hashtags = implode(',', $matches[1]);

				$tweetData = array(
					"id" => (int) $id,
					"content" => trim( $content ),
					"retweets" => (int) $retweets,
					"favs" => (int) $favs,
					"avatar" => trim( $avatar ),
					"screenname" => trim( $screenname ),
					"fullname" => trim( $fullname ),
					"time" => (int) $time,
					"hashtags" => (array) $hashtags,
					"url" => "http://twitter.com/" . trim( $screenname )
				);

				$tweets[$id] = $tweetData;
			}

			//restart keys
			$finalTweets = array();

			foreach ($tweets as $tweet) {
				$finalTweets[] = $tweet;
			}

			return array(
				"tweets" => $finalTweets,
				"user" => $user
			);
		}
	}



?>